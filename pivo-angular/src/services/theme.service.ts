import { Injectable } from '@angular/core'

const LOCAL_STORAGE_THEME_NAME = 'theme-name'

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
    autoConfigureColorMode() {
        let colorMode = undefined

        const localStorageColorMode = localStorage.getItem(LOCAL_STORAGE_THEME_NAME)
        if (localStorageColorMode) {
            colorMode = localStorage.getItem(LOCAL_STORAGE_THEME_NAME) === 'dark' ? 'dark' : 'light'
        } else {
            colorMode = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
        }

        document.querySelector('html')?.setAttribute('data-bs-theme', colorMode)
    }

    switchColorMode() {
        const current = this.getCurrentColorMode()
        const desiredColorMode = current === 'dark' ? 'light' : 'dark'
        localStorage.setItem(LOCAL_STORAGE_THEME_NAME, desiredColorMode)
        document.querySelector('html')?.setAttribute('data-bs-theme', desiredColorMode)
    }

    getCurrentColorMode() {
        const currentColorMode = document.querySelector('html')?.getAttribute('data-bs-theme')
        return currentColorMode === 'dark' ? 'dark' : 'light'
    }

}
