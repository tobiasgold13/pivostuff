import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { ADMIN_BAND_DELETE_ENDPOINT, ADMIN_LOCATION_DELETE_ENDPOINT, ADMIN_ORPHANS_GET_ENDPOINT, AdminOrhpansResponse } from './api-types/admin-orphans'

@Injectable({
  providedIn: 'root'
})
export class AdminOrphansService {

  constructor(private sessionService: SessionService) { }

  getOrphans(): Observable<AdminOrhpansResponse> {
    return this.sessionService.getAuthenticated<AdminOrhpansResponse>(ADMIN_ORPHANS_GET_ENDPOINT)
  }
  
  deleteBand(id: number): Observable<void> {
    return this.sessionService.deleteAuthenticated<void>(ADMIN_BAND_DELETE_ENDPOINT.replace(':id', `${id}`))
  }
  
  deleteLocation(id: number): Observable<void> {
    return this.sessionService.deleteAuthenticated<void>(ADMIN_LOCATION_DELETE_ENDPOINT.replace(':id', `${id}`))
  }

}
