import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, catchError, map, throwError } from 'rxjs'
import { LoginRequest, LoginResponse, TokenPayload } from './api-types/user-handling'
import { jwtDecode } from 'jwt-decode'

const LOCAL_STORAGE_TOKEN_NAME = 'login-token'

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  constructor(private httpClient: HttpClient) { }

  login(req: LoginRequest): Observable<void> {
    return this.httpClient
      .post<LoginResponse>('/api/login', req)
      .pipe(map(res => { localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, res.token) }))
  }

  getDebugData(): Observable<object> {
    return this.httpClient.get<object>('/api/debug')
  }

  logout() {
    localStorage.clear()
    window.location.href = '/'
  }

  isLoggedIn(): boolean {
    return localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME) !== null
  }

  private getToken(): string {
    return localStorage.getItem(LOCAL_STORAGE_TOKEN_NAME) ?? ''
  }

  getPayLoad(): TokenPayload {
    const token = this.getToken()
    return jwtDecode<TokenPayload>(token)
  }

  getAuthenticated<Type>(url: string): Observable<Type> {
    return this.httpClient
      .get<Type>(url, { headers: { 'Authorization': this.getToken() } })
      .pipe(catchError(this.handleError))
  }

  postAuthenticated<Type>(url: string, body?: object): Observable<Type> {
    return this.httpClient
      .post<Type>(url, body, { headers: { 'Authorization': this.getToken() } })
      .pipe(catchError(this.handleError))
  }

  putAuthenticated<Type>(url: string, body: object): Observable<Type> {
    return this.httpClient
      .put<Type>(url, body, { headers: { 'Authorization': this.getToken() } })
      .pipe(catchError(this.handleError))
  }

  deleteAuthenticated<Type>(url: string): Observable<Type> {
    return this.httpClient
      .delete<Type>(url, { headers: { 'Authorization': this.getToken() } })
      .pipe(catchError(this.handleError))
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('client or network error occured', error)
    } else if (error.status === 400) {
      // throw BAD REQUEST errors, because I might want to handle them in the component (mandatory fields etc.)
      alert('Es sind nicht alle Felder korrekt ausgefüllt!')
      return throwError(() => error)
    } else if (error.status === 401) {
      // session expired or sth
      localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
    } else if (error.status === 500) {
      if (typeof error.error === 'string') {
        prompt('Fehler! Kopiere folgenden Text und schicke an pivo:', error.error)
      } else {
        alert('Es ist ein Fehler aufgetreten!')
      }
    } else {
      console.error(error)
      alert('Es ist ein Fehler aufgetreten. :(')
    }
    return new Observable<never>()
  }
}
