import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { SEARCH_ENDPOINT, SearchRequest, SearchResponse } from './api-types/search'

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private sessionService: SessionService) { }

  search(query: string): Observable<SearchResponse> {
    const requestBody: SearchRequest = { query }
    return this.sessionService.postAuthenticated<SearchResponse>(SEARCH_ENDPOINT, requestBody)
  }

}
