import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT, ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT, ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT, ADMIN_SESSIONS_USER_DELETE_ENDPOINT, AdminSessionsActiveUser, AdminSessionsGeneralDataResponse } from './api-types/admin-sessions'

@Injectable({
  providedIn: 'root'
})
export class AdminSessionService {

  constructor(private sessionService: SessionService) { }

  getGeneralData(): Observable<AdminSessionsGeneralDataResponse> {
    return this.sessionService.getAuthenticated<AdminSessionsGeneralDataResponse>(ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT)
  }

  getSessions(): Observable<AdminSessionsActiveUser[]> {
    return this.sessionService.getAuthenticated<AdminSessionsActiveUser[]>(ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT)
  }

  deleteSessionsForUser(userId: number): Observable<void> {
    return this.sessionService.deleteAuthenticated<void>(ADMIN_SESSIONS_USER_DELETE_ENDPOINT.replace(':id', `${userId}`))
  }

  cleanExpiredSessions(): Observable<void> {
    return this.sessionService.deleteAuthenticated<void>(ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT)
  }

}
