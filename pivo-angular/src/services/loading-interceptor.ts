import { HttpErrorResponse, HttpEvent, HttpEventType, HttpHandlerFn, HttpRequest } from '@angular/common/http'
import { catchError, Observable, Subject, tap, throwError } from 'rxjs'

const loadingSubject = new Subject<boolean>()

export const getLoadingObservable = () => loadingSubject.asObservable()

export function loadingInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {
    loadingSubject.next(true)
    return next(req)
    .pipe(tap(event => {
        if (event.type === HttpEventType.Response) {
                loadingSubject.next(false)
            }
        }))
        .pipe(catchError((error: HttpErrorResponse) => {
            loadingSubject.next(false)
            return throwError(() => error)
        }))
  }