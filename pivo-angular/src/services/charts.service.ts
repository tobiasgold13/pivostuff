import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT, CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT, CHARTS_TOP_10_BANDS_GET_ENDPOINT, ChartsEventCountPerCategoryResponseElement, ChartsCountPerYearResponseElement, ChartsTop10Band, CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT } from './api-types/charts'
import { OLEGS_SPECIAL_LIST_GET_ENDPOINT, OlegsSpecialListRow } from './api-types/olegs-special-list'

@Injectable({
  providedIn: 'root'
})
export class ChartsService {

  constructor(private sessionService: SessionService) { }

  getEventsPerYear(): Observable<ChartsCountPerYearResponseElement[]> {
    return this.sessionService.getAuthenticated<ChartsCountPerYearResponseElement[]>(CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT)
  }

  getShowsPerYear(): Observable<ChartsCountPerYearResponseElement[]> {
    return this.sessionService.getAuthenticated<ChartsCountPerYearResponseElement[]>(CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT)
  }

  getEventsPerCategory(): Observable<ChartsEventCountPerCategoryResponseElement[]> {
    return this.sessionService.getAuthenticated<ChartsEventCountPerCategoryResponseElement[]>(CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT)
  }

  getOlegsSpecialList(): Observable<OlegsSpecialListRow[]> {
    return this.sessionService.getAuthenticated<OlegsSpecialListRow[]>(OLEGS_SPECIAL_LIST_GET_ENDPOINT)
  }

  getTop10Bands(): Observable<ChartsTop10Band[]> {
    return this.sessionService.getAuthenticated<ChartsTop10Band[]>(CHARTS_TOP_10_BANDS_GET_ENDPOINT)
  }

}
