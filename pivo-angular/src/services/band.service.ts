import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { Band, BAND_DETAILS_GET_ENDPOINT, BandDetails, SEARCH_BANDS_ENDPOINT, SearchBandsRequest } from './api-types/band'

@Injectable({
  providedIn: 'root'
})
export class BandService {

  constructor(private sessionService: SessionService) { }

  getEvents(id: string): Observable<BandDetails> {
    return this.sessionService.getAuthenticated<BandDetails>(BAND_DETAILS_GET_ENDPOINT.replace(':id', id))
  }

  searchBands(query: string): Observable<Band[]> {
    const requestBody: SearchBandsRequest = { query }
    return this.sessionService.postAuthenticated<Band[]>(SEARCH_BANDS_ENDPOINT, requestBody)
  }

}
