import { Injectable } from '@angular/core'
import { SessionService } from './session.service'
import { Observable } from 'rxjs'
import { EVENT_DETAIL_GET_ENDPOINT, EVENT_OVERVIEW_GET_ENDPOINT, EVENT_YEARS_GET_ENDPOINT, EventCategory, EventDetail, EventOverview } from './api-types/event'
import { SAVE_EVENT_ENDPOINT, SaveEventRequest } from './api-types/add-edit-event'

@Injectable({
  providedIn: 'root'
})
export class EventService {
  
  readonly LOCAL_STORAGE_LAST_SELECTED_YEAR = 'last-selected-year'
  // 2h TTL
  readonly LAST_SELECTED_YEAR_TTL = 2 * 60 *60 *1000

  constructor(private sessionService: SessionService) { }

  getAvailableYears(): Observable<number[]> {
    return this.sessionService.getAuthenticated<number[]>(EVENT_YEARS_GET_ENDPOINT)
  }

  getEvents(year: number): Observable<EventOverview[]> {
    return this.sessionService.getAuthenticated<EventOverview[]>(EVENT_OVERVIEW_GET_ENDPOINT.replace(':year', `${year}`))
  }

  getEventDetail(eventId: string): Observable<EventDetail> {
    return this.sessionService.getAuthenticated<EventDetail>(EVENT_DETAIL_GET_ENDPOINT.replace(':id', eventId))
  }

  getLastSelectedYear(): number | undefined {
    const storageString = localStorage.getItem(this.LOCAL_STORAGE_LAST_SELECTED_YEAR)
    if (!storageString) {
      return undefined
    }
    try {
      const storageObject: LastSelectedYear = JSON.parse(storageString)
      const lifetime = new Date().getTime() - storageObject.dateMillis
      if (lifetime >= this.LAST_SELECTED_YEAR_TTL) {
        return undefined
      }
      return storageObject.year
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (err) {
      localStorage.removeItem(this.LOCAL_STORAGE_LAST_SELECTED_YEAR)
      return undefined
    }

  }

  setLastSelectedYear(year: number) {
    const storageObject: LastSelectedYear = { year, dateMillis: new Date().getTime() }
    localStorage.setItem(this.LOCAL_STORAGE_LAST_SELECTED_YEAR, JSON.stringify(storageObject))
  }

  saveEvent(data: SaveEventRequest): Observable<EventDetail> {
    return this.sessionService.postAuthenticated<EventDetail>(SAVE_EVENT_ENDPOINT, data)
  }

}

interface LastSelectedYear {
  year: number,
  dateMillis: number
}

export const getEventCategoryLocalization = (c: EventCategory | undefined) => {
  switch (c) {
    case EventCategory.concert:
      return 'Konzert'
    case EventCategory.festival:
      return 'Festival'
    default:
      return ''
  }
}
