import { Injectable } from '@angular/core'
import { SessionService } from './session.service'
import { Location, LOCATION_DETAILS_GET_ENDPOINT, LocationDetailResponse, LocationSearchRequest } from './api-types/location'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private sessionService: SessionService) { }

  searchLocations(query: string): Observable<Location[]> {
    const requestBody: LocationSearchRequest = { query }
    return this.sessionService.postAuthenticated<Location[]>('/api/locations/search', requestBody)
  }

  getLocation(id: string): Observable<LocationDetailResponse> {
    return this.sessionService.getAuthenticated<LocationDetailResponse>(LOCATION_DETAILS_GET_ENDPOINT.replace(':id', id))
  }

}
