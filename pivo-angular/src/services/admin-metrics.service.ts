import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { ADMIN_METRICS_CURRENT_GET_ENDPOINT, ADMIN_METRICS_OVER_TIME, ADMIN_METRICS_OVER_TIME_GET_ENDPOINT, ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT, AdminMetricsCurrentResponse, AdminMetricsGroupCountPerUser } from './api-types/admin-metrics'

@Injectable({
  providedIn: 'root'
})
export class AdminMetricsService {

  constructor(private sessionService: SessionService) { }

  getEventsOverTime(): Observable<AdminMetricsGroupCountPerUser[]> {
    return this.sessionService.getAuthenticated<AdminMetricsGroupCountPerUser[]>(ADMIN_METRICS_OVER_TIME_GET_ENDPOINT.replace(':metric', ADMIN_METRICS_OVER_TIME.overallEvents))
  }

  getShowsOverTime(): Observable<AdminMetricsGroupCountPerUser[]> {
    return this.sessionService.getAuthenticated<AdminMetricsGroupCountPerUser[]>(ADMIN_METRICS_OVER_TIME_GET_ENDPOINT.replace(':metric', ADMIN_METRICS_OVER_TIME.overallTotalShows))
  }

  getStats(): Observable<AdminMetricsCurrentResponse> {
    return this.sessionService.getAuthenticated<AdminMetricsCurrentResponse>(ADMIN_METRICS_CURRENT_GET_ENDPOINT)
  }

  updateMetricsHistory(): Observable<void> {
    return this.sessionService.postAuthenticated<void>(ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT)
  }

}
