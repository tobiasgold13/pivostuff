import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { ADMIN_USER_CREATE_ENDPOINT, ADMIN_USER_DELETE_ENDPOINT, ADMIN_USER_GET_ENDPOINT, ADMIN_USER_MODIFY_ENDPOINT, ADMIN_USER_MODIFY_PASSWORD_ENDPOINT, ADMIN_USERS_GET_ENDPOINT, AdminUser, CreateUserRequest, ModifyPasswordRequest, ModifyUserRequest } from './api-types/admin-users'

@Injectable({
  providedIn: 'root'
})
export class AdminUserService {

  constructor(private sessionService: SessionService) { }

  getAllUsers(): Observable<AdminUser[]> {
    return this.sessionService.getAuthenticated<AdminUser[]>(ADMIN_USERS_GET_ENDPOINT)
  }
  
  deleteUser(id: number): Observable<void> {
    return this.sessionService.deleteAuthenticated<void>(ADMIN_USER_DELETE_ENDPOINT.replace(':id', `${id}`))
  }
  
  addUser(data: CreateUserRequest): Observable<void> {
    return this.sessionService.postAuthenticated<void>(ADMIN_USER_CREATE_ENDPOINT, data)
  }

  getUser(id: string): Observable<AdminUser> {
    return this.sessionService.getAuthenticated<AdminUser>(ADMIN_USER_GET_ENDPOINT.replace(':id', id))
  }
  
  modifyUser(user: AdminUser, data: ModifyUserRequest): Observable<void> {
    return this.sessionService.postAuthenticated<void>(ADMIN_USER_MODIFY_ENDPOINT.replace(':id', `${user.id}`), data)
  }
  
  modifyPassword(user: AdminUser, data: ModifyPasswordRequest): Observable<void> {
    return this.sessionService.postAuthenticated<void>(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT.replace(':id', `${user.id}`), data)
  }

}
