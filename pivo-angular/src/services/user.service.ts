import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { SessionService } from './session.service'
import { CHANGE_PASSWORD_ENDPOINT, ChangePasswordRequest, WHO_AM_I_ENDPOINT } from './api-types/user-handling'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private sessionService: SessionService) { }

  whoami(): Observable<{ name: string }> {
    // could be extracted from the token, but I like to use it to make sure the token is still valid
    return this.sessionService.getAuthenticated<{ name: string }>(WHO_AM_I_ENDPOINT)
  }

  changePassword(req: ChangePasswordRequest): Observable<void> {
    return this.sessionService.postAuthenticated<void>(CHANGE_PASSWORD_ENDPOINT, req)
  }

}
