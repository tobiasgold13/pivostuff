export const ADMIN_USERS_GET_ENDPOINT = '/api/admin/users'
export const ADMIN_USER_GET_ENDPOINT = '/api/admin/users/:id'

export interface AdminUser {
    id: number
    name: string
    roles?: string
}

export const ADMIN_USER_CREATE_ENDPOINT = '/api/admin/users'

export interface CreateUserRequest{
    name: string
    roles?: string
    password: string
}

export interface CreateUserResponse {
    id: number
}

export const ADMIN_USER_MODIFY_ENDPOINT = '/api/admin/users/:id'

export interface ModifyUserRequest {
    name: string
    roles?: string
}

export const ADMIN_USER_MODIFY_PASSWORD_ENDPOINT = '/api/admin/users/:id/password'

export interface ModifyPasswordRequest {
    password: string
}

export const ADMIN_USER_DELETE_ENDPOINT = '/api/admin/users/:id'
