import { Band } from './band'
import { EventCategory } from './event'

export const CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT = '/api/charts/event-count-per-year'
export const CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT = '/api/charts/show-count-per-year'

export interface ChartsCountPerYearResponseElement {
    year: number
    count: number
}

export const CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT = '/api/charts/event-count-per-category'

export interface ChartsEventCountPerCategoryResponseElement {
    category: EventCategory
    count: number
}

export const CHARTS_TOP_10_BANDS_GET_ENDPOINT = '/api/charts/top-10-bands'

export interface ChartsTop10Band extends Band {
    count: number
}