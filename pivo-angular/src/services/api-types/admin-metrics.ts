export const ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT = '/api/admin/metrics/over-time/update-history'

export const ADMIN_METRICS_OVER_TIME = {
  overallEvents: 'OVERALL_EVENTS',
  overallTotalShows: 'OVERALL_TOTAL_SHOWS'
}

export const ADMIN_METRICS_OVER_TIME_GET_ENDPOINT = '/api/admin/metrics/over-time/:metric'

export const ADMIN_METRICS_CURRENT_GET_ENDPOINT = '/api/admin/metrics/current'

export interface AdminMetricsCountPerUser {
    username: string
    count: number
}

export interface AdminMetricsGroupCountPerUser extends AdminMetricsCountPerUser {
    group: string
}

export interface AdminMetricsCurrentResponse {
    passedEvents: AdminMetricsCountPerUser[]
    passedDistinctBands: AdminMetricsCountPerUser[]
    passedTotalShows: AdminMetricsCountPerUser[]
    passedEventsByCategory: AdminMetricsGroupCountPerUser[]
}
