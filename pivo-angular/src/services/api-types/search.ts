import { Band } from './band'
import { EventOverview } from './event'
import { Location } from './location'

export const SEARCH_ENDPOINT = '/api/search'

export interface SearchRequest {
    query: string
}

export interface SearchResponse {
    bands: Band[]
    locations: Location[]
    events: EventOverview[]
}
