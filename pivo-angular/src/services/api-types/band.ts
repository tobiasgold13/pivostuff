import { EventOverview } from './event'

export interface Band {
    id: number
    name: string
}

export const BAND_DETAILS_GET_ENDPOINT = '/api/bands/:id'

export interface BandDetails {
    band: Band
    events: EventOverview[]
}

export const SEARCH_BANDS_ENDPOINT = '/api/bands/search'

export interface SearchBandsRequest {
    query: string
}
