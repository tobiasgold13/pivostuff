import { Component } from '@angular/core'
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap'
import { BaseChartDirective, provideCharts, withDefaultRegisterables } from 'ng2-charts'
import { ChartConfiguration } from 'chart.js'
import { ChartsService } from '../../services/charts.service'
import { ChartsTop10Band } from '../../services/api-types/charts'
import { getEventCategoryLocalization } from '../../services/event.service'
import { NgFor } from '@angular/common'
import { BandComponent } from '../band/band.component'

@Component({
  selector: 'app-page-charts',
  standalone: true,
  imports: [NgFor, BaseChartDirective, NgbAccordionModule, BandComponent],
  templateUrl: './page-charts.component.html',
  styleUrl: './page-charts.component.scss',
  providers: [provideCharts(withDefaultRegisterables())]
})
export class PageChartsComponent {

  constructor(private chartsService: ChartsService) {}

  eventsPerYearData: ChartConfiguration['data'] | undefined
  showsPerYearData: ChartConfiguration['data'] | undefined
  eventsPerCatgoryData: ChartConfiguration['data'] | undefined
  top10BandsData: ChartsTop10Band[] | undefined = undefined

  loadEventsPerYearData(): void {
    if (this.eventsPerYearData) {
      return
    }

    this.chartsService.getEventsPerYear().subscribe({
      next: data => {
        this.eventsPerYearData = {
          labels: data.map(el => el.year),
          datasets: [
            {
              data: data.map(el => el.count),
              borderWidth: 1
            }
          ]
        }
      }
    })
  }

  loadShowsPerYearData(): void {
    if (this.showsPerYearData) {
      return
    }

    this.chartsService.getShowsPerYear().subscribe({
      next: data => {
        this.showsPerYearData = {
          labels: data.map(el => el.year),
          datasets: [
            {
              data: data.map(el => el.count),
              borderWidth: 1
            }
          ]
        }
      }
    })
  }

  loadEventsPerCategoryData(): void {
    if (this.eventsPerCatgoryData) {
      return
    }

    this.chartsService.getEventsPerCategory().subscribe({
      next: data => {
        this.eventsPerCatgoryData = {
          labels: data.map(el => el.category ? getEventCategoryLocalization(el.category) : 'ohne Kategorie'),
          datasets: [
            {
              data: data.map(el => el.count),
              borderWidth: 1
            }
          ]
        }
      }
    })
  }

  loadTop10BandsData(): void {
    if (this.top10BandsData) {
      return
    }

    this.chartsService.getTop10Bands().subscribe({
      next: data => this.top10BandsData = data
    })
  }

}
