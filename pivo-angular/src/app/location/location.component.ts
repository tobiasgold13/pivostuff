import { Component, Input } from '@angular/core'
import { RouterLink } from '@angular/router'
import { Location } from '../../services/api-types/location'
import { NgClass, NgIf } from '@angular/common'

@Component({
  selector: 'app-location',
  standalone: true,
  imports: [RouterLink, NgIf, NgClass],
  templateUrl: './location.component.html',
  styleUrl: './location.component.scss'
})
export class LocationComponent {

  @Input({ required:true }) location!: Location
  @Input({ required:false }) border = false
  @Input({ required:false }) link = true

}
