import { Component, OnInit } from '@angular/core'
import { ThemeService } from '../../services/theme.service'
import { RouterLink } from '@angular/router'
import { NgIf } from '@angular/common'
import { UserService } from '../../services/user.service'
import { SessionService } from '../../services/session.service'

@Component({
  selector: 'app-page-menu',
  standalone: true,
  imports: [RouterLink, NgIf],
  templateUrl: './page-menu.component.html',
  styleUrl: './page-menu.component.scss'
})
export class PageMenuComponent implements OnInit {

  constructor(
    private themeService: ThemeService,
    private sessionService: SessionService,
    private userService: UserService
  ) {}

  username = ''

  ngOnInit(): void {
    this.userService.whoami().subscribe({
      next: whoami => this.username = whoami.name
    })
  }

  isDark(): boolean {
    return this.themeService.getCurrentColorMode() === 'dark'
  }

  switchColorMode() {
    this.themeService.switchColorMode()
  }

  logout() {
    this.sessionService.logout()
  }

  getUsername() {
    return this.username
  }

  isAdmin(): boolean {
    const payload = this.sessionService.getPayLoad()
    return payload.roles?.includes('ADMIN') === true
  }

}
