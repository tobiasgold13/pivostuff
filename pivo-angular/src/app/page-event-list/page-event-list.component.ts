import { Component, OnInit } from '@angular/core'
import { NgFor } from '@angular/common'
import { EventOverview } from '../../services/api-types/event'
import { EventComponent } from '../event/event.component'
import { EventService } from '../../services/event.service'
import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-page-event-list',
  standalone: true,
  imports: [NgFor, FormsModule, EventComponent],
  templateUrl: './page-event-list.component.html',
  styleUrl: './page-event-list.component.scss'
})
export class PageEventListComponent implements OnInit {
  constructor(private eventService: EventService) {}

  availableYears: number[] = []
  selectedYear: number | undefined
  events: EventOverview[] = []

  ngOnInit(): void {
    this.eventService.getAvailableYears().subscribe({
      next: years => {
        this.availableYears = years
        this.selectedYear = this.eventService.getLastSelectedYear() || years[0]
        this.loadEvents()
      }
    })
  }

  loadEvents() {
    if (this.selectedYear) {
      this.eventService.setLastSelectedYear(this.selectedYear)
      this.eventService.getEvents(this.selectedYear).subscribe({
        next: (events) => (this.events = events)
      })
    }
  }
}
