import { CommonModule } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { BandComponent } from '../band/band.component'
import { ActivatedRoute, Router } from '@angular/router'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { SearchService } from '../../services/search.service'
import { SearchResponse } from '../../services/api-types/search'
import { EventComponent } from '../event/event.component'
import { LocationComponent } from '../location/location.component'

@Component({
  selector: 'app-page-search',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, BandComponent, EventComponent, LocationComponent],
  templateUrl: './page-search.component.html',
  styleUrl: './page-search.component.scss'
})
export class PageSearchComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private searchService: SearchService,
    private formBuilder: FormBuilder
  ) {
    
  }

  searchForm = this.formBuilder.group({
    query: ['', Validators.required]
  })

  result: SearchResponse = { bands:[], events:[], locations:[] }

  ngOnInit(): void {
    const query = this.route.snapshot.queryParams['q']
    if (query) {
      this.searchForm.patchValue({ query })
      this.search(query)
    }
  }

  submit() {
    const query = this.searchForm.value.query?.trim()
    if (query) {
      this.router.navigate(
        [], 
        {
          relativeTo: this.route,
          queryParams: { q: query }, 
          queryParamsHandling: 'merge'
        }
      )
      this.search(query)
    }
  }
  
  private search(query: string) {
    this.searchService.search(query).subscribe({
      next: result => this.result = result
    })
  }

}
