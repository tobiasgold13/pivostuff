import { Component, Input } from '@angular/core'
import { RouterLink } from '@angular/router'
import { Band } from '../../services/api-types/band'

@Component({
  selector: 'app-band',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './band.component.html',
  styleUrl: './band.component.scss'
})
export class BandComponent {

  @Input({ required:true }) band!: Band

}
