import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor, NgIf } from '@angular/common'
import { LocationDetailResponse } from '../../services/api-types/location'
import { LocationComponent } from '../location/location.component'
import { EventComponent } from '../event/event.component'
import { LocationService } from '../../services/location.service'
import { BandComponent } from '../band/band.component'

@Component({
  selector: 'app-page-location-details',
  standalone: true,
  imports: [NgFor, NgIf, LocationComponent, EventComponent, BandComponent],
  templateUrl: './page-location-details.component.html',
  styleUrl: './page-location-details.component.scss'
})
export class PageLocationDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private locationService: LocationService
  ) {}

  data: LocationDetailResponse = { location:  { id:-1, name:'', city:'', country:'' }, events: [], bands: [] }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id']
    this.locationService.getLocation(id).subscribe({
      next: data => this.data = data
    })
  }

}
