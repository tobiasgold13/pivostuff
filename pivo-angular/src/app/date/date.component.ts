import { Component, Input, OnChanges } from '@angular/core'
import { EventDate } from '../../services/api-types/event'

@Component({
  selector: 'app-date',
  standalone: true,
  imports: [],
  templateUrl: './date.component.html',
  styleUrl: './date.component.scss'
})
export class DateComponent implements OnChanges {

  @Input({ required: true }) date!: EventDate
  @Input({ required: false }) format? = 'dd.mm.yyyy'

  formattedDate = '<date::error>'

  ngOnChanges(): void {
    try {
      const tmp = new Date(this.date)
      this.formattedDate = tmp.toLocaleDateString('de-DE', this.getDateConfig())

    } catch (err) {
      console.error(err)
    }
  }

  private getDateConfig(): Intl.DateTimeFormatOptions {
    let dateConfig: Intl.DateTimeFormatOptions

    switch (this.format) {
      case 'dd.mm.':
        dateConfig = {
          day: '2-digit',
          month: '2-digit'
        }
        break

      case 'dd.MM.':
        dateConfig = {
          day: '2-digit',
          month: 'long'
        }
        break

      case 'dd.mm.yy':
        dateConfig = {
          day: '2-digit',
          month: '2-digit',
          year: '2-digit'
        }
        break

      case 'dd.MM.yy':
        dateConfig = {
          day: '2-digit',
          month: 'long',
          year: '2-digit'
        }
        break

      default:
        // dd.mm.yyyy
        dateConfig = {
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        }
        break
    }
    return dateConfig
  }

}
