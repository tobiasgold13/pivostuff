import { Routes } from '@angular/router'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { PageEventListComponent } from './page-event-list/page-event-list.component'
import { PageEventDetailsComponent } from './page-event-details/page-event-details.component'
import { PageMenuComponent } from './page-menu/page-menu.component'
import { PageBandDetailComponent } from './page-band-detail/page-band-detail.component'
import { PageEditEventComponent } from './page-edit-event/page-edit-event.component'
import { PageAdminIndexComponent } from './admin/page-admin-index/page-admin-index.component'
import { PageChangePasswordComponent } from './page-change-password/page-change-password.component'
import { PageAdminUserListComponent } from './admin/page-admin-user-list/page-admin-user-list.component'
import { PageAdminUserEditComponent } from './admin/page-admin-user-edit/page-admin-user-edit.component'
import { PageAdminSessionsComponent } from './admin/page-admin-sessions/page-admin-sessions.component'
import { PageAdminMetricsComponent } from './admin/page-admin-metrics/page-admin-metrics.component'
import { PageSearchComponent } from './page-search/page-search.component'
import { PageLocationDetailsComponent } from './page-location-details/page-location-details.component'
import { PageAdminOrphansComponent } from './admin/page-admin-orphans/page-admin-orphans.component'
import { PageChartsComponent } from './page-charts/page-charts.component'
import { PageOlegsSpecialListComponent } from './page-olegs-special-list/page-olegs-special-list.component'

export const routes: Routes = [
    { path: '', component: PageEventListComponent },
    { path:'events/:id', component: PageEventDetailsComponent },
    { path:'bands/:id', component:PageBandDetailComponent },
    { path:'locations/:id', component:PageLocationDetailsComponent },
    { path:'search', component:PageSearchComponent },
    { path:'add', component:PageEditEventComponent },
    { path:'menu', component: PageMenuComponent },
    { path:'change-password', component: PageChangePasswordComponent },
    { path:'charts', component: PageChartsComponent },
    { path:'olegs-special-list', component: PageOlegsSpecialListComponent },

    { path:'admin', component: PageAdminIndexComponent },
    { path:'admin/userlist', component: PageAdminUserListComponent },
    { path:'admin/users/:id', component: PageAdminUserEditComponent },
    { path:'admin/sessions', component: PageAdminSessionsComponent },
    { path:'admin/metrics', component: PageAdminMetricsComponent },
    { path:'admin/orphans', component: PageAdminOrphansComponent },


    { path: '**', component: PageNotFoundComponent }
]
