import { ChangeDetectorRef, Component, OnInit } from '@angular/core'
import { LoginFormComponent } from './login-form/login-form.component'
import { AppLayoutComponent } from './app-layout/app-layout.component'
import { NgIf } from '@angular/common'
import { SessionService } from '../services/session.service'
import { ThemeService } from '../services/theme.service'
import { getLoadingObservable } from '../services/loading-interceptor'

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [LoginFormComponent, AppLayoutComponent, NgIf],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {

  loading = false

  constructor(
    private sessionService: SessionService,
    private themeService: ThemeService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.themeService.autoConfigureColorMode()
    getLoadingObservable().subscribe({
      next: n => {
        this.loading = n
        this.cdRef.detectChanges()
      }
    })
  }

  isLoggedIn(): boolean {
    // TODO check this less often
    return this.sessionService.isLoggedIn()
  }

}
