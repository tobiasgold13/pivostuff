import { NgClass, NgFor } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { ChartsService } from '../../services/charts.service'
import { OlegsSpecialListRow } from '../../services/api-types/olegs-special-list'

@Component({
  selector: 'app-page-olegs-special-list',
  standalone: true,
  imports: [NgFor, NgClass],
  templateUrl: './page-olegs-special-list.component.html',
  styleUrl: './page-olegs-special-list.component.scss'
})
export class PageOlegsSpecialListComponent implements OnInit {

  constructor(private chartsService: ChartsService) {}

  data: OlegsSpecialListRow[] = []

  ngOnInit(): void {
    this.chartsService.getOlegsSpecialList().subscribe({
      next: data => this.data = data
    })
  }

}
