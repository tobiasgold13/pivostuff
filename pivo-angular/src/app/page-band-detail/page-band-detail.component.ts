import { NgFor } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { EventComponent } from '../event/event.component'
import { BandDetails } from '../../services/api-types/band'
import { BandService } from '../../services/band.service'
import { EventOverview } from '../../services/api-types/event'

@Component({
  selector: 'app-page-band-detail',
  standalone: true,
  imports: [NgFor, EventComponent],
  templateUrl: './page-band-detail.component.html',
  styleUrl: './page-band-detail.component.scss'
})
export class PageBandDetailComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private bandService: BandService
  ) {}

  details: BandDetails = { band:{ id:-1, name:'' }, events:[] }

  ngOnInit(): void {
    const bandId=this.route.snapshot.params['id']
    this.bandService.getEvents(bandId).subscribe({
      next: details => this.details = details
    })
  }

  getSetlistUrl(e: EventOverview) {
    // TODO if there will ever be more than 1 frontend: move URL generation to backend
    const year = e.start.split('-')[0]
    let query = this.details.band.name
    
    // only add city, if date is in the past
    const start = new Date(e.start)
    start.setHours(0, 0, 0, 0)
    const today = new Date()
    today.setHours(0, 0, 0, 0)
    if (start < today) {
      query += ' ' + e.city
    }

    query = query.split(' ').join('+')
    return `https://www.setlist.fm/search?year=${year}&query=${query}`
  }

}
