import { Component } from '@angular/core'
import { RouterModule } from '@angular/router'
import { BackButtonDirective } from './back-button.directive'
import { NgClass } from '@angular/common'

@Component({
  selector: 'app-layout',
  standalone: true,
  imports: [RouterModule, BackButtonDirective, NgClass],
  templateUrl: './app-layout.component.html',
  styleUrl: './app-layout.component.scss'
})
export class AppLayoutComponent {

  hasRoundedCorners() {
    if (!/iPhone/.test(navigator.userAgent)) {
      return false
    }
    const isPwa = window.matchMedia('(display-mode: standalone)').matches ||
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (window.navigator as any).standalone === true
    const isFullscreen = document.fullscreenElement !== null
    if (!isPwa && !isFullscreen) {
      return false
    }

    const iPhoneRoundedModels = [
      { width: 375, height: 812 },  // iPhone X, XS, 11 Pro
      { width: 414, height: 896 },  // iPhone XR, 11, 11 Pro Max
      { width: 390, height: 844 },  // iPhone 12, 12 Pro
      { width: 428, height: 926 },  // iPhone 12 Pro Max, 13 Pro Max
      { width: 393, height: 852 },  // iPhone 14 Pro, 15
      { width: 430, height: 932 }   // iPhone 14 Pro Max
    ]

    return iPhoneRoundedModels.some(model =>
      (model.width === window.screen.width && model.height === window.screen.height)
    )
  }

}
