import { Injectable } from '@angular/core'
import { Location } from '@angular/common'
import { Router, NavigationEnd } from '@angular/router'

@Injectable({ providedIn: 'root' })
export class NavigationService {

    private history: string[] = []
    private backButtonUsed = false

    constructor(private router: Router, private location: Location) {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (!this.backButtonUsed) {
                    this.history.push(event.urlAfterRedirects)
                } else {
                    this.backButtonUsed = false
                }
            }
        })
    }

    back(): void {
        this.backButtonUsed = true
        this.history.pop()
        if (this.history.length > 0) {
            this.location.back()
        } else {
            const currentUrl = this.router.url.split('?')[0]
            if (currentUrl.startsWith('/admin')) {
                this.router.navigate([currentUrl.substring(0, currentUrl.lastIndexOf('/'))])
            } else if (currentUrl !== '/') {
                this.router.navigateByUrl('/')
            }
        }
    }
}