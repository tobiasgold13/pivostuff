import { CommonModule } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { SessionService } from '../../services/session.service'

@Component({
  selector: 'app-login-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  templateUrl: './login-form.component.html',
  styleUrl: './login-form.component.scss'
})
export class LoginFormComponent implements OnInit {
  constructor(
    private sessionService: SessionService,
    private formBuilder: FormBuilder
  ) {}

  debug = false
  debugData = ''

  ngOnInit(): void {
    this.debug = window.location.href.includes('debug=true')
    // if (this.debug) {
    //   this.sessionService.getDebugData().subscribe({
    //     next: d => {
    //       this.debugData = JSON.stringify(d, null, 4)
    //     },
    //     error: err => {
    //       this.debugData = JSON.stringify(err, null, 4)
    //     }
    //   })
    // }
  }

  error: string | undefined

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  })

  submit() {
    if (!this.loginForm.value.username || !this.loginForm.value.password) {
      this.error = 'Benutzername/Passwort eingeben'
      return
    }

    this.sessionService
      .login({
        username: this.loginForm.value.username,
        password: this.loginForm.value.password
      })
      .subscribe({
        next: () => {
          this.loginForm.reset()
        },
        error: () => {
          this.error = 'Login fehlgeschlagen'
        }
      })
  }
}
