import { Component, Input } from '@angular/core'
import { EventOverview } from '../../services/api-types/event'
import { RouterLink } from '@angular/router'
import { DateComponent } from '../date/date.component'
import { NgClass, NgIf } from '@angular/common'

@Component({
  selector: 'app-event',
  standalone: true,
  imports: [RouterLink, DateComponent, NgIf, NgClass],
  templateUrl: './event.component.html',
  styleUrl: './event.component.scss'
})
export class EventComponent {

  @Input({ required: true }) event!: EventOverview
  @Input({ required: false }) dateFormat?: string = undefined

}
