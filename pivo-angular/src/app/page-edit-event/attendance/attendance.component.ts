import { Component, Input } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { PartialAttendance, PartialBand } from '../../../services/api-types/add-edit-event'
import { BandService } from '../../../services/band.service'
import { NgFor } from '@angular/common'
import { NgbTypeaheadModule, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap'
import { debounceTime, distinctUntilChanged, Observable, OperatorFunction, switchMap } from 'rxjs'
import { DateComponent } from '../../date/date.component'

@Component({
  selector: 'app-attendance',
  standalone: true,
  imports: [NgFor, FormsModule, NgbTypeaheadModule, DateComponent],
  templateUrl: './attendance.component.html',
  styleUrl: './attendance.component.scss'
})
export class AttendanceComponent {

  constructor(private bandService: BandService) { }

  @Input({ required: true }) attendance!: PartialAttendance

  suggestions: PartialBand[] = []
  formatter = (v: PartialBand) => v.name ?? ''
  inputValue = ''

  removeBand(band: PartialBand) {
    this.attendance.bands = this.attendance.bands.filter(b => b !== band)
  }

  search: OperatorFunction<string, PartialBand[]> = (text: Observable<string>) =>
    text.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(term => term !== '' ? this.bandService.searchBands(term) : [])
    )

  bandSelected(e: NgbTypeaheadSelectItemEvent) {
    e.preventDefault()
    this.attendance.bands.push(e.item)
    this.inputValue = ''
  }

  addNewBand() {
    this.attendance.bands.push({ name: this.inputValue })
    this.inputValue = ''
  }

}
