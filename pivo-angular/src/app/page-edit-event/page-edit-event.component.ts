import { Component, EventEmitter, inject, Input, OnInit, Output } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AttendanceComponent } from './attendance/attendance.component'
import { EditLocationComponent } from './edit-location/edit-location.component'
import { FormsModule } from '@angular/forms'
import {
  PartialAttendance,
  PartialEventDetail,
  PartialLocation
} from '../../services/api-types/add-edit-event'
import { EventService, getEventCategoryLocalization } from '../../services/event.service'
import { NgbAccordionModule, NgbCalendar, NgbDate, NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap'
import { fromDateModel, toDateModel, toDateString } from './date-conversion'
import { Router } from '@angular/router'
import { EventCategory } from '../../services/api-types/event'

@Component({
  selector: 'app-edit-event',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NgbAccordionModule,
    NgbDatepickerModule,
    AttendanceComponent,
    EditLocationComponent
  ],
  templateUrl: './page-edit-event.component.html',
  styleUrl: './page-edit-event.component.scss'
})
export class PageEditEventComponent implements OnInit {

  constructor(
    private eventService: EventService,
    private router: Router
  ) {}

  @Input() event: PartialEventDetail = {
    name: '',
    start: '',
    end: '',
    category: undefined,
    location: {
      name: '',
      city: '',
      country: ''
    },
    attendances: []
  }
  @Output() cancelEvent = new EventEmitter<void>()
  
  availableCategories = [undefined, EventCategory.concert, EventCategory.festival]

  calendar = inject(NgbCalendar)
  hoveredDate: NgbDate | null = null
  fromDate: NgbDate = this.calendar.getToday()
  toDate: NgbDate | null = this.calendar.getToday()

  errorState = false

  ngOnInit(): void {
    this.event = structuredClone(this.event)
    if (this.event.start) {
      this.fromDate = fromDateModel(this.event.start)
    }
    if (this.event.end) {
      this.toDate = fromDateModel(this.event.end)
    }
  }

	onDateSelection(date: NgbDate) {
		if (!this.fromDate && !this.toDate) {
			this.fromDate = date
		} else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
			this.toDate = date
		} else {
			this.toDate = null
			this.fromDate = date
		}
	}

	isHovered(date: NgbDate) {
		return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate)
	}

	isInside(date: NgbDate) {
		return this.toDate && date.after(this.fromDate) && date.before(this.toDate)
	}

	isRange(date: NgbDate) {
		return (
			date.equals(this.fromDate) ||
			(this.toDate && date.equals(this.toDate)) ||
			this.isInside(date) ||
			this.isHovered(date)
		)
	}

  public updateAttendances() {
    this.event.start = toDateModel(this.fromDate)
    this.event.end = this.toDate ? toDateModel(this.toDate) : this.event.start

    // calculate new attendances
    const currentDate = new Date(this.event.start)
    const endDate = new Date(this.event.end)
    let attendances: PartialAttendance[] = []
    do {
      const currentDateString = toDateString(currentDate)
      attendances = [...attendances, { date: currentDateString, bands: [] }]
      currentDate.setDate(currentDate.getDate() + 1)
    } while (currentDate <= endDate)

    // merge attendances
    const mergedAttendances = []
    const oldAttendances = this.event.attendances.map(a => a.date)
    const newAttendances = attendances.map(a => a.date)
    // keep attendances, that haven't changed
    for (const a of this.event.attendances) {
      if (newAttendances.includes(a.date)) {
        mergedAttendances.push(a)
      }
    }
    // add new attendances
    for (const a of attendances) {
      if (!oldAttendances.includes(a.date)) {
        mergedAttendances.push(a)
      }
    }
    // sort, just in case
    this.event.attendances = mergedAttendances.sort((a, b) => a.date.localeCompare(b.date))
  }

  locationChanged(location: PartialLocation) {
    this.event.location = location
  }

  submit(): void {
    this.updateAttendances()
    this.eventService.saveEvent(this.event).subscribe({
      next: res => window.location.href = `/events/${res.id}`,
      error: () => this.errorState = true
    })
  }

  cancel(): void {
    if (this.event.id) {
      this.cancelEvent.emit()
    } else {
      this.router.navigate(['/'])
    }
  }

  getEventCategoryLocalization(c: EventCategory | undefined) {
    return getEventCategoryLocalization(c)
  }
}
