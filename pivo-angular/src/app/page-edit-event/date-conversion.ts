import { NgbDate } from '@ng-bootstrap/ng-bootstrap'

export const toDateString = (d: Date): string => {
  const local = new Date(d)
  local.setMinutes(d.getMinutes() - d.getTimezoneOffset())
  return local.toJSON().slice(0, 10)
}

const DELIMITER = '-'

export const fromDateModel = (s: string): NgbDate => {
  const date = s.split(DELIMITER)
  return new NgbDate(
    parseInt(date[0], 10),
    parseInt(date[1], 10),
    parseInt(date[2], 10)
  )
}

export const toDateModel = (date: NgbDate): string => {
  const day = date.day < 10 ? '0' + date.day : date.day
  const month = date.month < 10 ? '0' + date.month : date.month
  return date.year + DELIMITER + month + DELIMITER + day
}
