import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { LocationService } from '../../../services/location.service'
import { PartialLocation } from '../../../services/api-types/add-edit-event'
import { NgbTypeaheadModule, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap'
import { Observable, OperatorFunction } from 'rxjs'
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-edit-location',
  standalone: true,
  imports: [NgbTypeaheadModule, ReactiveFormsModule],
  templateUrl: './edit-location.component.html',
  styleUrl: './edit-location.component.scss'
})
export class EditLocationComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private locationService: LocationService
  ) {}

  @Output() locationChangedEvent = new EventEmitter<PartialLocation>()
  @Input() location: PartialLocation = { name:'', city:'', country:'' }
  @Input({ required: false }) showValidation = false

  
  locationForm = this.formBuilder.group({
    name: [''],
    city: ['', Validators.required],
    country: ['', Validators.required]
  })
  
  ngOnInit(): void {
    this.locationForm.patchValue(this.location)
  }
  
  formatter = (v: PartialLocation) => (v.name ? `${v.name} - ` : '') + `${v.city} (${v.country})`

  getInputStatusClass(controlName: string): string {
    if (!this.showValidation) {
      return ''
    }
    if (this.locationForm.get(controlName)?.errors) {
      return 'is-invalid'
    }
    return 'is-valid'
  }

  search: OperatorFunction<string, PartialLocation[]> = (text: Observable<string>) =>
		text.pipe(
			debounceTime(500),
			distinctUntilChanged(),
      switchMap( term => term !== '' ? this.locationService.searchLocations(term) : [])
		)

  locationSelected(e: NgbTypeaheadSelectItemEvent) {
    e.preventDefault()
    this.locationForm.patchValue(e.item)
    this.locationChangedEvent.emit(e.item)
  }
  
  detailsChanged() {
    const location: PartialLocation = { 
      name: this.locationForm.value.name ?? undefined,
      city: this.locationForm.value.city!,
      country: this.locationForm.value.country!
    }
    this.locationChangedEvent.emit(location)
  }

}
