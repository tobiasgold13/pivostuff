import { Component, OnInit } from '@angular/core'
import { AdminUserService } from '../../../services/admin-user.service'
import { AdminUser } from '../../../services/api-types/admin-users'
import { NgClass } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'

@Component({
  selector: 'app-admin-user-edit',
  standalone: true,
  imports: [ReactiveFormsModule, NgClass],
  templateUrl: './page-admin-user-edit.component.html',
  styleUrl: './page-admin-user-edit.component.scss'
})
export class PageAdminUserEditComponent implements OnInit {
  
  constructor(
    private route: ActivatedRoute,
    private userService: AdminUserService,
    private formBuilder: FormBuilder
  ) { }

  user: AdminUser = { id: -1, name:'' }

  editUserForm = this.formBuilder.group({
    name: ['', Validators.required],
    roles: ['']
  })

  newPasswordForm = this.formBuilder.group({
    password: ['', Validators.required]
  })

  users: AdminUser[] = []

  ngOnInit(): void {
    const id=this.route.snapshot.params['id']
    this.userService.getUser(id).subscribe({
      next: user => {
        this.user = user
        this.editUserForm.patchValue(user)
      },
      error: () => window.location.href = '/admin/userlist'
    })
  }

  saveUser(): void {
    if (!this.editUserForm.value.name) {
      return
    }

    this.userService.modifyUser(this.user, {
      name: this.editUserForm.value.name,
      roles: this.editUserForm.value.roles ?? undefined
    }).subscribe({
      next: () => {
        this.editUserForm.reset()
        this.ngOnInit()
      },
      error: () => alert('Fehler beim Speichern!')
    })
  }

  changePassword(): void {
    if (!this.newPasswordForm.value.password) {
      return
    }
    
    this.userService.modifyPassword(this.user, { password: this.newPasswordForm.value.password }).subscribe({
      next: () => {
        this.newPasswordForm.reset()
        this.ngOnInit()
      },
      error: () => alert('Fehler beim Speichern!')
    })
  }

}
