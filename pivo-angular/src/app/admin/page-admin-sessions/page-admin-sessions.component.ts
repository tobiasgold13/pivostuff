import { Component, OnInit } from '@angular/core'
import { AdminSessionService } from '../../../services/admin-sessions.service'
import { NgFor } from '@angular/common'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ConfirmationDialogComponent, modalConfig } from '../confirmation-dialog/confirmation-dialog.component'
import { AdminSessionsActiveUser } from '../../../services/api-types/admin-sessions'

@Component({
  selector: 'app-admin-sessions',
  standalone: true,
  imports: [NgFor],
  templateUrl: './page-admin-sessions.component.html',
  styleUrl: './page-admin-sessions.component.scss'
})
export class PageAdminSessionsComponent implements OnInit {

  generalData = {
    totalSessionsCount: -1,
    ttl: ''
  }
  usersWithSessions: AdminSessionsActiveUser[] = []

  constructor(
    private sessionService: AdminSessionService,
    private modal: NgbModal
  ) { }

  ngOnInit(): void {
    this.sessionService.getGeneralData().subscribe({
      next: res => this.generalData = res
    })
    this.sessionService.getSessions().subscribe({
      next: res => this.usersWithSessions = res
    })
  }

  killSessions(u: AdminSessionsActiveUser) {
    const modalRef = this.modal.open(ConfirmationDialogComponent, modalConfig)
    modalRef.componentInstance.message = `Alle Sessions von "${u.name}" wirklich beenden?`
    modalRef.closed.subscribe({
      next: () => this.performKillSessions(u)
    })
  }

  private performKillSessions(u: AdminSessionsActiveUser) {
    this.sessionService.deleteSessionsForUser(u.id).subscribe({
      next: () => this.ngOnInit()
    })
  }

  cleanSessions(): void {
    this.sessionService.cleanExpiredSessions().subscribe({
      next: () => this.ngOnInit()
    })
  }

  // instead of returning a formatted UTC timestamp and having to re-format it here again, just use the unix style seconds since 1970
  formatUnixTimestamp(ts: number): string {
    const d = new Date(ts * 1000)
    const datePortion = d.toLocaleDateString('de-DE', {
      year: '2-digit',
      month: '2-digit',
      day: '2-digit'
    })
    return `${datePortion} ${d.toLocaleTimeString('de-DE')}`
  }

}
