import { Component, OnInit } from '@angular/core'
import { RouterLink } from '@angular/router'
import { NgFor, NgIf } from '@angular/common'
import { AdminOrphansService } from '../../../services/admin-orphans.service'
import { AdminOrhpansResponse } from '../../../services/api-types/admin-orphans'
import { BandComponent } from '../../band/band.component'
import { LocationComponent } from '../../location/location.component'
import { Band } from '../../../services/api-types/band'
import { Location } from '../../../services/api-types/location'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ConfirmationDialogComponent, modalConfig } from '../confirmation-dialog/confirmation-dialog.component'

@Component({
  selector: 'app-admin-orphans',
  standalone: true,
  imports: [RouterLink, NgFor, NgIf, BandComponent, LocationComponent],
  templateUrl: './page-admin-orphans.component.html',
  styleUrl: './page-admin-orphans.component.scss'
})
export class PageAdminOrphansComponent implements OnInit {

  constructor(
    private orphansService: AdminOrphansService,
    private modal: NgbModal
  ) {}

  data: AdminOrhpansResponse = { bands:[], locations:[] }

  ngOnInit(): void {
    this.orphansService.getOrphans().subscribe({
      next: res => this.data = res
    })
  }

  deleteBand(b: Band): void {
    const modalRef = this.modal.open(ConfirmationDialogComponent, modalConfig)
    modalRef.componentInstance.message = 'Band wirklich löschen?'
    modalRef.closed.subscribe({
      next: () => this.performDeleteBand(b)
    })
  }

  private performDeleteBand(b: Band) {
    this.orphansService.deleteBand(b.id).subscribe({
      next: () => this.ngOnInit()
    })
  }

  deleteLocation(l: Location): void {
    const modalRef = this.modal.open(ConfirmationDialogComponent, modalConfig)
    modalRef.componentInstance.message = 'Location wirklich löschen?'
    modalRef.closed.subscribe({
      next: () => this.performDeleteLocation(l)
    })
  }

  private performDeleteLocation(l: Location) {
    this.orphansService.deleteLocation(l.id).subscribe({
      next: () => this.ngOnInit()
    })
  }

}
