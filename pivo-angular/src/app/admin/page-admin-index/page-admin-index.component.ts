import { Component } from '@angular/core'
import { RouterLink } from '@angular/router'

@Component({
  selector: 'app-admin-index',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './page-admin-index.component.html',
  styleUrl: './page-admin-index.component.scss'
})
export class PageAdminIndexComponent {

}
