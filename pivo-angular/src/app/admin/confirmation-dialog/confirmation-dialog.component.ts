import { Component, Input } from '@angular/core'
import {
  NgbActiveModal,
  NgbModalOptions
} from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-confirmation-dialog',
  standalone: true,
  imports: [],
  templateUrl: './confirmation-dialog.component.html',
  styleUrl: './confirmation-dialog.component.scss'
})
export class ConfirmationDialogComponent {

  @Input() message = 'Wirklich ausführen?'

  constructor(public activeModal: NgbActiveModal) {}
}

export const modalConfig: NgbModalOptions = {
  backdrop: 'static',
  animation: true,
  centered: true,
  size: 'sm'
}
