import { Component, OnInit } from '@angular/core'
import { RouterLink } from '@angular/router'
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap'
import { AdminMetricsService } from '../../../services/admin-metrics.service'
import { BaseChartDirective, provideCharts, withDefaultRegisterables } from 'ng2-charts'
import { ChartConfiguration } from 'chart.js'
import { AdminMetricsCurrentResponse, AdminMetricsGroupCountPerUser } from '../../../services/api-types/admin-metrics'

@Component({
  selector: 'app-admin-metrics',
  standalone: true,
  imports: [RouterLink, BaseChartDirective, NgbAccordionModule],
  templateUrl: './page-admin-metrics.component.html',
  styleUrl: './page-admin-metrics.component.scss',
  providers: [provideCharts(withDefaultRegisterables())]
})
export class PageAdminMetricsComponent implements OnInit {

  constructor(private metricsService: AdminMetricsService) { }

  updateMetricsButtonDisabled = false
  overallEventsData: ChartConfiguration['data'] | undefined
  overallTotalShowsData: ChartConfiguration['data'] | undefined

  currentDataLoaded = false
  passedEventCountData: ChartConfiguration['data'] | undefined
  passedDistinctBandCountData: ChartConfiguration['data'] | undefined
  passedTotalShowsCountData: ChartConfiguration['data'] | undefined
  passedEventsByCategoryCountData: ChartConfiguration['data'] | undefined

  ngOnInit(): void {
    // initialize with empty array, because chart.js rendering breaks if lazy loaded for some reason...
    this.processStatsData({ passedEvents:[], passedDistinctBands:[], passedTotalShows:[], passedEventsByCategory:[] })
  }

  updateMetricHistory(): void {
    this.updateMetricsButtonDisabled = true
    this.metricsService.updateMetricsHistory().subscribe({
      next: () => {
        setTimeout(() => this.updateMetricsButtonDisabled = false, 2500)
        this.overallEventsData = undefined
        this.overallTotalShowsData = undefined
      }
    })
  }

  loadOverallEvents(): void {
    if (this.overallEventsData) {
      return
    }

    this.metricsService.getEventsOverTime().subscribe({
      next: data => {
        this.overallEventsData = this.createChartsData(data)
      }
    })
  }

  loadOverallTotalShows(): void {
    if (this.overallTotalShowsData) {
      return
    }

    this.metricsService.getShowsOverTime().subscribe({
      next: data => {
        this.overallTotalShowsData = this.createChartsData(data)
      }
    })
  }

  loadCurrentData(): void {
    if (this.currentDataLoaded) {
      return
    }

    this.metricsService.getStats().subscribe({
      next: data => {
        this.currentDataLoaded = true
        this.processStatsData(data)
      }
    })
  }

  private processStatsData(data: AdminMetricsCurrentResponse): void {
    // passed event count
    this.passedEventCountData = {
      labels: data.passedEvents.map(el => el.username),
      datasets: [
        {
          data: data.passedEvents.map(el => el.count),
          borderWidth: 1
        }
      ]
    }
    // passed distinct band count
    this.passedDistinctBandCountData = {
      labels: data.passedDistinctBands.map(el => el.username),
      datasets: [
        {
          data: data.passedDistinctBands.map(el => el.count),
          borderWidth: 1
        }
      ]
    }
    // passed total shows count
    this.passedTotalShowsCountData = {
      labels: data.passedTotalShows.map(el => el.username),
      datasets: [
        {
          data: data.passedTotalShows.map(el => el.count),
          borderWidth: 1
        }
      ]
    }
    // passed events by category count
    for (const categoryElement of data.passedEventsByCategory) {
      if (!categoryElement.group) {
        categoryElement.group = ''
      }
    }
    this.passedEventsByCategoryCountData = this.createChartsData(data.passedEventsByCategory)
  }

  private createChartsData(input: AdminMetricsGroupCountPerUser[]): ChartConfiguration['data'] {
    const output: ChartConfiguration['data'] = {
      labels: input.map(el => el.group).filter((value, index, array) => array.indexOf(value) === index),
      datasets: []
    }

    const usernames = input.map(el => el.username).filter((value, index, array) => array.indexOf(value) === index)
    for (const un of usernames) {
      const dataset: number[] = []
      output.datasets.push({ label: un, data: dataset, spanGaps: true })
      for (const category of output.labels!) {
        const value = input.filter(c => c.username === un).filter(c => c.group === category)
        dataset.push(value.length > 0 ? value[0].count : NaN)
      }
    }

    return output
  }

}
