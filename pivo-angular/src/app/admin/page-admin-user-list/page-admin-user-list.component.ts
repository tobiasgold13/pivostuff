import { Component, OnInit } from '@angular/core'
import { AdminUserService } from '../../../services/admin-user.service'
import { AdminUser } from '../../../services/api-types/admin-users'
import { NgFor } from '@angular/common'
import { RouterLink } from '@angular/router'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import {
  ConfirmationDialogComponent,
  modalConfig
} from '../confirmation-dialog/confirmation-dialog.component'

@Component({
  selector: 'app-admin-user-list',
  standalone: true,
  imports: [NgFor, RouterLink, ReactiveFormsModule],
  templateUrl: './page-admin-user-list.component.html',
  styleUrl: './page-admin-user-list.component.scss'
})
export class PageAdminUserListComponent implements OnInit {
  constructor(
    private adminUserService: AdminUserService,
    private formBuilder: FormBuilder,
    private modal: NgbModal
  ) {}

  addUserForm = this.formBuilder.group({
    name: ['', Validators.required],
    password: ['', Validators.required],
    roles: ['']
  })

  users: AdminUser[] = []

  ngOnInit(): void {
    this.adminUserService.getAllUsers().subscribe({
      next: (users) => (this.users = users)
    })
  }

  deleteUser(u: AdminUser) {
    const modalRef = this.modal.open(ConfirmationDialogComponent, modalConfig)
    modalRef.componentInstance.message = `Benutzer "${u.name}" wirklich löschen?`
    modalRef.closed.subscribe({
      next: () => this.performDeleteUser(u)
    })
  }

  private performDeleteUser(u: AdminUser) {
    this.adminUserService.deleteUser(u.id).subscribe({
      next: () => this.ngOnInit()
    })
  }

  getInputStatusClass(controlName: string): string {
    if (!this.addUserForm.get(controlName)?.dirty) {
      return ''
    }
    if (this.addUserForm.get(controlName)?.errors) {
      return 'is-invalid'
    }
    return 'is-valid'
  }

  addUser() {
    if (!this.addUserForm.value.name || !this.addUserForm.value.password) {
      return
    }

    this.adminUserService
      .addUser({
        name: this.addUserForm.value.name,
        password: this.addUserForm.value.password,
        roles: this.addUserForm.value.roles ?? undefined
      })
      .subscribe({
        next: () => {
          this.addUserForm.reset()
          this.ngOnInit()
        }
      })
  }
}
