import { NgIf } from '@angular/common'
import { Component } from '@angular/core'
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms'
import { UserService } from '../../services/user.service'
import { SessionService } from '../../services/session.service'

@Component({
  selector: 'app-page-change-password',
  standalone: true,
  imports: [NgIf, ReactiveFormsModule],
  templateUrl: './page-change-password.component.html',
  styleUrl: './page-change-password.component.scss'
})
export class PageChangePasswordComponent {

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private sessionService: SessionService
  ) { }

  error?: string

  changePasswordForm = this.formBuilder.group({
    oldPassword: ['', Validators.required],
    newPassword1: ['', Validators.required],
    newPassword2: ['', Validators.required]
  })

  getInputStatusClass(controlName: string): string {
    if (!this.changePasswordForm.get(controlName)?.dirty) {
      return ''
    }
    if (this.changePasswordForm.get(controlName)?.errors) {
      return 'is-invalid'
    }
    return 'is-valid'
  }

  submit() {
    this.error = undefined
    if (!this.changePasswordForm.valid || !this.changePasswordForm.value.oldPassword || !this.changePasswordForm.value.newPassword1) {
      return
    }

    if (this.changePasswordForm.value.newPassword1 !== this.changePasswordForm.value.newPassword2) {
      this.error = 'Passwörter müssen identisch sein!'
      this.changePasswordForm.get('newPassword1')?.setErrors({})
      this.changePasswordForm.get('newPassword2')?.setErrors({ msg: 'passwords not equal' })
      return
    }

    this.userService
      .changePassword({
        oldPassword: this.changePasswordForm.value.oldPassword,
        newPassword: this.changePasswordForm.value.newPassword1
      })
      .subscribe({
        next: () => {
          this.sessionService.logout()
        },
        error: () => {
          this.error = 'Passwort konnte nicht geändert werden.'
        }
      })
  }

}
