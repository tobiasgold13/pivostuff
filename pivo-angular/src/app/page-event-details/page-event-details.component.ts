import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { EventDetail } from '../../services/api-types/event'
import { EventService, getEventCategoryLocalization } from '../../services/event.service'
import { NgFor, NgIf } from '@angular/common'
import { BandComponent } from '../band/band.component'
import { PageEditEventComponent } from '../page-edit-event/page-edit-event.component'
import { DateComponent } from '../date/date.component'
import { LocationComponent } from '../location/location.component'

@Component({
  selector: 'app-page-event-details',
  standalone: true,
  imports: [NgFor, NgIf, BandComponent, LocationComponent, PageEditEventComponent, DateComponent],
  templateUrl: './page-event-details.component.html',
  styleUrl: './page-event-details.component.scss'
})
export class PageEventDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private eventService: EventService
  ) {}

  event: EventDetail = { id:-1, name:'', start:'', end:'', category: undefined, location:{ id:-1, name:'', city:'', country:'' }, attendances:[] }
  editMode = false

  ngOnInit(): void {
    const eventId = this.route.snapshot.params['id']
    this.eventService.getEventDetail(eventId).subscribe({
      next: event => this.event = event
    })
    const edit = this.route.snapshot.queryParams['edit']
    if (edit === 'true') {
      this.setEdit(true)
    }
  }

  setEdit(edit: boolean): void {
    this.editMode = edit
    this.router.navigate(
      [], 
      {
        relativeTo: this.route,
        queryParams: edit ? { edit: 'true' } : {}, 
        queryParamsHandling: 'replace'
      }
    )
  }

  getEventCategory() {
    return getEventCategoryLocalization(this.event.category)
  }

}
