import { fillLoginForm } from '../support/login'

describe('Olegs Special List Page', () => {
  it('menu page should contain link to charts page', () => {
    cy.visit('/menu')
    fillLoginForm()

    cy.contains('Oleg\'s Special List').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/olegs-special-list`)
    cy.contains('Oleg\'s Special List')
    cy.contains('voll krass Einfärbung')
  })

  it('olegs list should display correct data', () => {
    cy.visit('/olegs-special-list')
    fillLoginForm('charts-statistic')

    cy.contains('00-Band 10-19').parents('tr').should('have.text', '1.00-Band 10-19 10x')
    cy.contains('00-Band 10-19').should('have.css', 'background-color', 'rgb(135, 206, 235)')
    cy.contains('00-From 5 Till 9').parents('tr').should('have.text', '2.00-From 5 Till 9 5x')
    cy.contains('00-From 5 Till 9').should('have.css', 'background-color', 'rgb(255, 255, 0)')
    cy.contains('00-One Two Four').parents('tr').should('have.text', '3.00-One Two Four 1x')
    cy.contains('00-One Two Four').should('have.css', 'background-color', 'rgba(0, 0, 0, 0)')
    cy.contains('00-Plus 20').parents('tr').should('have.text', '4.00-Plus 20 20x')
    cy.contains('00-Plus 20').should('have.css', 'background-color', 'rgb(255, 160, 122)')
  })
})