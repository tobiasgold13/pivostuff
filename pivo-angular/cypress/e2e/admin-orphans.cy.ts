import { fillLoginForm } from '../support/login'

describe('admin -> orphans', () => {
  it('admin index should link to orphans', () => {
    cy.visit('/admin')
    fillLoginForm('admin')

    cy.contains('Orphans').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/admin/orphans`)
    cy.contains('Orphans')
  })
  
  it('should display "none found" message', () => {
    cy.visit('/admin/orphans')
    fillLoginForm('admin')
    cy.contains('No orphaned entities')
  })
  
  it('lets create and handle some orphans', () => {
    cy.visit('/events/7')
    fillLoginForm('admin')
    cy.contains('Event For The Orphans')
    // create orphans
    cy.contains('Edit').click()
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type(new Date().toISOString())
    })
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.contains('The Orphans').parent().children('span[role = "button"]').click()
    })
    cy.contains('Speichern').click()
    
    // handle orphans
    cy.visit('/admin/orphans')
    cy.contains('Bands')
    cy.contains('The Orphans').parents('tr').find('button').click()
    cy.contains('Band wirklich löschen?')
    cy.get('button').contains('OK').click()
    cy.contains('Locations')
    cy.contains('Grindavik').parents('tr').find('button').click()
    cy.contains('Location wirklich löschen?')
    cy.get('button').contains('OK').click()
    cy.contains('No orphaned entities')
    
    // restore data
    cy.visit('/events/7')
    cy.contains('Edit').click()
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.get('input').type('The Orphans').type('{enter}')
    })
    cy.contains('Speichern').click()
  })

})
