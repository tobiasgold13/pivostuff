import { fillLoginForm } from '../support/login'

describe('admin -> session management', () => {
  it('admin index should link to session management', () => {
    cy.visit('/admin')
    fillLoginForm('admin')

    cy.contains('Sessions').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/admin/sessions`)
    cy.contains('Sessions')
    cy.contains('Count:')
    cy.contains('Time-To-Live: 5h')
    cy.get('button').contains('Clean Sessions')
    cy.contains('Aktive Benutzer')
  })
  
  it('should, at least, contain the session for the current user', () => {
    cy.visit('/admin/sessions')
    fillLoginForm('admin')

    const adminParent = cy.contains('admin').parent().parent().parent()
    adminParent.within(() => {
      cy.contains('Sessions:')
      cy.contains('Chrome')
      cy.get('button').contains('X').click()
    })
    cy.contains('Alle Sessions von "admin" wirklich beenden?')
    cy.get('button').contains('OK').click()
    cy.contains('Log In')
  })
})
