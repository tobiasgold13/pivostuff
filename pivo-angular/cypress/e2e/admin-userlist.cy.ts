import { fillLoginForm } from '../support/login'

describe('admin -> user area', () => {
  it('admin index should link to user management', () => {
    cy.visit('/admin')
    fillLoginForm('admin')

    cy.contains('Benutzerverwaltung').click()
    cy.contains('Benutzerverwaltung')
    cy.contains('Benutzer hinzufügen')
    cy.url().should('equal', `${Cypress.config().baseUrl}/admin/userlist`)
  })

  it('add user should work', () => {
    cy.visit('/admin/userlist')
    fillLoginForm('admin')

    cy.get('button[type=submit]').contains('Hinzufügen').should('be.disabled')
    cy.contains('Benutzername').siblings('input').type('admin-to-be-deleted')
    cy.contains('Passwort').siblings('input').type('pivoleg')
    cy.contains('Roles').siblings('input').type('ADMIN')
    cy.get('button[type=submit]').contains('Hinzufügen').click()
    cy.contains('admin-to-be-deleted')
  })

  it('deletion of the current user should result in log out', () => {
    cy.visit('/admin/userlist')
    fillLoginForm('admin-to-be-deleted')

    // try with cancel
    cy.contains('admin-to-be-deleted').siblings('button').click()
    cy.contains('wirklich löschen?')
    cy.get('button').contains('Abbrechen').click()
    // try with confirm
    cy.contains('admin-to-be-deleted').siblings('button').click()
    cy.contains('wirklich löschen?')
    cy.get('button').contains('OK').click()
    cy.contains('Log In')
  })
})
