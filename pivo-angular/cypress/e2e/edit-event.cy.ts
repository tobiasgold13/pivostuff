import { fillLoginForm } from '../support/login'

describe('Event Edit', () => {
  it('edit some event attributes', () => {
    // go to test event
    cy.visit('/events/4')
    fillLoginForm()

    // make sure event is wrong
    cy.contains('Wrong Event')
    cy.contains('12.02. - 15.02.2022')
    cy.contains('Tolmin - Tolmin (Slowenien)')
    cy.contains('Landmvrks')
    
    // edit and change stuff
    cy.contains('Edit').click()
    cy.contains('Bands').click()
    cy.get('app-attendance').should('have.length', 4)
    cy.contains('Veranstaltungsdaten').click()

    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('Right name')
      cy.contains('Kategorie').siblings('select').select('Festival')
      cy.get('div[role = "gridcell"]').contains('13').click()
      cy.get('div[role = "gridcell"]').contains('15').click()
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('paun')
      cy.contains('Paunchy Cats').click()
    })
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.get('app-attendance').should('have.length', 3)
      cy.contains('14.').parents('app-attendance').find('input').type('slaye')
      cy.contains('Slayer').click()
      cy.contains('Landmvrks').parent().children('span[role = "button"]').click()
    })
    cy.contains('Speichern').click()
    
    // make sure stuff is saved
    cy.contains('Right Name')
    cy.contains('Kategorie: Festival')
    cy.contains('13.02. - 15.02.2022')
    cy.contains('Paunchy Cats - Lichtenfels (Deutschland)')
    cy.contains('Slayer')
    cy.contains('Landmvrks').should('not.exist')
    
    // reset in order to have the test runnable multiple times
    cy.contains('Edit').click()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('Wrong event')
      cy.contains('Kategorie').siblings('select').select('')
      cy.get('div[role = "gridcell"]').contains('12').click()
      cy.get('div[role = "gridcell"]').contains('15').click()
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('tolmin')
      cy.contains('Tolmin').click()
    })
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.get('app-attendance').should('have.length', 4)
      cy.contains('Slayer').parent().children('span[role = "button"]').click()
      cy.contains('14.').parents('app-attendance').find('input').type('Landmvrks')
      cy.contains('Landmvrks').click()
    })
    cy.contains('Speichern').click()
  })
  
  it('edit nullable fields', () => {
    // go to test event
    cy.visit('/events/5')
    fillLoginForm()

    // make sure event is wrong
    cy.contains('Kategorie E')
    cy.contains('Paunchy Cats - Lichtenfels (Deutschland)')
    
    // edit and change stuff
    cy.contains('Edit').click()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear()
      cy.contains('Kategorie').siblings('select').select('')
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear()
      cy.contains('Ort/Stadt').siblings('input').clear().type('Cdmx')
      cy.contains('Land').siblings('input').clear().type('Mexiko')
    })
    cy.contains('Speichern').click()
    
    // make sure stuff is saved
    cy.contains('Kategorie E').should('not.exist')
    cy.contains('Kategorie:').should('not.exist')
    cy.contains('Cdmx (Mexiko)')
    
    // reset in order to have the test runnable multiple times
    cy.contains('Edit').click()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('Kategorie E')
      cy.contains('Kategorie').siblings('select').select('Konzert')
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('paunchy')
      cy.contains('Paunchy').click()
    })
    cy.contains('Speichern').click()
  })
  
  it('save event with null fields', () => {
    // go to test event
    cy.visit('/events/6')
    fillLoginForm()

    // data before edit
    cy.contains('ein Ort (irgendwo)')
    
    // just click edit and change nothing
    cy.contains('Edit').click()
    cy.contains('Speichern').click()
    
    // make sure stuff is still the same and null fields didn't break anything
    cy.contains('ein Ort (irgendwo)')
  })

  it('new bands can be added by pressing enter', () => {
    // go to test event
    cy.visit('/events/4')
    fillLoginForm()

    cy.contains('Edit').click()
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      const dateString = new Date().toISOString().toLowerCase()
      cy.get('app-attendance').first().find('input').type('band'+dateString+'{enter}')
      cy.contains('band'+dateString).parent().children('span[role = "button"]').should('have.text', 'X')
    })
  })

  it('should add and remove edit mode param to url', () => {
    // go to test event
    cy.visit('/events/1')
    fillLoginForm()

    cy.contains('Edit').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/events/1?edit=true`)
    cy.contains('Abbrechen').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/events/1`)
  })

  it('should enter event in edit mode, if parameter is set in url', () => {
    // go to test event
    cy.visit('/events/1?edit=true')
    fillLoginForm()
    cy.contains('Veranstaltung bearbeiten')
  })

})
