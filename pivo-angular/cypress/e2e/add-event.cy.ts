import { fillLoginForm } from '../support/login'

describe('Event Add Page', () => {
  beforeEach(() => {
    cy.visit('/add')
    fillLoginForm()
  })

  it('selecting a single date, should render a single attendance selection', () => {
    cy.get('div[role = "gridcell"]').contains('14').click()
    cy.get('body').click(0, 0)
    cy.contains('Bands').click()
    cy.contains('14.')
    cy.get('app-attendance').should('have.length', 1)
  })

  it('selecting a date range, should render attendance selection for each date', () => {
    cy.get('div[role = "gridcell"]').contains('14').click()
    cy.get('div[role = "gridcell"]').contains('16').click()
    cy.contains('Bands').click()
    cy.contains('14.')
    cy.contains('15.')
    cy.contains('16.')
    cy.get('app-attendance').should('have.length', 3)
  })

  it('changing the date range, should keep the unchanged attendances', () => {
    // select initial date range
    cy.get('div[role = "gridcell"]').contains('14').click()
    cy.get('div[role = "gridcell"]').contains('15').click()
    cy.contains('Bands').click()
    cy.get('app-attendance').should('have.length', 2)
    // select some bands
    cy.contains('14.').parents('app-attendance').find('input').type('land')
    cy.contains('Landmvrks').click()
    cy.contains('14.').parents('app-attendance').find('input').type('slay')
    cy.contains('Slayer').click()
    // change date range
    cy.contains('Veranstaltungsdaten').click()
    cy.get('div[role = "gridcell"]').contains('13').click()
    cy.get('div[role = "gridcell"]').contains('14').click()
    // make sure the one date that didn't change, still has the selected band
    cy.contains('Bands').click()
    cy.get('app-attendance').should('have.length', 2)
    cy.contains('13.')
    cy.contains('14.').parents('app-attendance').contains('Landmvrks')
  })

  it('location name input should autocomplete existing locations', () => {
    const location = cy.contains('Location')
    location.click()
    location.parents('.accordion-item').within(() => {
      cy.contains('Name').siblings('input').type('pau')
      cy.contains('Paunchy Cats').click()
      cy.contains('Name').siblings('input').should('have.value', 'Paunchy Cats')
      cy.contains('Ort/Stadt').siblings('input').should('have.value', 'Lichtenfels')
      cy.contains('Land').siblings('input').should('have.value', 'Deutschland')
    })
  })

  it('create an event with existing location/band and save', () => {
    const dateString = new Date().toISOString().toLowerCase()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('amazing festival' + dateString)
      cy.get('div[role = "gridcell"]').contains('13').click()
      cy.get('div[role = "gridcell"]').contains('15').click()
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('paun')
      cy.contains('Paunchy Cats').click()
    })
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.get('app-attendance').should('have.length', 3)
      cy.contains('14.').parents('app-attendance').find('input').type('land')
      cy.contains('Landmvrks').click()
    })
    cy.contains('Speichern').click()
    cy.url().should('include', '/events/')
    cy.contains('Amazing Festival' + dateString)
  })

  it('create an event with new location/band and save', () => {
    const dateString = new Date().toISOString().toLowerCase()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('amazing festival ' + dateString)
      cy.get('div[role = "gridcell"]').contains('13').click()
      cy.get('div[role = "gridcell"]').contains('15').click()
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('loca-Name ' + dateString)
      cy.contains('Ort/Stadt').siblings('input').clear().type('loca-city ' + dateString)
      cy.contains('Land').siblings('input').clear().type('loca-country ' + dateString)
    })
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.get('app-attendance').should('have.length', 3)
      cy.contains('14.').parents('app-attendance').find('input').type('land')
      cy.contains('Landmvrks').click()
      cy.contains('15.').parents('app-attendance').find('input').type('band' + dateString + '{enter}')
    })
    cy.contains('Speichern').click()
    cy.url().should('include', '/events/')
    cy.contains(dateString)
  })

  it('adding an existing band should only add the band and not the search string', () => {
    cy.get('div[role = "gridcell"]').contains('24').click()
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.contains('24.').parents('app-attendance').find('input').type('land')
      cy.contains('Landmvrks').click()
      const bands = cy.get('app-attendance').children().eq(1).children('div.d-inline-block')
      bands.should('have.length', 1)
      bands.first().contains('Landmvrks')
    })
  })

  it('selecting a category should save and display it', () => {
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Kategorie').siblings('select').select('Festival')
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('paun')
      cy.contains('Paunchy Cats').click()
    })
    cy.contains('Speichern').click()
    cy.url().should('include', '/events/')
    cy.contains('Kategorie: Festival')
  })

  it('adding an event without bands should still display it', () => {
    const dateString = new Date().toISOString().toLowerCase()
    cy.get('.accordion-item').eq(0).within(() => {
      cy.contains('Name').siblings('input').clear().type('empty event ' + dateString)
    })
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('paun')
      cy.contains('Paunchy Cats').click()
    })
    cy.contains('Speichern').click()
    cy.url().should('include', '/events/')
    cy.contains('Empty Event ' + dateString)
  })

  it('there should be a button on the menu page to the add page', () => {
    cy.visit('/menu')
    cy.contains('Veranstaltung hinzufügen').click()
    cy.contains('Veranstaltung anlegen')
    cy.url().should('equal', `${Cypress.config().baseUrl}/add`)
  })

  it('location autocomplete should work for existing location without name', () => {
    cy.contains('Location').click()
    cy.get('.accordion-item').eq(1).within(() => {
      cy.contains('Name').siblings('input').clear().type('mexi')
      cy.contains('Cdmx').click()
      cy.contains('Ort/Stadt').siblings('input').should('have.value', 'Cdmx')
    })
  })

})
