import { fillLoginForm } from '../support/login'

describe('admin -> user edit', () => {
  it('userlist should link to user edit', () => {
    cy.visit('/admin/userlist')
    fillLoginForm('admin')

    cy.contains('admin').click()
    cy.contains('admin bearbeiten')
    cy.url().should('include', '/admin/users/')
  })
  
  it('rename current user should change the name and logout', () => {
    cy.visit('/admin/userlist')
    const origUsername = 'rename-username-test'
    const newUsername = 'rename-username-test-new'
    fillLoginForm(origUsername)

    cy.contains(origUsername).click()
    cy.contains(`${origUsername} bearbeiten`)
    cy.contains('Benutzername').siblings('input').clear().type(newUsername)
    cy.contains('Roles').siblings('input').type(',STH')
    cy.get('button[type=submit]').contains('Speichern').click()
    
    cy.contains('Log In')
    fillLoginForm(newUsername)
    cy.contains(newUsername).click()
    cy.contains(`${newUsername} bearbeiten`)
    cy.contains('Benutzername').siblings('input').clear().type(origUsername)
    cy.contains('Roles').siblings('input').clear().type('ADMIN')
    cy.get('button[type=submit]').contains('Speichern').click()
  })
  
  it('changing password of current user should change the password and logout', () => {
    cy.visit('/admin/userlist')
    const username = 'admin-password-change'
    const newPassword = 'asdf1234'
    fillLoginForm(username)

    cy.contains(username).click()
    cy.contains(`${username} bearbeiten`)
    cy.contains('Neues Passwort').siblings('input').clear().type(newPassword)
    cy.get('button[type=submit]').contains('Passwort ändern').click()
    
    cy.contains('Log In')
    fillLoginForm(username, newPassword)
    cy.contains(username).click()
    cy.contains(`${username} bearbeiten`)
    cy.contains('Neues Passwort').siblings('input').clear().type('pivoleg')
    cy.get('button[type=submit]').contains('Passwort ändern').click()
  })
})
