import { fillLoginForm } from '../support/login'

describe('global back button', () => {
  it('should go to the parent path, if no history is available (admin area only)', () => {
    cy.visit('/admin/metrics')
    fillLoginForm('admin')

    cy.contains('Zurück').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/admin`)
  })
  
  it('navigate through the app and check if going back works', () => {
    cy.visit('/')
    fillLoginForm('charts-statistic')
    
    // navigate some pages
    cy.get('app-event').first().click()
    cy.url().should('include', '/events/')
    cy.get('app-band').first().click()
    cy.url().should('include', '/bands/')
    cy.get('[alt="home"]').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/menu`)
    cy.contains('Suche').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/search`)
    // navigate back till root
    cy.contains('Zurück').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/menu`)
    cy.contains('Zurück').click()
    cy.url().should('include', '/bands/')
    cy.contains('Zurück').click()
    cy.url().should('include', '/events/')
    cy.contains('Zurück').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/`)

    // stay on root if navigation back on root
    cy.contains('Zurück').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/`)
  })
})
