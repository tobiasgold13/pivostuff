import { fillLoginForm } from '../support/login'

describe('General view and navigation', () => {
  beforeEach(() => {
    cy.visit('/')
    fillLoginForm()
  })

  it('clicking on a event should display the details page', () => {
    cy.get('select').select('2022')
    cy.get('app-event').contains('Metalcamp').click()
    cy.contains('02.02. - 05.02.2022')
    cy.url().should('include', '/events/')
  })
  
  it('the band page should display all the events this band was seen at', () => {
    cy.get('select').select('2021')
    cy.get('app-event').contains('Summerbreeze').click()
    cy.contains('Landmvrks').click()
    cy.get('app-event').contains('Summerbreeze')
    cy.get('app-event').contains('Metalcamp')
  })
  
  it('on menu page should be a button to link to this overview page', () => {
    cy.visit('/menu')
    cy.contains('Übersicht').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/`)
  })
  
  it('the band page should contain a link to the setlist search for every passed event', () => {
    cy.visit('/bands/4')
    cy.contains('Stick To Your Guns')
    cy.contains('2025')
    cy.contains('Release Tour').parents('.flex-row').find('a.btn').should('have.attr', 'href', 'https://www.setlist.fm/search?year=2025&query=Stick+To+Your+Guns+Nürnberg')
  })
  
  it('the band page should contain a link to the setlist search for every future event', () => {
    cy.visit('/events/10')
    cy.contains('Stick To Your Guns')
    cy.contains('Future Tour')

    cy.contains('Edit').click()
    const nextYear = new Date().getFullYear() + 1
    cy.get('select[aria-label="Select year"]').select(`${nextYear}`)
    cy.get('div[role="gridcell"]').contains('16').click()
    cy.contains('Bands').click()
    cy.get('.accordion-item').eq(2).within(() => {
      cy.contains('16.').parents('app-attendance').find('input').type('Stick to your')
      cy.contains('Stick To Your Guns').click()
    })
    cy.contains('Speichern').click()
    
    cy.contains('Stick To Your Guns').click()
    cy.contains('Future Tour').parents('.flex-row').find('a.btn').should('have.attr', 'href', `https://www.setlist.fm/search?year=${nextYear}&query=Stick+To+Your+Guns`)
  })
  
})
