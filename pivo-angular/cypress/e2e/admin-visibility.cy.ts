import { fillLoginForm } from '../support/login'

describe('admin area', () => {
    it('link to admin area should not be visible for normal user', () => {
      cy.visit('/menu')
      fillLoginForm()

      cy.contains('ADMIN').should('not.exist')
    })

    it('normal user should be logged out when trying to access admin area', () => {
      cy.visit('/admin')
      fillLoginForm()

      cy.get('a.btn').first().click()
      cy.contains('Log In')
    })
    
    it('admin user should be able to access admin area', () => {
      cy.visit('/menu')
      fillLoginForm('admin')
      
      cy.contains('ADMIN').click()
      cy.contains('Admin')
      cy.contains('Benutzerverwaltung')
      cy.contains('Sessions')
      cy.get('a.btn').first().click()
    })
  })