import { fillLoginForm } from '../support/login'

describe('Login Page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('invalid login should display error message', () => {
    cy.get('input[placeholder="Benutzername"]').type('asdf')
    cy.get('input[placeholder="Passwort"]').type('asdf')
    cy.get('input[type="submit"]').click()
    cy.contains('Login fehlgeschlagen')
  })

  it('login should be triggered by pressing enter', () => {
    cy.get('input[placeholder="Benutzername"]').type('asdf')
    cy.get('input[placeholder="Passwort"]').type('asdf{enter}')
    cy.contains('Login fehlgeschlagen')
  })

  it('correct credentials should login the user', () => {
    fillLoginForm()
    cy.contains('Pivoleg Consulting GmbH & Co KG')
  })

  it('should display debug information', () => {
    cy.contains('debug active').should('not.exist')
    cy.visit('/?debug=true')
    cy.contains('debug active')

  })
})