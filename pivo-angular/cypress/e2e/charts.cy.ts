import { fillLoginForm } from '../support/login'

describe('Charts Page', () => {
  it('menu page should contain link to charts page', () => {
    cy.visit('/menu')
    fillLoginForm()

    cy.contains('Statistiken').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/charts`)
    cy.contains('Statistiken')
  })

  it('should contain respective charts', () => {
    cy.visit('/charts')
    fillLoginForm()

    cy.contains('Veranstaltungen pro Jahr').click()
    cy.contains('Shows pro Jahr').click()
    cy.contains('Events pro Kategorie').click()
    cy.get('canvas').should('have.length', 3)
  })

  it('should contain Top 10 Bands List', () => {
    cy.visit('/charts')
    fillLoginForm('charts-statistic')

    cy.contains('Top 10 Bands').click()
    cy.get('tr').eq(0).should('have.text', '100-Plus 2020 Mal')
    cy.get('tr').eq(1).should('have.text', '200-Band 10-1910 Mal')
    cy.get('tr').eq(2).should('have.text', '300-From 5 Till 95 Mal')
  })
})