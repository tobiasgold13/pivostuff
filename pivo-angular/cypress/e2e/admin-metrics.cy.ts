import { fillLoginForm } from '../support/login'

describe('admin -> metrics', () => {
  it('admin index should link to metrics', () => {
    cy.visit('/admin')
    fillLoginForm('admin')

    cy.contains('Metrics').click()
    cy.url().should('equal', `${Cypress.config().baseUrl}/admin/metrics`)
    cy.contains('Metrics')
  })
  
  it('should display various metrics', () => {
    cy.visit('/admin/metrics')
    fillLoginForm('admin')

    cy.contains('Controls')
    cy.get('button').contains('Update Metrics History')
    cy.contains('Overall Events')
    cy.contains('Overall Total Shows')

    cy.contains('Passed Events')
    cy.contains('Passed Distinct Bands')
    cy.contains('Passed Total Shows')
    cy.contains('Passed Events By Category')
    cy.get('canvas').should('have.length', 6)
  })
})
