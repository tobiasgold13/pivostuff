import { fillLoginForm } from '../support/login'

describe('Search Page', () => {
    it('search page should be navigatable from menu', () => {
      cy.visit('/menu')
      fillLoginForm()
      cy.contains('Suche').click()
      cy.url().should('equal', `${Cypress.config().baseUrl}/search`)
    })

    it('search input should have placeholder and add query to url', () => {
      cy.visit('/search')
      fillLoginForm()
      cy.get('input').should('have.attr', 'placeholder', 'Band, Location, Veranstaltung...')
      cy.url().should('equal', `${Cypress.config().baseUrl}/search`)
      cy.get('input').type('test').type('{enter}')
      cy.url().should('equal', `${Cypress.config().baseUrl}/search?q=test`)
    })

    it('query url parameter should fill input and trigger search', () => {
      cy.visit('/search?q=mvrks')
      fillLoginForm()
      cy.get('input').should('have.value', 'mvrks')
      cy.contains('Bands')
      cy.contains('Landmvrks')
    })

    it('search for location', () => {
      cy.visit('/search')
      fillLoginForm()
      cy.get('input').type('Land').type('{enter}')
      cy.contains('Locations')
      cy.contains('Paunchy Cats').click()
      cy.url().should('include', '/locations/')
    })

    it('search for bands', () => {
      cy.visit('/search')
      fillLoginForm()
      cy.get('input').type('Land').type('{enter}')
      cy.contains('Bands')
      cy.contains('Landmvrks').click()
      cy.url().should('include', '/bands/')
    })

    it('search for events', () => {
      cy.visit('/search')
      fillLoginForm()
      cy.get('input').type('Kate').type('{enter}')
      cy.contains('Veranstaltungen')
      cy.contains('Kategorie E').click()
      cy.url().should('include', '/events/')
    })
  })