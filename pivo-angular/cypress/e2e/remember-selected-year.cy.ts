import { fillLoginForm } from '../support/login'

describe('remember year selection', () => {
  it('selection of a year, other than the latest, should be remembered', () => {
    cy.visit('/')
    fillLoginForm()
    cy.get('select').find('option:selected').should('not.have.text', '2021')
    cy.get('select').select('2021')
    cy.get('app-event').contains('Full Force').click()
    cy.contains('Pivoleg Consulting GmbH & Co KG').click()
    cy.get('select').find('option:selected').should('have.text', '2021')
  })
})