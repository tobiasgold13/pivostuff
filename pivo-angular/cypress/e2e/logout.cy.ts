import { fillLoginForm } from '../support/login'

describe('Logout', () => {
  it('the logout button should be available and working', () => {
    cy.visit('/menu')
    fillLoginForm()
    cy.contains('Log Out oleg').click()
    cy.contains('Log In')
  })
})