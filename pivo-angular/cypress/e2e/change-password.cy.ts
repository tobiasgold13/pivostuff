import { fillLoginForm } from '../support/login'

describe('change password', () => {
  const USERNAME = 'user-password-change'

  it('menu page should contain link to change password page', () => {
    cy.visit('/menu')
    fillLoginForm(USERNAME)

    cy.contains('Passwort ändern').click()
    cy.contains('Passwort ändern')
    cy.url().should('equal', `${Cypress.config().baseUrl}/change-password`)
  })

  it('button should be disabled on missing input', () => {
    cy.visit('/change-password')
    fillLoginForm(USERNAME)

    cy.get('button[type=submit]').should('be.disabled')
    cy.contains('aktuelles Passwort*').siblings('input').type('asdf')
    cy.get('button[type=submit]').should('be.disabled')
    cy.contains('neues Passwort*').siblings('input').type('asdf')
    cy.get('button[type=submit]').should('be.disabled')
    cy.contains('neues Passwort wiederholen*').siblings('input').type('asd')
    cy.get('button[type=submit]').should('be.enabled')
    cy.get('button[type=submit]').click()
    cy.get('button[type=submit]').should('be.disabled')
    cy.contains('Passwörter müssen identisch sein!')
  })
  
  it('should alter the password', () => {
    cy.visit('/change-password')
    fillLoginForm(USERNAME)
    const oldPassword = 'pivoleg'
    const newPassword = 'new-password'
    // change password
    cy.contains('aktuelles Passwort*').siblings('input').type(oldPassword)
    cy.contains('neues Passwort*').siblings('input').type(newPassword)
    cy.contains('neues Passwort wiederholen*').siblings('input').type(newPassword)
    cy.get('button[type=submit]').click()
    // change back to orig password
    fillLoginForm(USERNAME, newPassword)
    cy.visit('/change-password')
    cy.contains('aktuelles Passwort*').siblings('input').type(newPassword)
    cy.contains('neues Passwort*').siblings('input').type(oldPassword)
    cy.contains('neues Passwort wiederholen*').siblings('input').type(oldPassword)
    cy.get('button[type=submit]').click()
    cy.contains('Log In')
  })
})