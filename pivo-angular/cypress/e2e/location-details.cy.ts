import { fillLoginForm } from '../support/login'

describe('Location Details Page', () => {
    it('should display details for location', () => {
      cy.visit('/locations/1')
      fillLoginForm()
      cy.contains('Paunchy Cats - Lichtenfels (Deutschland)')
    })
    
    it('should display events for this location', () => {
      cy.visit('/locations/3')
      fillLoginForm()
      cy.contains('ein Ort (irgendwo)')
      cy.contains('Veranstaltungen (1)')
      cy.contains('01.01.2021 ein Ort').click()
      cy.url().should('include', '/events/')
    })
    
    it('should display bands for this location', () => {
      cy.visit('/locations/1')
      fillLoginForm()
      cy.contains('Paunchy Cats - Lichtenfels (Deutschland)')
      cy.contains('Bands (2)')
      cy.contains('Slayer').click()
      cy.url().should('include', '/bands/')
    })
  })