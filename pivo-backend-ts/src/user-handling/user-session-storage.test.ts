const dbMock = {
  queryAndRelease: jest.fn(),
  insertAndRelease: jest.fn()
}
jest.mock('../db/mariadb', () => dbMock)

const bcryptMock = { compare: jest.fn() }
jest.mock('bcrypt', () => bcryptMock)
const jwtMock = { sign: jest.fn(), verify: jest.fn() }
jest.mock('jsonwebtoken', () => jwtMock)

import { doLogin, getUserForSession } from './user-session-storage'

describe('user-handling/user-session-storage.ts', () => {
  describe('doLogin(...)', () => {
    test('should call the database correctly', async () => {
      await doLogin('pivoslav', '')
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT id, name, password, roles FROM user WHERE name = ?', ['pivoslav'])
    })

    test('should return undefined if user is not found', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce(undefined)
      const token = await doLogin('pivoslav', '')
      expect(token).toBeUndefined()
    })

    test('should return undefined if password is incorrect', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ password: 'password-database' }])
      bcryptMock.compare.mockResolvedValueOnce(false)
      const token = await doLogin('', 'password-param')
      expect(token).toBeUndefined()
      expect(bcryptMock.compare).toHaveBeenCalledWith('password-param', 'password-database')
    })

    test('should get jwt config from the database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ name: 'name-database', password: 'password-database', roles: 'ROLE-DB' }])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ token_exp_time: '2 days', jwt_secret: 'secret-key-from-db' }])
      bcryptMock.compare.mockResolvedValueOnce(true)
      await doLogin('', '')
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT token_exp_time, jwt_secret FROM configuration')
    })

    test('should return a token string for correct credentials', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ name: 'name-database', password: 'password-database', roles: 'ROLE-DB' }])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ token_exp_time: '2 days', jwt_secret: 'secret-key-from-db' }])
      bcryptMock.compare.mockResolvedValueOnce(true)
      jwtMock.sign.mockReturnValueOnce('generated-token')
      const token = await doLogin('', 'password-param')
      expect(token).toEqual('generated-token')
      expect(jwtMock.sign).toHaveBeenCalledWith({ username: 'name-database', roles: 'ROLE-DB' }, 'secret-key-from-db', { expiresIn: '2 days' })
    })

    test('should store the token in the database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ id: -1337, name: 'name-database', password: 'password-database' }])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ token_exp_time: '', jwt_secret: '' }])
      bcryptMock.compare.mockResolvedValueOnce(true)
      jwtMock.sign.mockReturnValueOnce('expected-token')
      await doLogin('', 'password-param')
      expect(dbMock.insertAndRelease).toHaveBeenCalledWith('INSERT INTO session (token, created, user_id, user_agent) VALUES (?, from_unixtime(?), ?, ?)', ['expected-token', expect.any(Number), -1337, undefined])
    })

    test('should store the token and user-agent header in the database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ id: -1337, name: 'name-database', password: 'password-database' }])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ token_exp_time: '', jwt_secret: '' }])
      bcryptMock.compare.mockResolvedValueOnce(true)
      jwtMock.sign.mockReturnValueOnce('expected-token')
      await doLogin('', 'password-param', 'optional-user-agent')
      expect(dbMock.insertAndRelease).toHaveBeenCalledWith('INSERT INTO session (token, created, user_id, user_agent) VALUES (?, from_unixtime(?), ?, ?)', ['expected-token', expect.any(Number), -1337, 'optional-user-agent'])
    })
  })

  describe('getUserForSession(...)', () => {
    test('should return undefined for invalid token', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ jwt_secret: 'secret' }])
      jwtMock.verify.mockImplementationOnce(() => { throw new Error() })
      const user = await getUserForSession('invalid-token')
      expect(user).toBeUndefined()
      expect(jwtMock.verify).toHaveBeenCalledWith('invalid-token', 'secret')
      expect(dbMock.queryAndRelease).toHaveBeenCalledTimes(1)
    })

    test('should return undefined for unkown session', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      const user = await getUserForSession('unkown-but-valid-token')
      expect(user).toBeUndefined()
    })

    test('should return user for known session from database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ id: -3, name: 'test-user', password: '', roles: 'STH', created: 0 }])
      const user = await getUserForSession('this-is-an-actual-token')
      expect(user).toStrictEqual({ id: -3, name: 'test-user', password: '', roles: 'STH' })
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.id, u.name, u.password, u.roles, unix_timestamp(s.created) AS created FROM session s JOIN user u ON s.user_id = u.id WHERE s.token = ?', ['this-is-an-actual-token'])
    })
  })
})
