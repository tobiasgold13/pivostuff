import { Request } from 'express'
import { AuthenticatedRequest } from './authenticated-request'

export const getCurrentUser = (req: Request) => {
  const authReq = req as AuthenticatedRequest
  return authReq.user
}
