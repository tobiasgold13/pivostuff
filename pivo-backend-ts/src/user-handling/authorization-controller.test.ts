import express, { json } from 'express'
import request from 'supertest'

const storageMock = {
  getUserForSession: jest.fn()
}
jest.mock('./user-session-storage', () => storageMock)

import { authorizationRouter } from './authorization-controller'

describe('user-handling/authorization-controller.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(authorizationRouter)

  test('unauthenticated calls should return 401 status', async () => {
    const response = await request(dummyApp).get('/test-auth')
    expect(response.status).toEqual(401)
  })

  test('unauthorized calls should return 401 status', async () => {
    storageMock.getUserForSession.mockResolvedValueOnce(undefined)
    const response = await request(dummyApp)
      .get('/test-auth')
      .set('authorization', 'test-token')
    expect(response.status).toEqual(401)
    expect(storageMock.getUserForSession).toHaveBeenCalledWith('test-token')
  })

  test('authorized calls should return a successful status code', async () => {
    storageMock.getUserForSession.mockResolvedValueOnce({ id: -1337, name: 'jest-test', password: '' })
    const response = await request(dummyApp)
      .get('/test-auth')
      .set('authorization', 'test-token')
    expect(response.status).toEqual(204)
    expect(storageMock.getUserForSession).toHaveBeenCalledWith('test-token')
  })

  test('ID 1 user should be able to access /api/admin', async () => {
    storageMock.getUserForSession.mockResolvedValueOnce({ id: 1, name: 'id-1', password: '' })
    const response = await request(dummyApp)
      .get('/api/admin/test')
      .set('authorization', 'test-token')
    expect(response.status).toEqual(404)
    expect(storageMock.getUserForSession).toHaveBeenCalledWith('test-token')
  })

  test('normal users should not be able to access /api/admin', async () => {
    storageMock.getUserForSession.mockResolvedValueOnce({ id: -1337, name: 'normal-user-without-special-roles', password: '' })
    const response = await request(dummyApp)
      .get('/api/admin/test')
      .set('authorization', 'test-token')
    expect(response.status).toEqual(401)
    expect(storageMock.getUserForSession).toHaveBeenCalledWith('test-token')
  })

  test('admin users should be able to access /api/admin', async () => {
    storageMock.getUserForSession.mockResolvedValueOnce({ id: -1337, name: 'admin-user', password: '', roles: 'ADMIN' })
    const response = await request(dummyApp)
      .get('/api/admin/test')
      .set('authorization', 'test-token')
    expect(response.status).toEqual(404)
    expect(storageMock.getUserForSession).toHaveBeenCalledWith('test-token')
  })
})
