import { Request } from 'express'
import { getCurrentUser } from './current-user'

describe('user-handling/current-user.ts', () => {
  test('should cast the object and return the user', async () => {
    const user = { id: 666 }
    const req = { user } as unknown as Request
    const currentUser = getCurrentUser(req)
    expect(currentUser).toStrictEqual(user)
  })
})
