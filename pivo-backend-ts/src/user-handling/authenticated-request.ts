import { Request } from 'express'

export interface PivolegUser {
    id: number
    name: string
    password: string
    roles: string
}

export interface PivolegSession {
    user: PivolegUser
    createdSeconds: number
}

export interface AuthenticatedRequest extends Request {
    user: PivolegUser
}
