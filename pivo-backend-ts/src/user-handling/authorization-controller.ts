import { Router, Response, Request, NextFunction } from 'express'
import { AuthenticatedRequest } from './authenticated-request'
import { getUserForSession } from './user-session-storage'

export const authorizationRouter = Router()

authorizationRouter.use(async (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization
  if (!token) {
    return res.status(401).send('nope')
  }

  const user = await getUserForSession(token)
  if (!user) {
    return res.status(401).send('nope')
  }

  if (
    req.url.startsWith('/api/admin') &&
    user.id !== 1 &&
    !user.roles?.includes('ADMIN')
  ) {
    return res.status(401).send('nope')
  }

  const authReq = req as AuthenticatedRequest
  authReq.user = user
  next()
})

authorizationRouter.get('/test-auth', (_: Request, res: Response) => {
  return res.status(204).send()
})
