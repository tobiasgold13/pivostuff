import { compare } from 'bcrypt'
import { insertAndRelease, queryAndRelease } from '../db/mariadb'
import { PivolegSession, PivolegUser } from './authenticated-request'
import { sign, verify } from 'jsonwebtoken'
import { TokenPayload } from '../controllers/api-types/user-handling'

let jwtSecret = ''

const createSession = async (user: PivolegUser, userAgent?: string) => {
  // timespan in https://github.com/vercel/ms
  const config = await getJwtConfiguration()
  jwtSecret = config.jwt_secret
  const payload: TokenPayload = { username: user.name, roles: user.roles }
  const token = sign(payload, config.jwt_secret, { expiresIn: config.token_exp_time })

  // create session object and store in database AND memory cache
  const createdSeconds = Math.round(Date.now() / 1000)
  const session: PivolegSession = { user, createdSeconds }
  await storeSessionInDatabase(token, session, userAgent)

  return token
}

export const doLogin = async (name: string, password: string, userAgent?: string) => {
  const user = await getUserByNameFromDatabase(name)
  if (!user) {
    return undefined
  }

  const valid = await compare(password, user.password)
  if (valid) {
    const token = await createSession(user, userAgent)
    return token
  } else {
    return undefined
  }
}

export const getUserForSession = async (token: string): Promise<PivolegUser | undefined> => {
  if (!jwtSecret) {
    const config = await getJwtConfiguration()
    jwtSecret = config.jwt_secret
  }

  try {
    verify(token, jwtSecret)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  } catch (_) {
    return undefined
  }

  const databaseSession = await getSessionFromDatabase(token)
  return databaseSession?.user
}

// database
const getUserByNameFromDatabase = async (name: string): Promise<PivolegUser> => {
  const result = await queryAndRelease<PivolegUser>('SELECT id, name, password, roles FROM user WHERE name = ?', [name])
  return result?.[0]
}

const storeSessionInDatabase = async (token:string, session: PivolegSession, userAgent?: string) =>
  insertAndRelease('INSERT INTO session (token, created, user_id, user_agent) VALUES (?, from_unixtime(?), ?, ?)', [token, session.createdSeconds, session.user.id, userAgent])

const getSessionFromDatabase = async (token: string): Promise<PivolegSession | undefined> => {
  const [result] = await queryAndRelease<PivolegUser & {created: number}>('SELECT u.id, u.name, u.password, u.roles, unix_timestamp(s.created) AS created FROM session s JOIN user u ON s.user_id = u.id WHERE s.token = ?', [token])
  return result
    ? {
      user: {
        id: result.id,
        name: result.name,
        password: result.password,
        roles: result.roles
      },
      createdSeconds: result.created
    }
    : undefined
}

interface JwtConfiguration {
  token_exp_time: string,
  jwt_secret: string
}

const getJwtConfiguration = async (): Promise<JwtConfiguration> => {
  const result = await queryAndRelease<JwtConfiguration>('SELECT token_exp_time, jwt_secret FROM configuration')
  return result?.[0]
}
