import express, { Express, json } from 'express'
import { configureRoutes } from './routes'
import serverless from 'serverless-http'

const initialPort = Number(process.env.PORT)
export const app: Express = express()
app.use(json())

configureRoutes(app)

const success: () => void = () => {
  console.log(`🚀 Server listening on port ${initialPort}`)
}

const startServer = async () => {
  if (initialPort) {
    app.listen(initialPort, success)
  }
}
startServer()

export const handler = serverless(app)
