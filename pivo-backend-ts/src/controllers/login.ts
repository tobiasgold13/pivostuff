import { Request, Response, Router } from 'express'
import { LOGIN_ENDPOINT, LoginRequest } from './api-types/user-handling'
import { object, ObjectSchema, string } from 'yup'
import { doLogin } from '../user-handling/user-session-storage'

export const loginRouter = Router()

const loginRequestSchema: ObjectSchema<LoginRequest> = object({
  username: string().required(),
  password: string().required()
})

loginRouter.post(LOGIN_ENDPOINT, async (req: Request, res: Response) => {
  let loginRequest: LoginRequest
  try {
    loginRequest = await loginRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }

  const userAgent = req.headers['user-agent']
  const token = await doLogin(loginRequest.username.trim(), loginRequest.password.trim(), userAgent)
  if (token) {
    return res.json({ token })
  } else {
    return res.sendStatus(403)
  }
})
