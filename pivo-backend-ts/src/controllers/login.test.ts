import express, { json } from 'express'
import request from 'supertest'

const storageMock = {
  doLogin: jest.fn()
}
jest.mock('../user-handling/user-session-storage', () => storageMock)

import { loginRouter } from './login'
import { LOGIN_ENDPOINT } from './api-types/user-handling'

describe('controllers/login.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(loginRouter)

  test(`${LOGIN_ENDPOINT} should trigger the login and return a token for valid credentials`, async () => {
    storageMock.doLogin.mockResolvedValueOnce('tbd-oleg')

    const response = await request(dummyApp).post(LOGIN_ENDPOINT)
      .send({ username: 'oleg ', password: 'test ' })
    expect(response.status).toEqual(200)
    expect(response.body).toStrictEqual({ token: 'tbd-oleg' })

    expect(storageMock.doLogin).toHaveBeenCalledWith('oleg', 'test', undefined)
  })

  test(`${LOGIN_ENDPOINT} should pass the user-agent with the login`, async () => {
    storageMock.doLogin.mockResolvedValueOnce('tbd-oleg')

    const response = await request(dummyApp).post(LOGIN_ENDPOINT).set('user-agent',  'test-user-agent')
      .send({ username: 'oleg ', password: 'test ' })
    expect(response.status).toEqual(200)
    expect(response.body).toStrictEqual({ token: 'tbd-oleg' })

    expect(storageMock.doLogin).toHaveBeenCalledWith('oleg', 'test', 'test-user-agent')
  })

  test(`${LOGIN_ENDPOINT} should pass the user-agent, despite uppercase key, with the login`, async () => {
    storageMock.doLogin.mockResolvedValueOnce('tbd-oleg')

    const response = await request(dummyApp).post(LOGIN_ENDPOINT).set('User-Agent',  'test-user-agent')
      .send({ username: 'oleg ', password: 'test ' })
    expect(response.status).toEqual(200)
    expect(response.body).toStrictEqual({ token: 'tbd-oleg' })

    expect(storageMock.doLogin).toHaveBeenCalledWith('oleg', 'test', 'test-user-agent')
  })

  test(`${LOGIN_ENDPOINT} should return 403 for invalid credentials`, async () => {
    storageMock.doLogin.mockResolvedValueOnce(undefined)

    const response = await request(dummyApp).post(LOGIN_ENDPOINT)
      .send({ username: 'oleg ', password: 'asdf' })
    expect(response.status).toEqual(403)
  })

  test(`${LOGIN_ENDPOINT} should return 400 for invalid request`, async () => {
    storageMock.doLogin.mockResolvedValueOnce(undefined)

    const response = await request(dummyApp).post(LOGIN_ENDPOINT)
      .send({ name: 'oleg ', pass: 'asdf' })
    expect(response.status).toEqual(400)
  })
})
