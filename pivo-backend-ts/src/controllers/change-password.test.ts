import express, { json } from 'express'
import request from 'supertest'

const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))
const bcryptMock = { compare: jest.fn(), hash: jest.fn() }
jest.mock('bcrypt', () => bcryptMock)
const executeStatement = jest.fn()
jest.mock('../db/mariadb', () => ({ executeStatement }))

import { changePasswordRouter } from './change-password'
import { CHANGE_PASSWORD_ENDPOINT, WHO_AM_I_ENDPOINT } from './api-types/user-handling'

describe('controllers/change-password.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(changePasswordRouter)

  test(`${CHANGE_PASSWORD_ENDPOINT} should return status 400 on invalid request`, async () => {
    const response = await request(dummyApp).post(CHANGE_PASSWORD_ENDPOINT).send({})
    expect(response.statusCode).toEqual(400)
  })

  test(`${CHANGE_PASSWORD_ENDPOINT} should return status 403 if old password is incorreect`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667, password: 'db-old-pw' })
    bcryptMock.compare.mockResolvedValueOnce(false)

    const response = await request(dummyApp).post(CHANGE_PASSWORD_ENDPOINT)
      .send({ oldPassword: 'req-old-pw', newPassword: 'req-new-pw' })
    expect(response.statusCode).toEqual(403)

    expect(bcryptMock.compare).toHaveBeenCalledWith('req-old-pw', 'db-old-pw')
  })

  test(`${CHANGE_PASSWORD_ENDPOINT} should return status 204 if everything is correct and update db`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667, password: 'db-old-pw' })
    bcryptMock.compare.mockResolvedValueOnce(true)
    bcryptMock.hash.mockResolvedValueOnce('new-pw-hash')

    const response = await request(dummyApp).post(CHANGE_PASSWORD_ENDPOINT)
      .send({ oldPassword: 'req-old-pw', newPassword: 'req-new-pw' })
    expect(response.statusCode).toEqual(204)

    expect(bcryptMock.hash).toHaveBeenCalledWith('req-new-pw', 4)
    expect(executeStatement).toHaveBeenCalledWith('UPDATE user SET password = ? WHERE id = ?', ['new-pw-hash', 667])
  })

  test(`${WHO_AM_I_ENDPOINT} should return the username of the currently logged in user`, async () => {
    getCurrentUser.mockReturnValueOnce({ name: 'happy-user' })
    const response = await request(dummyApp).get(WHO_AM_I_ENDPOINT)
    expect(response.body).toStrictEqual({ name: 'happy-user' })
  })
})
