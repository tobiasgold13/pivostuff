import request from 'supertest'
import express from 'express'
import { rootRouter } from './root'

describe('controllers/root.ts', () => {
  const dummyApp = express()
  dummyApp.use(rootRouter)

  test('/ should return a cool smiley', async () => {
    await request(dummyApp)
      .get('/')
      .expect(200)
      .expect('🤘')
  })
})
