import { Band } from './band'
import { Location } from './location'

export const EVENT_YEARS_GET_ENDPOINT = '/api/events/years'

export type EventDate = string

export enum EventCategory {
    concert = 'concert',
    festival = 'festival'
}

interface AbstractEvent {
    id: number
    name?: string
    start: EventDate
    category?: EventCategory
}

export const EVENT_OVERVIEW_GET_ENDPOINT = '/api/events/overview/:year'

export interface EventOverview extends AbstractEvent {
    location: string
    city: string
}

// need this interface, because a "simple" map cannot be serialized
export interface Attendance {
    date: EventDate
    bands: Band[]
}

export const EVENT_DETAIL_GET_ENDPOINT = '/api/events/:id'

export interface EventDetail extends AbstractEvent {
    end: EventDate
    location: Location
    attendances: Attendance[]
}
