import { Band } from './band'
import { EventOverview } from './event'

export interface Location {
    id: number
    name?: string
    city: string
    country: string
}

export const LOCATION_SEARCH_POST_ENDPOINT = '/api/locations/search'

export interface LocationSearchRequest {
    query: string
}

export const LOCATION_DETAILS_GET_ENDPOINT = '/api/locations/:id'

export interface LocationDetailResponse {
    location: Location
    events: EventOverview[]
    bands: Band[]
}
