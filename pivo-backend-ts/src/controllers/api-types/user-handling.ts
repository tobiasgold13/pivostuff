export const LOGIN_ENDPOINT = '/api/login'

export interface LoginRequest {
    username: string
    password: string
}

export interface TokenPayload {
    username: string,
    roles?: string
}

export interface LoginResponse {
    token: string
}

export const CHANGE_PASSWORD_ENDPOINT = '/api/change-password'

export interface ChangePasswordRequest {
    oldPassword: string
    newPassword: string
}

export const DEBUG_GET_ENDPOINT = '/api/debug'

export interface DebugResponse {
    msg: string,
    date: string,
    platform: string,
    hostname: string,
    database: object
}

export const WHO_AM_I_ENDPOINT = '/api/whoami'

export interface WhoAmIResponse {
    name: string
}
