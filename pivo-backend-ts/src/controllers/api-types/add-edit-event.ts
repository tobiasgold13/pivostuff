import { EventCategory, EventDate } from './event'

export const SAVE_EVENT_ENDPOINT = '/api/events'

export interface PartialBand {
    id?: number
    name: string
}

export interface PartialAttendance {
    date: EventDate
    bands: PartialBand[]
}

export interface PartialLocation {
    id?: number
    name?: string
    city: string
    country: string
}

export interface PartialEventDetail {
    id?: number
    name?: string
    start: EventDate
    end: EventDate
    category?: EventCategory
    location: PartialLocation
    attendances: PartialAttendance[]
}

export interface SaveEventRequest {
    id?: number
    name?: string,
    start: EventDate
    end: EventDate
    category?: EventCategory
    location: PartialLocation
    attendances: PartialAttendance[]
}
