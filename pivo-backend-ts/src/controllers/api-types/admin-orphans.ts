import { Band } from './band'
import { Location } from './location'

export const ADMIN_ORPHANS_GET_ENDPOINT = '/api/admin/orphans'

export interface AdminOrhpansResponse {
    bands: Band[]
    locations: Location[]
}

export const ADMIN_BAND_DELETE_ENDPOINT = '/api/admin/orphans/band/:id'
export const ADMIN_LOCATION_DELETE_ENDPOINT = '/api/admin/orphans/location/:id'
