import { AdminUser } from './admin-users'

export const ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT = '/api/admin/sessions/general-data'

export interface AdminSessionsGeneralDataResponse {
    totalSessionsCount: number
    ttl: string
}

export const ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT = '/api/admin/sessions/active-users'
export const ADMIN_SESSIONS_USER_DELETE_ENDPOINT = '/api/admin/sessions/user/:id'

interface Session {
    created: number
    userAgent: string
}

export interface AdminSessionsActiveUser extends AdminUser {
    sessions: Session[]
}

export const ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT = '/api/admin/sessions/expired'
