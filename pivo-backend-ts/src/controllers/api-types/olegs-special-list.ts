import { Band } from './band'

export const OLEGS_SPECIAL_LIST_GET_ENDPOINT = '/api/charts/olegs-special-list'

export interface OlegsSpecialListRow {
    band: Band
    count: number
}
