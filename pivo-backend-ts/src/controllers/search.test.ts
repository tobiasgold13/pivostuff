import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { searchRouter } from './search'
import { SEARCH_ENDPOINT } from './api-types/search'

describe('controllers/search.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(searchRouter)

  test(`${SEARCH_ENDPOINT} should return 400 (Bad Request) if no search string is passed`, async () => {
    const response = await request(dummyApp).post(SEARCH_ENDPOINT).send({ query: ' ' })
    expect(response.statusCode).toEqual(400)
  })

  test(`${SEARCH_ENDPOINT} should search for bands, locations and events`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).post(SEARCH_ENDPOINT).send({ query: 'one' })
    expect(response.status).toEqual(200)
    expect(response.body).toStrictEqual({
      bands: expect.any(Array),
      locations: expect.any(Array),
      events: expect.any(Array)
    })
    expect(queryAndRelease).toHaveBeenCalledTimes(3)
  })

  test(`${SEARCH_ENDPOINT} should search for bands for this user`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    queryAndRelease.mockResolvedValueOnce([{ id: 66, name: 'Slayer' }])
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).post(SEARCH_ENDPOINT).send({ query: 'one' })
    expect(response.body.bands).toStrictEqual([{ id: 66, name: 'Slayer' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT b.id, b.name FROM band b JOIN attendance a ON a.band_id = b.id WHERE b.name LIKE concat("%" , ?, "%") AND a.user_id = ? ORDER BY b.name', ['one', 1337])
  })

  test(`${SEARCH_ENDPOINT} should search for locations for this user`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([{ id: 13, name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' }])
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).post(SEARCH_ENDPOINT).send({ query: 'one' })
    expect(response.body.locations).toStrictEqual([{ id: 13, name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT l.id, l.country, l.city, l.name FROM location l JOIN event e ON e.location_id = l.id WHERE CONCAT_WS(" ", l.country, l.city, l.name) LIKE concat("%" , ?, "%") AND e.user_id = ?', ['one', 1337])
  })

  test(`${SEARCH_ENDPOINT} should search for events for this user`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([{ id: 1, name: 'asdf', start: '2024-02-02', city: 'In Nürnberg', location: 'Löwensaal' }])
    const response = await request(dummyApp).post(SEARCH_ENDPOINT).send({ query: 'one' })
    expect(response.body.events).toStrictEqual([{ id: 1, name: 'asdf', start: '2024-02-02', city: 'In Nürnberg', location: 'Löwensaal' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.name LIKE concat("%" , ?, "%") AND e.user_id = ? ORDER BY e.start', ['one', 1337])
  })
})
