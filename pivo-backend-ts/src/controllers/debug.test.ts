import request from 'supertest'
import express from 'express'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { debugRouter } from './debug'
import { DEBUG_GET_ENDPOINT } from './api-types/user-handling'

describe('controllers/debug.ts', () => {
  const dummyApp = express()
  dummyApp.use(debugRouter)

  test(`${DEBUG_GET_ENDPOINT} should return a json obj`, async () => {
    const { body } = await request(dummyApp)
      .get(DEBUG_GET_ENDPOINT)
      .expect(200)
    expect(body.msg).toBeDefined()
    expect(body.date).toBeDefined()
  })

  test(`${DEBUG_GET_ENDPOINT} should work with database`, async () => {
    queryAndRelease.mockResolvedValueOnce(['current timestamp'])
    queryAndRelease.mockResolvedValueOnce([{ Database: 'test' }])
    const { body } = await request(dummyApp)
      .get(DEBUG_GET_ENDPOINT)
      .expect(200)
    expect(body.database).toBeDefined()
    expect(body.database.selectNow).toEqual('current timestamp')
    expect(body.database.schemas).toEqual(['test'])
    expect(body.database.error).toBeUndefined()
  })

  test(`${DEBUG_GET_ENDPOINT} should work without database`, async () => {
    queryAndRelease.mockRejectedValueOnce(Error('test error'))
    const { body } = await request(dummyApp)
      .get(DEBUG_GET_ENDPOINT)
      .expect(200)
    expect(body.database).toBeDefined()
    expect(body.database.error).toBeDefined()
  })
})
