import { Request, Response, Router } from 'express'
import { queryAndRelease } from '../db/mariadb'
import { Location, LOCATION_DETAILS_GET_ENDPOINT, LOCATION_SEARCH_POST_ENDPOINT, LocationDetailResponse } from './api-types/location'
import { getCurrentUser } from '../user-handling/current-user'
import { PivolegUser } from '../user-handling/authenticated-request'
import { EventOverview } from './api-types/event'
import { Band } from './api-types/band'

export const locationsRouter = Router()

locationsRouter.post(LOCATION_SEARCH_POST_ENDPOINT, async (req:Request, res:Response) => {
  const query = req.body?.query?.trim()
  if (!query) {
    return res.status(400).send()
  }
  const bands = await getLocationsLike(query)
  return res.json(bands)
})

const getLocationsLike = async (query: string): Promise<Location[]> => {
  const result = await queryAndRelease<Location>('SELECT DISTINCT id, country, city, name FROM location WHERE CONCAT_WS(" ",country, city, name) LIKE concat("%" , ?, "%")', [query])
  return result
}

locationsRouter.get(LOCATION_DETAILS_GET_ENDPOINT, async (req:Request, res: Response) => {
  const location = await getLocationForId(req.params.id)
  if (!location) {
    return res.status(404).send()
  }
  const user = getCurrentUser(req)
  const events = await getEventOverviewsForLocationAndUser(location, user)
  const bands = await getBandsForLocationAndUser(location, user)
  const response: LocationDetailResponse = { location, events, bands }
  res.json(response)
})

const getLocationForId = async (id: string): Promise<Location> => {
  const [result] = await queryAndRelease<Location>('SELECT id, country, city, name FROM location WHERE id = ?', [id])
  return result
}

const getEventOverviewsForLocationAndUser = async (location: Location, user: PivolegUser): Promise<EventOverview[]> => {
  const result = await queryAndRelease<EventOverview>('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.location_id = ? AND e.user_id = ? ORDER BY e.start', [location.id, user.id])
  return result
}

const getBandsForLocationAndUser = async (location: Location, user: PivolegUser): Promise<Band[]> => {
  const result = await queryAndRelease<Band>('SELECT DISTINCT b.id, b.name FROM band b JOIN attendance a ON a.band_id = b.id JOIN event e ON e.id = a.event_id WHERE e.location_id = ? AND a.user_id = ? ORDER BY b.name', [location.id, user.id])
  return result
}
