import express, { json } from 'express'
import request from 'supertest'

const dbMock = { queryAndRelease: jest.fn(), executeStatement: jest.fn() }
jest.mock('../../db/mariadb', () => dbMock)

import { adminSessionsRouter } from './admin-sessions'
import { ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT, ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT, ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT, ADMIN_SESSIONS_USER_DELETE_ENDPOINT } from '../api-types/admin-sessions'

describe('controllers/admin/admin-sessions.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(adminSessionsRouter)

  describe(`GET ${ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT}`, () => {
    test('should return general data', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ count: 27 }])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ ttl: '2 days' }])
      const response = await request(dummyApp).get(ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body).toStrictEqual({ totalSessionsCount: 27, ttl: '2 days' })
      expect(dbMock.queryAndRelease).toHaveBeenCalledTimes(2)
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT COUNT(1) AS count FROM session')
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT token_exp_time AS ttl FROM configuration')
    })
  })

  describe(`GET ${ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT}`, () => {
    test('should return users with sessions', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([
        { id: 1, name: 'qwer', roles: '', user_agent: 'test-agent1', created: 1740941530 },
        { id: 1, name: 'qwer', roles: '', user_agent: 'test-agent2', created: 1740941533 },
        { id: 2, name: 'asdf', roles: '', user_agent: 'test-agent3', created: 1740941535 }
      ])
      const response = await request(dummyApp).get(ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body).toStrictEqual([
        { id: 1, name: 'qwer', roles: '', sessions: [{ created: 1740941530, userAgent: 'test-agent1' }, { created: 1740941533, userAgent: 'test-agent2' }] },
        { id: 2, name: 'asdf', roles: '', sessions: [{ created: 1740941535, userAgent: 'test-agent3' }] }
      ])
      // eslint-disable-next-line quotes
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith("SELECT u.id, u.name, '' AS roles, UNIX_TIMESTAMP(s.created) AS created, s.user_agent FROM user u JOIN session s ON u.id = s.user_id ORDER BY u.name ASC, created DESC")
    })
  })

  describe(`DELETE ${ADMIN_SESSIONS_USER_DELETE_ENDPOINT}`, () => {
    test('should return BAD REQUEST if :id is invalid', async () => {
      const response = await request(dummyApp).delete(ADMIN_SESSIONS_USER_DELETE_ENDPOINT.replace(':id', 'four'))
      expect(response.statusCode).toEqual(400)
    })

    test('should return FORBIDDEN if :id is 1', async () => {
      const response = await request(dummyApp).delete(ADMIN_SESSIONS_USER_DELETE_ENDPOINT.replace(':id', '1'))
      expect(response.statusCode).toEqual(403)
    })

    test('should remove database entities for user id and return SUCCESS response', async () => {
      const response = await request(dummyApp).delete(ADMIN_SESSIONS_USER_DELETE_ENDPOINT.replace(':id', '666'))
      expect(response.statusCode).toEqual(204)
      expect(dbMock.executeStatement).toHaveBeenCalledWith('DELETE FROM session WHERE user_id = ?', [666])
    })
  })

  // there could be more stuff to unit test (like calculating the date, but I decided not to)
  describe(`DELETE ${ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT}`, () => {
    test('should delete old sessions from the database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ ttl: '2 days' }])
      const response = await request(dummyApp).delete(ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT)
      expect(response.statusCode).toEqual(204)
      expect(dbMock.executeStatement).toHaveBeenCalledWith('DELETE FROM session WHERE created <= ?', [expect.any(String)])
    })
  })
})
