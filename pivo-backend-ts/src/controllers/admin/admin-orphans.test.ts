import express, { json } from 'express'
import request from 'supertest'

const dbMock = { queryAndRelease: jest.fn(), executeStatement: jest.fn() }
jest.mock('../../db/mariadb', () => dbMock)

import { adminOrphansRouter } from './admin-orphans'
import { ADMIN_BAND_DELETE_ENDPOINT, ADMIN_LOCATION_DELETE_ENDPOINT, ADMIN_ORPHANS_GET_ENDPOINT } from '../api-types/admin-orphans'

describe('controllers/admin/admin-orphans.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(adminOrphansRouter)

  test(`GET ${ADMIN_ORPHANS_GET_ENDPOINT} should return orphaned entities`, async () => {
    dbMock.queryAndRelease.mockResolvedValueOnce([{ id: 28, name: 'Comeback Kid' }])
    dbMock.queryAndRelease.mockResolvedValueOnce([{ id: 35, country: 'Alman-Land', city: 'Würzburg' }])
    const response = await request(dummyApp).get(ADMIN_ORPHANS_GET_ENDPOINT)
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual({
      bands: [{ id: 28, name: 'Comeback Kid' }],
      locations: [{ id: 35, country: 'Alman-Land', city: 'Würzburg' }]
    })
    expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT b.id, b.name FROM band b LEFT JOIN attendance a ON a.band_id = b.id WHERE a.id IS NULL')
    expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT l.id, l.country, l.city, l.name FROM location l LEFT JOIN event e ON e.location_id = l.id WHERE e.location_id IS NULL')
  })

  test(`DELETE ${ADMIN_BAND_DELETE_ENDPOINT} should delete band with id`, async () => {
    const response = await request(dummyApp).delete(ADMIN_BAND_DELETE_ENDPOINT.replace(':id', '24 '))
    expect(response.statusCode).toEqual(204)
    expect(dbMock.executeStatement).toHaveBeenCalledWith('DELETE FROM band WHERE id = ?', ['24'])
  })

  test(`DELETE ${ADMIN_LOCATION_DELETE_ENDPOINT} should delete location with id`, async () => {
    const response = await request(dummyApp).delete(ADMIN_LOCATION_DELETE_ENDPOINT.replace(':id', '24 '))
    expect(response.statusCode).toEqual(204)
    expect(dbMock.executeStatement).toHaveBeenCalledWith('DELETE FROM location WHERE id = ?', ['24'])
  })
})
