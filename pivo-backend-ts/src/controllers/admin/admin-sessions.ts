import { Request, Response, Router } from 'express'
import { ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT, ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT, ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT, AdminSessionsGeneralDataResponse, ADMIN_SESSIONS_USER_DELETE_ENDPOINT, AdminSessionsActiveUser } from '../api-types/admin-sessions'
import { executeStatement, queryAndRelease } from '../../db/mariadb'
import ms, { StringValue } from 'ms'

export const adminSessionsRouter = Router()

adminSessionsRouter.get(ADMIN_SESSIONS_GENERAL_DATA_GET_ENDPOINT, async (_: Request, res: Response) => {
  const totalSessionsCount = await getSessionCount()
  const ttl = await getSessionTTL()
  const response: AdminSessionsGeneralDataResponse = {
    totalSessionsCount,
    ttl
  }
  res.json(response)
})

const getSessionCount = async (): Promise<number> => {
  const [{ count }] = await queryAndRelease<{count: number}>('SELECT COUNT(1) AS count FROM session')
  return count
}

const getSessionTTL = async (): Promise<string> => {
  const [{ ttl }] = await queryAndRelease<{ttl: string}>('SELECT token_exp_time AS ttl FROM configuration')
  return ttl
}

adminSessionsRouter.get(ADMIN_SESSIONS_ACTIVE_USERS_GET_ENDPOINT, async (_: Request, res: Response) => {
  const activeSessionsResult = await getActiveSessions()
  const aggregate = new Map<number, AdminSessionsActiveUser>()
  for (const row of activeSessionsResult) {
    let user = aggregate.get(row.id)
    if (!user) {
      user = {
        id: row.id,
        name: row.name,
        roles: row.roles,
        sessions: []
      }
      aggregate.set(row.id, user)
    }
    user?.sessions.push({ created: row.created, userAgent: row.user_agent })
  }
  res.json(aggregate.values().toArray())
})

interface ActiveSessionResult {
  id: number
  name: string
  roles: string
  created: number
  user_agent: string
}

const getActiveSessions = async (): Promise<ActiveSessionResult[]> => {
  // eslint-disable-next-line quotes
  const usersWithSessionCount = await queryAndRelease<ActiveSessionResult>("SELECT u.id, u.name, '' AS roles, UNIX_TIMESTAMP(s.created) AS created, s.user_agent FROM user u JOIN session s ON u.id = s.user_id ORDER BY u.name ASC, created DESC")
  return usersWithSessionCount
}

adminSessionsRouter.delete(ADMIN_SESSIONS_USER_DELETE_ENDPOINT, async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id, 10)
  if (Number.isNaN(userId)) {
    return res.status(400).send()
  }

  if (userId === 1) {
    return res.status(403).send()
  }

  await deleteSessionsForUser(userId)
  return res.status(204).send()
})

const deleteSessionsForUser = async (userId: number) => {
  await executeStatement('DELETE FROM session WHERE user_id = ?', [userId])
}

adminSessionsRouter.delete(ADMIN_SESSIONS_EXPIRED_DELETE_ENDPOINT, async (_: Request, res: Response) => {
  const ttl = await getSessionTTL()
  // ms.StringValue is defined as a `${number}${unit}`
  const ttlMillis = ms(ttl as StringValue)
  const expireTimestampMillis = Math.round(Date.now() - ttlMillis)
  const expireTimestamp = new Date(expireTimestampMillis).toJSON().replace('T', ' ').substring(0, 19)
  await deleteSessionsOlderThan(expireTimestamp)
  return res.status(204).send()
})

const deleteSessionsOlderThan = async (timestamp: string) => {
  await executeStatement('DELETE FROM session WHERE created <= ?', [timestamp])
}
