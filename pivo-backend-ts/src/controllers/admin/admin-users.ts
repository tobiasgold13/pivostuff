import { Request, Response, Router } from 'express'
import { ADMIN_USER_DELETE_ENDPOINT, ADMIN_USERS_GET_ENDPOINT, ADMIN_USER_CREATE_ENDPOINT, AdminUser, CreateUserRequest, CreateUserResponse, ModifyPasswordRequest, ModifyUserRequest, ADMIN_USER_GET_ENDPOINT, ADMIN_USER_MODIFY_ENDPOINT, ADMIN_USER_MODIFY_PASSWORD_ENDPOINT } from '../api-types/admin-users'
import { executeStatement, insertAndRelease, queryAndRelease } from '../../db/mariadb'
import { object, ObjectSchema, string } from 'yup'
import { hash } from 'bcrypt'

export const adminUsersRouter = Router()

adminUsersRouter.get(ADMIN_USERS_GET_ENDPOINT, async (_: Request, res: Response) => {
  const users = await getAllUsers()
  res.json(users)
})

const getAllUsers = async (): Promise<AdminUser[]> => {
  const allUsers = await queryAndRelease<AdminUser>('SELECT id, name, roles FROM user')
  return allUsers
}

const createUserRequestSchema: ObjectSchema<CreateUserRequest> = object({
  name: string().required(),
  password: string().required(),
  roles: string()
})

adminUsersRouter.post(ADMIN_USER_CREATE_ENDPOINT, async (req: Request, res: Response) => {
  let createUserRequest: CreateUserRequest
  try {
    createUserRequest = await createUserRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }

  const passwordHash = await hash(createUserRequest.password, 4)
  const id = await insertUser(createUserRequest.name, passwordHash, createUserRequest.roles)
  const response: CreateUserResponse = { id }
  return res.json(response)
})

const insertUser = async (name: string, passwordHash: string, roles?: string) => {
  const result = await insertAndRelease('INSERT INTO user (name, password, roles) VALUES (?,?,?)', [name, passwordHash, roles])
  return result.insertId
}

adminUsersRouter.delete(ADMIN_USER_DELETE_ENDPOINT, async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id, 10)
  if (Number.isNaN(userId)) {
    return res.status(400).send()
  }

  if (userId === 1) {
    return res.status(403).send()
  }

  await deleteAttendancesForUser(userId)
  await deleteEventsForUser(userId)
  await deleteSessionsForUser(userId)
  await deleteUser(userId)

  return res.status(204).send()
})

const deleteAttendancesForUser = async (userId: number) => {
  await executeStatement('DELETE FROM attendance WHERE user_id = ?', [userId])
}

const deleteEventsForUser = async (userId: number) => {
  await executeStatement('DELETE FROM event WHERE user_id = ?', [userId])
}

const deleteSessionsForUser = async (userId: number) => {
  await executeStatement('DELETE FROM session WHERE user_id = ?', [userId])
}

const deleteUser = async (userId: number) => {
  await executeStatement('DELETE FROM user WHERE id = ?', [userId])
}

adminUsersRouter.get(ADMIN_USER_GET_ENDPOINT, async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id, 10)
  if (Number.isNaN(userId)) {
    return res.status(400).send()
  }

  const user = await getUser(userId)
  res.json(user)
})

const getUser = async (id: number): Promise<AdminUser | undefined> => {
  const [user] = await queryAndRelease<AdminUser>('SELECT id, name, roles FROM user WHERE id = ?', [id])
  return user
}

const modifyUserRequestSchema: ObjectSchema<ModifyUserRequest> = object({
  name: string().required(),
  roles: string()
})

adminUsersRouter.post(ADMIN_USER_MODIFY_ENDPOINT, async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id, 10)
  if (Number.isNaN(userId)) {
    return res.status(400).send()
  }

  if (userId === 1) {
    return res.status(403).send()
  }

  let modifyUserRequest: ModifyUserRequest
  try {
    modifyUserRequest = await modifyUserRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }

  await updateUser(userId, modifyUserRequest)
  await deleteSessionsForUser(userId)
  return res.status(204).send()
})

const updateUser = async (userId: number, { name, roles }: ModifyUserRequest): Promise<void> => (executeStatement('UPDATE user SET name = ?, roles = ? WHERE id = ?', [name, roles, userId]))

const modifyPasswordRequestSchema: ObjectSchema<ModifyPasswordRequest> = object({
  password: string().required()
})

adminUsersRouter.post(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT, async (req: Request, res: Response) => {
  const userId = parseInt(req.params.id, 10)
  if (Number.isNaN(userId)) {
    return res.status(400).send()
  }

  if (userId === 1) {
    return res.status(403).send()
  }

  let modifyPasswordRequest: ModifyPasswordRequest
  try {
    modifyPasswordRequest = await modifyPasswordRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }

  const passwordHash = await hash(modifyPasswordRequest.password, 4)
  await updatePassword(userId, passwordHash)
  await deleteSessionsForUser(userId)
  return res.status(204).send()
})

const updatePassword = async (userId: number, passwordHash: string): Promise<void> => (executeStatement('UPDATE user SET password = ? WHERE id = ?', [passwordHash, userId]))
