import { Request, Response, Router } from 'express'
import { queryAndRelease } from '../../db/mariadb'
import { ADMIN_METRICS_CURRENT_GET_ENDPOINT, ADMIN_METRICS_OVER_TIME_GET_ENDPOINT, ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT, AdminMetricsCountPerUser, AdminMetricsCurrentResponse, AdminMetricsGroupCountPerUser } from '../api-types/admin-metrics'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { appendMetrics } from './admin-metrics-service'

export const adminMetricsRouter = Router()

adminMetricsRouter.post(ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT, async (_: Request, res: Response) => {
  const users = await getAllUsers()
  for (const user of users) {
    await appendMetrics(user)
  }
  return res.status(204).send()
})

const getAllUsers = async (): Promise<PivolegUser[]> => {
  // eslint-disable-next-line quotes
  const allUsers = await queryAndRelease<PivolegUser>("SELECT id, name, '' AS password, '' AS roles FROM user")
  return allUsers
}

adminMetricsRouter.get(ADMIN_METRICS_OVER_TIME_GET_ENDPOINT, async (req: Request, res: Response) => {
  const metricKey = req.params.metric
  const data = await getMetricOverTime(metricKey)
  return res.json(data)
})

const getMetricOverTime = async (metric: string): Promise<AdminMetricsGroupCountPerUser[]> => {
  const result = await queryAndRelease<AdminMetricsGroupCountPerUser>('SELECT u.name AS username, m.day AS `group`, m.count FROM metrics_history m JOIN user u ON u.id = m.user_id WHERE m.metric = ? ORDER BY m.day, m.user_id', [metric])
  return result
}

adminMetricsRouter.get(ADMIN_METRICS_CURRENT_GET_ENDPOINT, async (_: Request, res: Response) => {
  const passedEvents = await getPassedEventCount()
  const passedDistinctBands = await getPassedDistinctBandCount()
  const passedTotalShows = await getPassedTotalShowCount()
  const passedEventsByCategory = await getPassedEventsByCategoryCount()
  const response: AdminMetricsCurrentResponse = {
    passedEvents,
    passedDistinctBands,
    passedTotalShows,
    passedEventsByCategory
  }
  res.json(response)
})

const getPassedEventCount = async (): Promise<AdminMetricsCountPerUser[]> => {
  const allUsers = await queryAndRelease<AdminMetricsCountPerUser>('SELECT u.name AS username, COUNT(1) AS count FROM event e JOIN user u ON e.user_id = u.id WHERE e.start <= NOW() GROUP BY u.id ORDER BY u.name')
  return allUsers
}

const getPassedDistinctBandCount = async (): Promise<AdminMetricsCountPerUser[]> => {
  const allUsers = await queryAndRelease<AdminMetricsCountPerUser>('SELECT u.name AS username, COUNT(DISTINCT a.band_id) AS count FROM attendance a JOIN user u ON a.user_id = u.id WHERE a.date <= NOW() GROUP BY u.id ORDER BY u.name')
  return allUsers
}

const getPassedTotalShowCount = async (): Promise<AdminMetricsCountPerUser[]> => {
  const allUsers = await queryAndRelease<AdminMetricsCountPerUser>('SELECT u.name AS username, COUNT(1) AS count FROM attendance a JOIN user u ON a.user_id = u.id WHERE a.date <= NOW() GROUP BY u.id ORDER BY u.name')
  return allUsers
}

const getPassedEventsByCategoryCount = async (): Promise<AdminMetricsGroupCountPerUser[]> => {
  const allUsers = await queryAndRelease<AdminMetricsGroupCountPerUser>('SELECT u.name AS username, e.category AS `group`, COUNT(1) AS count FROM event e JOIN user u ON e.user_id = u.id WHERE e.start <= NOW() GROUP BY u.id, e.category ORDER BY u.name, e.category')
  return allUsers
}