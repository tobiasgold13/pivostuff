import { PivolegUser } from '../../user-handling/authenticated-request'

const dbMock = { queryAndRelease: jest.fn(), insertAndRelease: jest.fn(), executeStatement: jest.fn() }
jest.mock('../../db/mariadb', () => dbMock)

import { appendMetrics } from './admin-metrics-service'

describe('controllers/admin/admin-metrics-service.ts', () => {
  test('should query and update the correct metric', async () => {
    const user = { id: 48, name: 'doesnt matter' }
    dbMock.queryAndRelease.mockResolvedValueOnce([{ count: 2 }])
    dbMock.queryAndRelease.mockResolvedValueOnce([{ count: 12 }])

    await appendMetrics(user as PivolegUser)

    expect(dbMock.executeStatement).toHaveBeenCalledTimes(1)
    expect(dbMock.executeStatement).toHaveBeenCalledWith('FLUSH TABLE event, attendance')
    expect(dbMock.queryAndRelease).toHaveBeenCalledTimes(2)
    expect(dbMock.queryAndRelease).toHaveBeenNthCalledWith(1, 'SELECT COUNT(1) AS count FROM event e WHERE e.user_id = ?', [user.id])
    expect(dbMock.queryAndRelease).toHaveBeenNthCalledWith(2, 'SELECT COUNT(1) AS count FROM attendance a WHERE a.user_id = ?', [user.id])
    expect(dbMock.insertAndRelease).toHaveBeenCalledTimes(2)
    expect(dbMock.insertAndRelease).toHaveBeenNthCalledWith(1, 'INSERT INTO metrics_history (user_id, day, metric, count) VALUES (?,NOW(),?,?) ON DUPLICATE KEY UPDATE count = ?', [user.id, 'OVERALL_EVENTS', 2, 2])
    expect(dbMock.insertAndRelease).toHaveBeenNthCalledWith(2, 'INSERT INTO metrics_history (user_id, day, metric, count) VALUES (?,NOW(),?,?) ON DUPLICATE KEY UPDATE count = ?', [user.id, 'OVERALL_TOTAL_SHOWS', 12, 12])
  })
})
