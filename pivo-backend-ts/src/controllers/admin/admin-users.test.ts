import express, { json } from 'express'
import request from 'supertest'

const dbMock = { queryAndRelease: jest.fn(), executeStatement: jest.fn(), insertAndRelease: jest.fn() }
jest.mock('../../db/mariadb', () => dbMock)
const hash = jest.fn()
jest.mock('bcrypt', () => ({ hash }))

import { adminUsersRouter } from './admin-users'
import { ADMIN_USER_DELETE_ENDPOINT, ADMIN_USERS_GET_ENDPOINT, ADMIN_USER_CREATE_ENDPOINT, ADMIN_USER_GET_ENDPOINT, ADMIN_USER_MODIFY_ENDPOINT, ADMIN_USER_MODIFY_PASSWORD_ENDPOINT } from '../api-types/admin-users'

describe('controllers/admin/admin-users.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(adminUsersRouter)

  describe(`GET ${ADMIN_USERS_GET_ENDPOINT}`, () => {
    test('should return all users returned by db call', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ id: 2, name: 'someone', roles: 'NONE' }])
      const response = await request(dummyApp).get(ADMIN_USERS_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body).toStrictEqual([{ id: 2, name: 'someone', roles: 'NONE' }])
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT id, name, roles FROM user')
    })
  })

  describe(`POST ${ADMIN_USER_CREATE_ENDPOINT}`, () => {
    test('should return BAD REQUEST if the request is invalid', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_CREATE_ENDPOINT).send({})
      expect(response.statusCode).toEqual(400)
    })

    test('should create a password hash', async () => {
      hash.mockResolvedValueOnce('pw-hash')
      dbMock.insertAndRelease.mockResolvedValueOnce({ insertId: 12 })
      const response = await request(dummyApp).post(ADMIN_USER_CREATE_ENDPOINT).send({ name: 'u-name', password: 'u-pw', roles: 'u-roles' })
      expect(response.statusCode).toEqual(200)
      expect(hash).toHaveBeenCalledWith('u-pw', 4)
    })

    test('should store the user data and password hash in the database', async () => {
      hash.mockResolvedValueOnce('pw-hash')
      dbMock.insertAndRelease.mockResolvedValueOnce({ insertId: 12 })
      const response = await request(dummyApp).post(ADMIN_USER_CREATE_ENDPOINT).send({ name: 'u-name', password: 'u-pw', roles: 'u-roles' })
      expect(response.statusCode).toEqual(200)
      expect(dbMock.insertAndRelease).toHaveBeenCalledWith('INSERT INTO user (name, password, roles) VALUES (?,?,?)', ['u-name', 'pw-hash', 'u-roles'])
    })

    test('should return the new user id', async () => {
      dbMock.insertAndRelease.mockResolvedValueOnce({ insertId: 12 })
      const response = await request(dummyApp).post(ADMIN_USER_CREATE_ENDPOINT).send({ name: 'u-name', password: 'u-pw', roles: 'u-roles' })
      expect(response.statusCode).toEqual(200)
      expect(response.body).toStrictEqual({ id: 12 })
    })
  })

  describe(`DELETE ${ADMIN_USER_DELETE_ENDPOINT}`, () => {
    test('should return BAD REQUEST if :id is invalid', async () => {
      const response = await request(dummyApp).delete(ADMIN_USER_DELETE_ENDPOINT.replace(':id', 'four'))
      expect(response.statusCode).toEqual(400)
    })

    test('should return FORBIDDEN if :id is 1', async () => {
      const response = await request(dummyApp).delete(ADMIN_USER_DELETE_ENDPOINT.replace(':id', '1'))
      expect(response.statusCode).toEqual(403)
    })

    test('should remove database entities for user id and return SUCCESS response', async () => {
      const response = await request(dummyApp).delete(ADMIN_USER_DELETE_ENDPOINT.replace(':id', '666'))
      expect(response.statusCode).toEqual(204)
      expect(dbMock.executeStatement).toHaveBeenCalledTimes(4)
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(1, 'DELETE FROM attendance WHERE user_id = ?', [666])
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(2, 'DELETE FROM event WHERE user_id = ?', [666])
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(3, 'DELETE FROM session WHERE user_id = ?', [666])
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(4, 'DELETE FROM user WHERE id = ?', [666])
    })
  })

  describe(`GET ${ADMIN_USER_GET_ENDPOINT}`, () => {
    test('should return BAD REQUEST if :id is invalid', async () => {
      const response = await request(dummyApp).get(ADMIN_USER_GET_ENDPOINT.replace(':id', 'four'))
      expect(response.statusCode).toEqual(400)
    })

    test('should return the user from the database', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ id: 2, name: 'someone', roles: 'NONE' }])
      const response = await request(dummyApp).get(ADMIN_USER_GET_ENDPOINT.replace(':id', '666'))
      expect(response.statusCode).toEqual(200)
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT id, name, roles FROM user WHERE id = ?', [666])
    })
  })

  describe(`POST ${ADMIN_USER_MODIFY_ENDPOINT}`, () => {
    test('should return BAD REQUEST if :id is invalid', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_ENDPOINT.replace(':id', 'four'))
      expect(response.statusCode).toEqual(400)
    })

    test('should return FORBIDDEN if :id is 1', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_ENDPOINT.replace(':id', '1'))
      expect(response.statusCode).toEqual(403)
    })

    test('should return BAD REQUEST if request body is invalid', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_ENDPOINT.replace(':id', '2')).send({})
      expect(response.statusCode).toEqual(400)
    })

    test('should update the user in the database', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_ENDPOINT.replace(':id', '3')).send({ name: 'new-name', roles: 'new-role' })
      expect(response.statusCode).toEqual(204)
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(1, 'UPDATE user SET name = ?, roles = ? WHERE id = ?', ['new-name', 'new-role', 3])
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(2, 'DELETE FROM session WHERE user_id = ?', [3])
    })
  })

  describe(`POST ${ADMIN_USER_MODIFY_PASSWORD_ENDPOINT}`, () => {
    test('should return BAD REQUEST if :id is invalid', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT.replace(':id', 'four'))
      expect(response.statusCode).toEqual(400)
    })

    test('should return FORBIDDEN if :id is 1', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT.replace(':id', '1'))
      expect(response.statusCode).toEqual(403)
    })

    test('should return BAD REQUEST if request body is invalid', async () => {
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT.replace(':id', '2')).send({})
      expect(response.statusCode).toEqual(400)
    })

    test('should update the password hash in the database', async () => {
      hash.mockResolvedValueOnce('new-password-hash')
      const response = await request(dummyApp).post(ADMIN_USER_MODIFY_PASSWORD_ENDPOINT.replace(':id', '7')).send({ password: 'new-password' })
      expect(response.statusCode).toEqual(204)
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(1, 'UPDATE user SET password = ? WHERE id = ?', ['new-password-hash', 7])
      expect(dbMock.executeStatement).toHaveBeenNthCalledWith(2, 'DELETE FROM session WHERE user_id = ?', [7])
      expect(hash).toHaveBeenCalledWith('new-password', 4)
    })
  })
})
