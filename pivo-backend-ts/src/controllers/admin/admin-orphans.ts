import { Request, Response, Router } from 'express'
import { executeStatement, queryAndRelease } from '../../db/mariadb'
import { ADMIN_BAND_DELETE_ENDPOINT, ADMIN_LOCATION_DELETE_ENDPOINT, ADMIN_ORPHANS_GET_ENDPOINT, AdminOrhpansResponse } from '../api-types/admin-orphans'
import { Band } from '../api-types/band'
import { Location } from '../api-types/location'

export const adminOrphansRouter = Router()

adminOrphansRouter.get(ADMIN_ORPHANS_GET_ENDPOINT, async (_: Request, res: Response) => {
  const bands = await getOrphanedBands()
  const locations = await getOrphanedLocations()
  const orphans: AdminOrhpansResponse = {
    bands,
    locations
  }
  res.json(orphans)
})

const getOrphanedBands = async (): Promise<Band[]> => {
  const result = await queryAndRelease<Band>('SELECT DISTINCT b.id, b.name FROM band b LEFT JOIN attendance a ON a.band_id = b.id WHERE a.id IS NULL')
  return result
}

const getOrphanedLocations = async (): Promise<Location[]> => {
  const result = await queryAndRelease<Location>('SELECT DISTINCT l.id, l.country, l.city, l.name FROM location l LEFT JOIN event e ON e.location_id = l.id WHERE e.location_id IS NULL')
  return result
}

adminOrphansRouter.delete(ADMIN_BAND_DELETE_ENDPOINT, async (req: Request, res: Response) => {
  await deleteBandWithId(req.params.id)
  return res.status(204).send()
})

const deleteBandWithId = async (id: string) => {
  await executeStatement('DELETE FROM band WHERE id = ?', [id])
}

adminOrphansRouter.delete(ADMIN_LOCATION_DELETE_ENDPOINT, async (req: Request, res: Response) => {
  await deleteLocationWithId(req.params.id)
  return res.status(204).send()
})

const deleteLocationWithId = async (id: string) => {
  await executeStatement('DELETE FROM location WHERE id = ?', [id])
}
