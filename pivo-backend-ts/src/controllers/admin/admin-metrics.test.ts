import express, { json } from 'express'
import request from 'supertest'

const dbMock = { queryAndRelease: jest.fn() }
jest.mock('../../db/mariadb', () => dbMock)
const appendMetrics = jest.fn()
jest.mock('../admin/admin-metrics-service', () => ({ appendMetrics }))

import { adminMetricsRouter } from './admin-metrics'
import { ADMIN_METRICS_CURRENT_GET_ENDPOINT, ADMIN_METRICS_OVER_TIME_GET_ENDPOINT, ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT } from '../api-types/admin-metrics'

describe('controllers/admin/admin-metrics.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(adminMetricsRouter)

  test(`POST ${ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT} should call metrics-service`, async () => {
    const dummyUsers = [
      { id: 1, name:'user1', password:'', roles:'' },
      { id: 2, name:'user2', password:'', roles:'' }
    ]
    dbMock.queryAndRelease.mockResolvedValueOnce(dummyUsers)
    const response = await request(dummyApp).post(ADMIN_METRICS_UPDATE_HISTORY_POST_ENDPOINT)
    expect(response.statusCode).toEqual(204)
    // eslint-disable-next-line quotes
    expect(dbMock.queryAndRelease).toHaveBeenCalledWith("SELECT id, name, '' AS password, '' AS roles FROM user")
    expect(appendMetrics).toHaveBeenCalledWith(dummyUsers[0])
    expect(appendMetrics).toHaveBeenCalledWith(dummyUsers[1])
  })

  test(`GET ${ADMIN_METRICS_OVER_TIME_GET_ENDPOINT} should load the correct data`, async () => {
    const dummyData = [{ username: 'oleg', group:'2024-02-01', count: 3 }, { username:'pivo', group:'2024-02-02', count: 5 }]
    dbMock.queryAndRelease.mockResolvedValueOnce(dummyData)
    const response = await request(dummyApp).get(ADMIN_METRICS_OVER_TIME_GET_ENDPOINT.replace(':metric', 'SAMPLE'))
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual(dummyData)
    expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.name AS username, m.day AS `group`, m.count FROM metrics_history m JOIN user u ON u.id = m.user_id WHERE m.metric = ? ORDER BY m.day, m.user_id', ['SAMPLE'])
  })

  describe(`GET ${ADMIN_METRICS_CURRENT_GET_ENDPOINT}`, () => {
    test('should return event count for every user', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([{ username: 'oleg', count: 12 }])
      const response = await request(dummyApp).get(ADMIN_METRICS_CURRENT_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body.passedEvents).toStrictEqual([{ username: 'oleg', count: 12 }])
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.name AS username, COUNT(1) AS count FROM event e JOIN user u ON e.user_id = u.id WHERE e.start <= NOW() GROUP BY u.id ORDER BY u.name')
    })
  
    test('should return distinct band count for every user', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ username: 'oleg', count: 12 }])
      const response = await request(dummyApp).get(ADMIN_METRICS_CURRENT_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body.passedDistinctBands).toStrictEqual([{ username: 'oleg', count: 12 }])
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.name AS username, COUNT(DISTINCT a.band_id) AS count FROM attendance a JOIN user u ON a.user_id = u.id WHERE a.date <= NOW() GROUP BY u.id ORDER BY u.name')
    })
  
    test('should return show count for every user', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ username: 'oleg', count: 12 }])
      const response = await request(dummyApp).get(ADMIN_METRICS_CURRENT_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body.passedTotalShows).toStrictEqual([{ username: 'oleg', count: 12 }])
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.name AS username, COUNT(1) AS count FROM attendance a JOIN user u ON a.user_id = u.id WHERE a.date <= NOW() GROUP BY u.id ORDER BY u.name')
    })
  
    test('should return event category count for every user', async () => {
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([])
      dbMock.queryAndRelease.mockResolvedValueOnce([{ username: 'oleg', group: 'festival', count: 12 }])
      const response = await request(dummyApp).get(ADMIN_METRICS_CURRENT_GET_ENDPOINT)
      expect(response.statusCode).toEqual(200)
      expect(response.body.passedEventsByCategory).toStrictEqual([{ username: 'oleg', group: 'festival', count: 12 }])
      expect(dbMock.queryAndRelease).toHaveBeenCalledWith('SELECT u.name AS username, e.category AS `group`, COUNT(1) AS count FROM event e JOIN user u ON e.user_id = u.id WHERE e.start <= NOW() GROUP BY u.id, e.category ORDER BY u.name, e.category')
    })
  })
})
