import { insertAndRelease, queryAndRelease, executeStatement } from '../../db/mariadb'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { ADMIN_METRICS_OVER_TIME } from '../api-types/admin-metrics'

export const appendMetrics = async (user: PivolegUser) => {
  await flushTablesBeforeCalculatinMetrics()
  const eventCount = await getEventCount(user)
  await upsertMetric(user, ADMIN_METRICS_OVER_TIME.overallEvents, eventCount.count)

  const showCount = await getShowCount(user)
  await upsertMetric(user, ADMIN_METRICS_OVER_TIME.overallTotalShows, showCount.count)
}

interface MetricResult {
  count: number
}

const flushTablesBeforeCalculatinMetrics = async (): Promise<void> => {
  await executeStatement('FLUSH TABLE event, attendance')
}

const getEventCount = async (user: PivolegUser): Promise<MetricResult> => {
  const [result] = await queryAndRelease<MetricResult>('SELECT COUNT(1) AS count FROM event e WHERE e.user_id = ?', [user.id])
  return result
}

const getShowCount = async (user: PivolegUser): Promise<MetricResult> => {
  const [result] = await queryAndRelease<MetricResult>('SELECT COUNT(1) AS count FROM attendance a WHERE a.user_id = ?', [user.id])
  return result
}

const upsertMetric = async (user: PivolegUser, metric: string, count: number) => 
  await insertAndRelease('INSERT INTO metrics_history (user_id, day, metric, count) VALUES (?,NOW(),?,?) ON DUPLICATE KEY UPDATE count = ?', [user.id, metric, count, count])

