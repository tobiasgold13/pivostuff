import { Request, Response, Router } from 'express'
import { Band, BAND_DETAILS_GET_ENDPOINT, BandDetails, SEARCH_BANDS_ENDPOINT } from './api-types/band'
import { queryAndRelease } from '../db/mariadb'
import { PivolegUser } from '../user-handling/authenticated-request'
import { EventOverview } from './api-types/event'
import { getCurrentUser } from '../user-handling/current-user'

export const bandsRouter = Router()

bandsRouter.post(SEARCH_BANDS_ENDPOINT, async (req:Request, res:Response) => {
  const query = req.body?.query?.trim()
  if (!query) {
    return res.status(400).send()
  }
  const bands = await getBandsLike(query)
  return res.json(bands)
})

const getBandsLike = async (nameSearchStr: string): Promise<Band[]> => {
  const result = await queryAndRelease<Band>('SELECT DISTINCT b.id, b.name FROM band b WHERE name LIKE concat("%" , ?, "%") ORDER BY b.name', [nameSearchStr])
  return result
}

bandsRouter.get(BAND_DETAILS_GET_ENDPOINT, async (req:Request, res: Response) => {
  const band = await getBandForId(req.params.id)
  if (!band) {
    return res.status(404).send()
  }
  const user = getCurrentUser(req)
  const events = await getEventsOverviewForBand(user, band)
  const response: BandDetails = {
    band,
    events
  }
  res.json(response)
})

const getBandForId = async (id: string): Promise<Band | undefined> => {
  const [result] = await queryAndRelease<Band>('SELECT id, name FROM band WHERE id = ?', [id])
  return result
}

const getEventsOverviewForBand = async (user: PivolegUser, b: Band): Promise<EventOverview[]> => {
  const result = await queryAndRelease<EventOverview>('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN attendance a ON e.id = a.event_id JOIN location l ON l.id = e.location_id WHERE e.user_id = ? AND a.band_id = ? ORDER BY e.start', [user.id, b.id])
  return result
}
