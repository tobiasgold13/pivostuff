import { executeStatement, insertAndRelease } from '../../db/mariadb'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { EventCategory, EventDate } from '../api-types/event'
import { Location } from '../api-types/location'
import { capitalizeWords } from './normalize-input'

interface ProcessEventData {
    id?: number
    name?: string
    start: EventDate
    end: EventDate
    category?: EventCategory
}

export interface ProcessEventResult {
    id: number
    name?: string
    start: EventDate
    end: EventDate
    category?: EventCategory
}

export const processEvent = async (user: PivolegUser, inputData: ProcessEventData, location: Location): Promise<ProcessEventResult> => {
  const name = inputData.name ? capitalizeWords(inputData.name) : undefined
  if (inputData.id) {
    const resultData: ProcessEventResult = {
      id: inputData.id,
      start: inputData.start,
      end: inputData.end,
      name,
      category: inputData.category
    }
    await updateEvent(resultData, location)
    return resultData
  } else {
    const newEventId = await insertEvent(user, { ...inputData, name }, location)
    const result: ProcessEventResult = {
      id: newEventId,
      start: inputData.start,
      end: inputData.end,
      name,
      category: inputData.category
    }
    return result
  }
}

const insertEvent = async (user:PivolegUser, event: ProcessEventData, location: Location) => {
  const result = await insertAndRelease('INSERT INTO event (name, start, end, category, location_id, user_id) VALUES (?,?,?,?,?,?)', [event.name, event.start, event.end, event.category, location.id, user.id])
  return result.insertId
}

const updateEvent = async (event: ProcessEventResult, location: Location): Promise<void> => (executeStatement('UPDATE event SET name = ?, start = ?, end = ?, category = ?, location_id = ? WHERE id = ?', [event.name, event.start, event.end, event.category, location.id, event.id]))
