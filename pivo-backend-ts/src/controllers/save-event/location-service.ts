import { PartialLocation } from '../api-types/add-edit-event'
import { insertAndRelease, queryAndRelease } from '../../db/mariadb'
import { Location } from '../api-types/location'
import { capitalizeWords } from './normalize-input'

export const processLocation = async (inputLocation: PartialLocation): Promise<Location> => {
  // if there's an id, the location exists already
  if (inputLocation.id) {
    return inputLocation as Location
  }

  // normalize input
  const normalizedLocation: PartialLocation = {
    name: inputLocation.name ? capitalizeWords(inputLocation.name) : undefined,
    city: capitalizeWords(inputLocation.city),
    country: capitalizeWords(inputLocation.country)
  }

  // try to find a location with these values
  const [existingLocation] = await getLocations(normalizedLocation)
  if (existingLocation) {
    return existingLocation
  }

  // create new location
  return await insertLocation(normalizedLocation)
}

const getLocations = async ({ name, city, country }: PartialLocation): Promise<Location[]> => {
  if (name) {
    const result = await queryAndRelease<Location>('SELECT id, country, city, name FROM location WHERE country = ? AND city = ? AND name = ?', [country, city, name])
    return result
  } else {
    const result = await queryAndRelease<Location>('SELECT id, country, city, name FROM location WHERE country = ? AND city = ? AND name IS NULL', [country, city])
    return result
  }
}

const insertLocation = async ({ name, city, country }: PartialLocation) => {
  const result = await insertAndRelease('INSERT INTO location (country, city, name) VALUES (?,?,?)', [country, city, name])
  return {
    id: result.insertId,
    name,
    city,
    country
  }
}
