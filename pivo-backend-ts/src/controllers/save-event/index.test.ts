import express, { json } from 'express'
import request from 'supertest'
import { SAVE_EVENT_ENDPOINT, SaveEventRequest } from '../api-types/add-edit-event'

const getCurrentUser = jest.fn()
jest.mock('../../user-handling/current-user', () => ({ getCurrentUser }))
const processLocation = jest.fn()
jest.mock('./location-service', () => ({ processLocation }))
const processEvent = jest.fn()
jest.mock('./event-service', () => ({ processEvent }))
const processAttendances = jest.fn()
jest.mock('./attendance-service', () => ({ processAttendances }))
const appendMetrics = jest.fn()
jest.mock('../admin/admin-metrics-service', () => ({ appendMetrics }))

import { saveEventRouter } from '.'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { EventCategory, EventDetail } from '../api-types/event'

describe('controllers/save-event/save-event.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(saveEventRouter)

  test(`POST ${SAVE_EVENT_ENDPOINT} process the minimum data and return the saved event`, async () => {
    const requestData: SaveEventRequest = {
      start: '2001-01-01',
      end: '2001-01-02',
      location: { city: 'request-city', country: 'request-country' },
      attendances: []
    }
    const responseData: EventDetail = {
      id: 1000,
      start: '2002-02-02',
      end: '2002-02-03',
      location: { id: 1001, city: 'processed-city', country: 'processed-country' },
      attendances: []
    }
    const user = { id: 1337 } as PivolegUser
    getCurrentUser.mockReturnValueOnce(user)
    processLocation.mockResolvedValueOnce(responseData.location)
    const processEventReturn = { id: responseData.id, name: responseData.name, start: responseData.start, end: responseData.end }
    processEvent.mockResolvedValueOnce(processEventReturn)
    processAttendances.mockResolvedValueOnce(responseData.attendances)

    const response = await request(dummyApp).post(SAVE_EVENT_ENDPOINT).send(requestData)
    expect(response.body).toStrictEqual(responseData)

    expect(processLocation).toHaveBeenCalledWith(requestData.location)
    expect(processEvent).toHaveBeenCalledWith(user, requestData, responseData.location)
    expect(processAttendances).toHaveBeenCalledWith(user, processEventReturn, requestData.attendances)
  })

  test(`POST ${SAVE_EVENT_ENDPOINT} process the maximum data and return the saved event`, async () => {
    const requestData: SaveEventRequest = {
      id: 1000,
      name: 'Super Event',
      start: '2001-01-01',
      end: '2001-01-02',
      category: 'concert' as EventCategory,
      location: { id: 1001, name: 'request-location-name', city: 'request-city', country: 'request-country' },
      attendances: [{ date: '2001-01-01', bands: [{ id: 1002, name: 'Banndame' }, { name: 'Banndame2' }] }]
    }
    const responseData: EventDetail = {
      id: 1000,
      name: 'Super event',
      start: '2002-02-02',
      end: '2002-02-03',
      category: 'concert' as EventCategory,
      location: { id: 1001, name: 'processed-location-name', city: 'processed-city', country: 'processed-country' },
      attendances: [{ date: '2002-02-02', bands: [{ id: 1002, name: 'Banndame' }, { id: 1003, name: 'Banndame2' }] }]
    }
    const user = { id: 1337 } as PivolegUser
    getCurrentUser.mockReturnValueOnce(user)
    processLocation.mockResolvedValueOnce(responseData.location)
    let processEventReturn
    {
      const { id, name, start, end, category } = responseData
      processEventReturn = { id, name, start, end, category }
    }
    processEvent.mockResolvedValueOnce(processEventReturn)
    processAttendances.mockResolvedValueOnce(responseData.attendances)

    const response = await request(dummyApp).post(SAVE_EVENT_ENDPOINT).send(requestData)
    expect(response.body).toStrictEqual(responseData)

    expect(processLocation).toHaveBeenCalledWith(requestData.location)
    expect(processEvent).toHaveBeenCalledWith(user, requestData, responseData.location)
    expect(processAttendances).toHaveBeenCalledWith(user, processEventReturn, requestData.attendances)
  })

  describe('request validation', () => {
    test('invalid request should fail (not testing every possibility)', async () => {
      const requestData: SaveEventRequest = {
        start: '01-01-01',
        end: '2001-01-02',
        location: { name: 'request-location-name', city: 'request-city', country: 'request-country' },
        attendances: [{ date: '2001-01-01', bands: [] }]
      }

      const response = await request(dummyApp).post(SAVE_EVENT_ENDPOINT).send(requestData)
      expect(response.statusCode).toEqual(400)
    })

    test('invalid event category should fail', async () => {
      const requestData: SaveEventRequest = {
        start: '2001-01-02',
        end: '2001-01-02',
        category: 'sth-invalid' as EventCategory,
        location: { name: 'request-location-name', city: 'request-city', country: 'request-country' },
        attendances: [{ date: '2001-01-01', bands: [] }]
      }

      const response = await request(dummyApp).post(SAVE_EVENT_ENDPOINT).send(requestData)
      expect(response.statusCode).toEqual(400)
    })

    test(`POST ${SAVE_EVENT_ENDPOINT} should update metrics history`, async () => {
      const requestData: SaveEventRequest = {
        start: '2001-01-01',
        end: '2001-01-02',
        location: { city: 'request-city', country: 'request-country' },
        attendances: []
      }
      const user = { id: 1337 } as PivolegUser
      getCurrentUser.mockReturnValueOnce(user)
  
      const response = await request(dummyApp).post(SAVE_EVENT_ENDPOINT).send(requestData)
      expect(response.statusCode).toStrictEqual(200)
  
      expect(appendMetrics).toHaveBeenCalledWith(user)
    })
  })
})
