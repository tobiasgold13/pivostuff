import { capitalizeWords } from './normalize-input'

describe('normalize-input.ts', () => {
  test('should capitalize single word', () => {
    expect(capitalizeWords('unrest')).toEqual('Unrest')
  })

  test('should lowercase the rest of the world', () => {
    expect(capitalizeWords('UNREST')).toEqual('Unrest')
  })

  test('should trim the entire string', () => {
    expect(capitalizeWords(' Unrest ')).toEqual('Unrest')
  })

  test('should replace tabs with spaces', () => {
    expect(capitalizeWords('\tA\t\tB\t')).toEqual('A B')
  })

  test('should capitalize words, seperated by space', () => {
    expect(capitalizeWords('as i lay dying')).toEqual('As I Lay Dying')
  })

  test('should capitalize words, seperated by minus', () => {
    expect(capitalizeWords('as-i-lay-dying')).toEqual('As-I-Lay-Dying')
  })

  test('should capitalize words, seperated by underscore', () => {
    expect(capitalizeWords('as_i_lay_dying')).toEqual('As_I_Lay_Dying')
  })

  test('should remove unnecessary separators', () => {
    expect(capitalizeWords('  As  I Lay  Dying')).toEqual('As I Lay Dying')
  })

  test('should work with multiple separators in a single string', () => {
    expect(capitalizeWords('As--I__Lay  Dying')).toEqual('As-I_Lay Dying')
  })
})
