import { insertAndRelease, queryAndRelease } from '../../db/mariadb'
import { PartialBand } from '../api-types/add-edit-event'
import { Band } from '../api-types/band'
import { capitalizeWords } from './normalize-input'

export const processBand = async (inputBand: PartialBand): Promise<Band> => {
  if (inputBand.id) {
    return inputBand as Band
  }

  // normalize input
  const normalizedBand: PartialBand = {
    name: capitalizeWords(inputBand.name)
  }

  // try to find a band with these values
  const [existingBand] = await getBands(normalizedBand)
  if (existingBand) {
    return existingBand
  }

  // create new band
  return await insertBand(normalizedBand)
}

const getBands = async ({ name }: PartialBand): Promise<Band[]> => {
  const result = await queryAndRelease<Band>('SELECT id, name FROM band WHERE name = ? ', [name])
  return result
}

const insertBand = async ({ name }: PartialBand) => {
  const result = await insertAndRelease('INSERT INTO band (name) VALUES (?)', [name])
  return {
    id: result.insertId,
    name
  }
}
