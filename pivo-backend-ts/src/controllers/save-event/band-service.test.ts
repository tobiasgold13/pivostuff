import { PartialBand } from '../api-types/add-edit-event'
import * as normalizeInput from './normalize-input'

const queryAndRelease = jest.fn()
const insertAndRelease = jest.fn()
jest.mock('../../db/mariadb', () => ({ queryAndRelease, insertAndRelease }))

import { processBand } from './band-service'

describe('controllers/save-event/band-service.ts', () => {
  describe('processBand(...)', () => {
    test('should return the band, if input is an actual band', async () => {
      const partialBand: PartialBand = { id: 4, name: 'Amazing Band' }
      const processedBand = await processBand(partialBand)
      expect(processedBand).toStrictEqual(partialBand)
    })

    test('should find the band, that has excactly this data', async () => {
      const partialBand: PartialBand = { name: 'something' }
      const expectedBand = { id: 4, name: 'Something' }
      queryAndRelease.mockResolvedValueOnce([expectedBand])
      const capitalizeSpy = jest.spyOn(normalizeInput, 'capitalizeWords')

      const processedBand = await processBand(partialBand)
      expect(processedBand).toStrictEqual(expectedBand)
      expect(queryAndRelease).toHaveBeenCalledWith('SELECT id, name FROM band WHERE name = ? ', [expectedBand.name])
      expect(capitalizeSpy).toHaveBeenCalledTimes(1)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialBand.name)
    })

    test('should insert the band, if it does not exist', async () => {
      const partialBand: PartialBand = { name: 'something' }
      const expectedBand = { id: 7, name: 'Something' }
      queryAndRelease.mockResolvedValueOnce([])
      insertAndRelease.mockResolvedValueOnce({ insertId: 7 })
      const capitalizeSpy = jest.spyOn(normalizeInput, 'capitalizeWords')

      const processedBand = await processBand(partialBand)
      expect(processedBand).toStrictEqual(expectedBand)
      expect(insertAndRelease).toHaveBeenCalledWith('INSERT INTO band (name) VALUES (?)', [expectedBand.name])
      expect(capitalizeSpy).toHaveBeenCalledTimes(1)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialBand.name)
    })
  })
})
