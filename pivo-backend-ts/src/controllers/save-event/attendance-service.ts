import { batchAndRelease, executeStatement } from '../../db/mariadb'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { PartialAttendance } from '../api-types/add-edit-event'
import { Band } from '../api-types/band'
import { Attendance } from '../api-types/event'
import { processBand } from './band-service'
import { ProcessEventResult } from './event-service'

export const processAttendances = async (user: PivolegUser, event: ProcessEventResult, inputAttendances: PartialAttendance[]): Promise<Attendance[]> => {
  let resultAttendances: Attendance[] = []
  // create attendances
  for (const ia of inputAttendances) {
    if (ia.bands.length > 0) {
      let bands: Band[] = []
      for (const pb of ia.bands) {
        const b = await processBand(pb)
        bands = [...bands, b]
      }
      resultAttendances = [...resultAttendances, { date: ia.date, bands }]
    }
  }
  // store them
  await removeAttendances(event)
  await insertAttendances(event, resultAttendances, user)
  return resultAttendances
}

const removeAttendances = async ({ id }: ProcessEventResult) => {
  await executeStatement('DELETE FROM attendance WHERE event_id = ?', [id])
}

const insertAttendances = async ({ id }: ProcessEventResult, attendances: Attendance[], u: PivolegUser) => {
  const vars: any[] = []
  for (const a of attendances) {
    for (const b of a.bands) {
      vars.push([id, b.id, a.date, u.id])
    }
  }
  if (vars.length > 0) {
    batchAndRelease('INSERT INTO attendance (event_id, band_id, date, user_id) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE id = id', vars)
  }
}
