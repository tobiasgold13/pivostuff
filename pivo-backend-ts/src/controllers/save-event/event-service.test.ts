import { Location } from '../api-types/location'
import * as normalizeInput from './normalize-input'

const executeStatement = jest.fn()
const insertAndRelease = jest.fn()
jest.mock('../../db/mariadb', () => ({ executeStatement, insertAndRelease }))

import { processEvent } from './event-service'
import { PivolegUser } from '../../user-handling/authenticated-request'
import { EventCategory } from '../api-types/event'

describe('controllers/save-event/event-service.ts', () => {
  describe('processEvent(...)', () => {
    test('should update the database, for an existing event', async () => {
      const capitalizeSpy = jest.spyOn(normalizeInput, 'capitalizeWords')
      const user = { id: 28 } as PivolegUser
      const location = { id: 24 } as Location
      const eventData = { id: 1, name: 'COOLES EVENT', start: '2024-06-05', end: '2024-06-06', category: EventCategory.concert }
      await processEvent(user, eventData, location)
      expect(insertAndRelease).toHaveBeenCalledTimes(0)
      expect(executeStatement).toHaveBeenCalledTimes(1)
      expect(executeStatement).toHaveBeenCalledWith('UPDATE event SET name = ?, start = ?, end = ?, category = ?, location_id = ? WHERE id = ?', [normalizeInput.capitalizeWords(eventData.name), eventData.start, eventData.end, 'concert', location.id, eventData.id])
      expect(capitalizeSpy).toHaveBeenCalledWith(eventData.name)
    })

    test('should insert in the database, for a new event', async () => {
      const user = { id: 28 } as PivolegUser
      const location = { id: 24 } as Location
      const eventData = { start: '2024-06-05', end: '2024-06-06', category: EventCategory.festival }
      insertAndRelease.mockResolvedValueOnce({ insertId: 333 })
      const newEvent = await processEvent(user, eventData, location)
      expect(newEvent).toStrictEqual({ ...eventData, id: 333, name: undefined })
      expect(executeStatement).toHaveBeenCalledTimes(0)
      expect(insertAndRelease).toHaveBeenCalledTimes(1)
      expect(insertAndRelease).toHaveBeenCalledWith('INSERT INTO event (name, start, end, category, location_id, user_id) VALUES (?,?,?,?,?,?)', [undefined, eventData.start, eventData.end, 'festival', location.id, user.id])
    })
  })
})
