const separators = [' ', '-', '_']

export const capitalizeWords = (str: string) => {
  str = str.trim()
  str = str.replaceAll('\t', ' ')
  str = processString(str)
  return str
}

const processString = (str: string) => {
  let result = ''
  let currentWord = ''
  let currentSeparator = ''
  let lastCharSeparator = false
  for (let i = 0; i < str.length; i++) {
    const currentChar = str.charAt(i)
    if (separators.includes(currentChar)) {
      if (!lastCharSeparator) {
        lastCharSeparator = true
        result = result.concat(currentSeparator, capitalize(currentWord.trim()))
        currentWord = ''
        currentSeparator = currentChar
      }
    } else {
      lastCharSeparator = false
      currentWord = currentWord.concat(currentChar)
    }
  }
  result = result.concat(currentSeparator, capitalize(currentWord.trim()))

  return result
}

const capitalize = (s: string) => s && s[0].toUpperCase() + s.slice(1).toLowerCase()
