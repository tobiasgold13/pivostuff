import { PartialLocation } from '../api-types/add-edit-event'
import * as normalizeInput from './normalize-input'

const queryAndRelease = jest.fn()
const insertAndRelease = jest.fn()
jest.mock('../../db/mariadb', () => ({ queryAndRelease, insertAndRelease }))

import { processLocation } from './location-service'

describe('controllers/save-event/location-service.ts', () => {
  describe('processLocation(...)', () => {
    test('should simply return the Location, if input is an actual Location', async () => {
      const partialLocation: PartialLocation = { id: 3, name: 'Something', city: 'Somewhere', country: 'Wherever' }
      const processedLocation = await processLocation(partialLocation)
      expect(processedLocation).toStrictEqual(partialLocation)
    })

    test('should find the location, that has excactly this data', async () => {
      const partialLocation: PartialLocation = { name: 'something', city: 'somewhere', country: 'wherever' }
      const expectedLocation = { id: 4, name: 'Something', city: 'Somewhere', country: 'Wherever' }
      queryAndRelease.mockResolvedValueOnce([expectedLocation])
      const capitalizeSpy = jest.spyOn(normalizeInput, 'capitalizeWords')

      const processedLocation = await processLocation(partialLocation)
      expect(processedLocation).toStrictEqual(expectedLocation)
      expect(queryAndRelease).toHaveBeenCalledWith('SELECT id, country, city, name FROM location WHERE country = ? AND city = ? AND name = ?', [expectedLocation.country, expectedLocation.city, expectedLocation.name])
      expect(capitalizeSpy).toHaveBeenCalledTimes(3)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.name)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.city)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.country)
    })

    test('should find the location, that has excactly this data with name = null', async () => {
      const partialLocation: PartialLocation = { city: 'somewhere', country: 'wherever' }
      const expectedLocation = { id: 4, city: 'Somewhere', country: 'Wherever' }
      queryAndRelease.mockResolvedValueOnce([expectedLocation])

      const processedLocation = await processLocation(partialLocation)
      expect(processedLocation).toStrictEqual(expectedLocation)
      expect(queryAndRelease).toHaveBeenCalledWith('SELECT id, country, city, name FROM location WHERE country = ? AND city = ? AND name IS NULL', [expectedLocation.country, expectedLocation.city])
    })

    test('should insert the location, if it does not exist', async () => {
      const partialLocation: PartialLocation = { name: 'something', city: 'somewhere', country: 'wherever' }
      const expectedLocation = { id: 5, name: 'Something', city: 'Somewhere', country: 'Wherever' }
      queryAndRelease.mockResolvedValueOnce([])
      insertAndRelease.mockResolvedValueOnce({ insertId: 5 })
      const capitalizeSpy = jest.spyOn(normalizeInput, 'capitalizeWords')

      const processedLocation = await processLocation(partialLocation)
      expect(processedLocation).toStrictEqual(expectedLocation)
      expect(insertAndRelease).toHaveBeenCalledWith('INSERT INTO location (country, city, name) VALUES (?,?,?)', [expectedLocation.country, expectedLocation.city, expectedLocation.name])
      expect(capitalizeSpy).toHaveBeenCalledTimes(3)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.name)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.city)
      expect(capitalizeSpy).toHaveBeenCalledWith(partialLocation.country)
    })

    test('should insert the location, if it does not exist (name is optional)', async () => {
      const partialLocation: PartialLocation = { city: 'Somewhere', country: 'Wherever' }
      const expectedLocation = { id: 5, name: undefined, city: 'Somewhere', country: 'Wherever' }
      queryAndRelease.mockResolvedValueOnce([])
      insertAndRelease.mockResolvedValueOnce({ insertId: 5 })

      const processedLocation = await processLocation(partialLocation)
      expect(processedLocation).toStrictEqual(expectedLocation)
      expect(insertAndRelease).toHaveBeenCalledWith('INSERT INTO location (country, city, name) VALUES (?,?,?)', [expectedLocation.country, expectedLocation.city, undefined])
    })
  })
})
