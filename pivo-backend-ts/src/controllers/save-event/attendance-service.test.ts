import { PivolegUser } from '../../user-handling/authenticated-request'
import { PartialAttendance } from '../api-types/add-edit-event'
import { EventDetail } from '../api-types/event'

const batchAndRelease = jest.fn()
const executeStatement = jest.fn()
jest.mock('../../db/mariadb', () => ({ batchAndRelease, executeStatement }))

import { processAttendances } from './attendance-service'

describe('controllers/save-event/attendance-service.ts', () => {
  describe('processAttendances(...)', () => {
    test('should only return attendances with attached bands', async () => {
      const dummyUser = { id: 133 } as PivolegUser
      const dummyEvent = { id: 667 } as EventDetail
      const partialAttendances: PartialAttendance[] = [
        { date: '2012-12-12', bands: [] },
        { date: '2012-12-13', bands: [{ id: 42, name: 'Exodus' }] }
      ]
      const actualAttendances = await processAttendances(dummyUser, dummyEvent, partialAttendances)
      expect(actualAttendances).toStrictEqual([partialAttendances[1]])
    })

    test('should remove old and store new attendances in one database call', async () => {
      const dummyUser = { id: 133 } as PivolegUser
      const dummyEvent = { id: 667 } as EventDetail
      const partialAttendances: PartialAttendance[] = [
        { date: '2012-12-12', bands: [{ id: 12, name: 'TGI' }] },
        { date: '2012-12-13', bands: [{ id: 42, name: 'Exodus' }] }
      ]
      await processAttendances(dummyUser, dummyEvent, partialAttendances)
      expect(executeStatement).toHaveBeenCalledTimes(1)
      expect(executeStatement).toHaveBeenCalledWith('DELETE FROM attendance WHERE event_id = ?', [dummyEvent.id])
      expect(batchAndRelease).toHaveBeenCalledTimes(1)
      expect(batchAndRelease).toHaveBeenCalledWith('INSERT INTO attendance (event_id, band_id, date, user_id) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE id = id', [[dummyEvent.id, 12, '2012-12-12', 133], [dummyEvent.id, 42, '2012-12-13', 133]])
    })

    test('should not call database, if no attendances should be inserted ', async () => {
      const dummyUser = { id: 133 } as PivolegUser
      const dummyEvent = { id: 667 } as EventDetail
      const partialAttendances: PartialAttendance[] = [{ date: '2012-12-12', bands: [] }]
      await processAttendances(dummyUser, dummyEvent, partialAttendances)
      expect(batchAndRelease).not.toHaveBeenCalled()
    })
  })
})
