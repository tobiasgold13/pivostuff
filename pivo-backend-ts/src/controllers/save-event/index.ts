import { Router, Request, Response } from 'express'
import { SAVE_EVENT_ENDPOINT, SaveEventRequest } from '../api-types/add-edit-event'
import { processLocation } from './location-service'
import { processEvent } from './event-service'
import { EventCategory, EventDetail } from '../api-types/event'
import { processAttendances } from './attendance-service'
import { getCurrentUser } from '../../user-handling/current-user'
import { array, number, object, ObjectSchema, string } from 'yup'
import { appendMetrics } from '../admin/admin-metrics-service'

export const saveEventRouter = Router()

const saveEventRequestSchema: ObjectSchema<SaveEventRequest> = object({
  id: number().positive().integer(),
  name: string(),
  start: string().required().matches(/\d{4}-\d{2}-\d{2}/, { excludeEmptyString: true }),
  end: string().required().matches(/\d{4}-\d{2}-\d{2}/, { excludeEmptyString: true }),
  category: string().oneOf([EventCategory.concert, EventCategory.festival]),
  location: object({
    id: number().positive().integer(),
    name: string(),
    city: string().required(),
    country: string().required()
  }).required(),
  attendances: array(object({
    date: string().required().matches(/\d{4}-\d{2}-\d{2}/, { excludeEmptyString: true }),
    bands: array(object({
      id: number().positive().integer(),
      name: string().required()
    })).required()
  })).required()
})

saveEventRouter.post(SAVE_EVENT_ENDPOINT, async (req: Request, res: Response) => {
  let saveEventRequest: SaveEventRequest
  try {
    saveEventRequest = await saveEventRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }
  const user = getCurrentUser(req)

  const location = await processLocation(saveEventRequest.location)
  const eventData = await processEvent(user, saveEventRequest, location)
  const attendances = await processAttendances(user, eventData, saveEventRequest.attendances)
  const eventResult: EventDetail = {
    ...eventData,
    location,
    attendances
  }

  await appendMetrics(user)

  return res.json(eventResult)
})
