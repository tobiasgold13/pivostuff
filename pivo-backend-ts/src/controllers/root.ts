import { Router, Response } from 'express'

export const rootRouter = Router()

rootRouter.get('/', (_, res: Response) => {
  console.log(`[${new Date().toISOString()}] request to "/"`)
  res.send('🤘')
})
