import { Router, Request, Response } from 'express'
import { queryAndRelease } from '../db/mariadb'
import { PivolegUser } from '../user-handling/authenticated-request'
import { getCurrentUser } from '../user-handling/current-user'
import { Attendance, EVENT_DETAIL_GET_ENDPOINT, EVENT_OVERVIEW_GET_ENDPOINT, EVENT_YEARS_GET_ENDPOINT, EventCategory, EventDetail, EventOverview } from './api-types/event'
import { Band } from './api-types/band'

export const eventsRouter = Router()

eventsRouter.get(EVENT_YEARS_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const years = await getYearsForEvents(user)
  res.json(years)
})

const getYearsForEvents = async (user: PivolegUser): Promise<number[]> => {
  const result = await queryAndRelease<{year: number}>('SELECT DISTINCT YEAR(start) as year FROM event WHERE user_id = ? ORDER BY year DESC', [user.id])
  return result.map(r => r.year)
}

eventsRouter.get(EVENT_OVERVIEW_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const events = await getEventsOverviewForYear(user, req.params.year)
  res.json(events)
})

const getEventsOverviewForYear = async (user: PivolegUser, year: string): Promise<EventOverview[]> => {
  const result = await queryAndRelease<EventOverview>('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.user_id = ? AND YEAR(e.start) = ? ORDER BY e.start', [user.id, year])
  return result
}

eventsRouter.get(EVENT_DETAIL_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  // make sure it's an event for this user
  const event = await getEventForId(req.params.id, user)
  if (!event) {
    return res.status(404).send()
  }
  event.attendances = await getAttendancesForEvent(event)
  res.json(event)
})

const getEventForId = async (id: string, user: PivolegUser): Promise<EventDetail | undefined> => {
  const [result] = await queryAndRelease<{id:number, name:string, start: string, end:string, category?: EventCategory, locationId: number, locationName:string, city:string, country:string}>('SELECT DISTINCT e.id, e.name, e.start, e.end, e.category, l.id AS locationId, l.name AS locationName, l.city, l.country FROM event e JOIN location l ON l.id = e.location_id WHERE e.id = ? AND e.user_id = ?', [id, user.id])
  if (!result) {
    return undefined
  }
  return {
    id: result.id,
    name: result.name ?? undefined,
    start: result.start,
    end: result.end,
    category: result.category ?? undefined,
    location: {
      id: result.locationId,
      name: result.locationName ?? undefined,
      city: result.city,
      country: result.country
    },
    attendances: []
  }
}

const getAttendancesForEvent = async (event: EventDetail): Promise<Attendance[]> => {
  const result = await queryAndRelease<{date:string, id:number, name:string}>('SELECT a.date, b.id, b.name FROM attendance a JOIN band b ON a.band_id = b.id WHERE event_id = ? ORDER BY a.date', [event.id])
  const attendanceMap = new Map<string, Band[]>()
  for (const r of result) {
    const bands = attendanceMap.get(r.date) || []
    bands.push({ id: r.id, name: r.name })
    attendanceMap.set(r.date, bands)
  }
  const attendances: Attendance[] = []
  for (const date of attendanceMap.keys()) {
    attendances.push({ date, bands: attendanceMap.get(date)! })
  }
  return attendances
}
