import { Request, Response, Router } from 'express'
import { queryAndRelease } from '../db/mariadb'
import { PivolegUser } from '../user-handling/authenticated-request'
import { getCurrentUser } from '../user-handling/current-user'
import { OLEGS_SPECIAL_LIST_GET_ENDPOINT, OlegsSpecialListRow } from './api-types/olegs-special-list'

export const olegsRouter = Router()

olegsRouter.get(OLEGS_SPECIAL_LIST_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const flatList = await getOlegsSpecialData(user)
  const data: OlegsSpecialListRow[] = flatList.map(flat => ({ band: { id: flat.id, name: flat.name }, count: flat.count }))
  res.json(data)
})

interface OlegsSpecialFlatRow {
  id: number
  name: string
  count: number
}

const getOlegsSpecialData = async (user: PivolegUser): Promise<OlegsSpecialFlatRow[]> => {
  const result = await queryAndRelease<OlegsSpecialFlatRow>('SELECT b.id, b.name, COUNT(1) AS count FROM attendance a JOIN band b ON a.band_id = b.id WHERE user_id = ? GROUP BY a.band_id ORDER BY b.name', [user.id])
  return result
}
