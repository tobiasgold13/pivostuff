import express from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { eventsRouter } from './events'
import { EVENT_DETAIL_GET_ENDPOINT, EVENT_OVERVIEW_GET_ENDPOINT, EVENT_YEARS_GET_ENDPOINT, EventOverview } from './api-types/event'

describe('controllers/events.ts', () => {
  const dummyApp = express()
  dummyApp.use(eventsRouter)

  test(`${EVENT_YEARS_GET_ENDPOINT} should call the database and return the available years for the current user`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667 })
    const dummyYears = [{ year: 2012 }, { year: 2021 }]
    queryAndRelease.mockResolvedValueOnce(dummyYears)

    const response = await request(dummyApp).get(EVENT_YEARS_GET_ENDPOINT)
    expect(response.body).toStrictEqual(dummyYears.map(y => y.year))

    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT YEAR(start) as year FROM event WHERE user_id = ? ORDER BY year DESC', [667])
  })

  test(`${EVENT_OVERVIEW_GET_ENDPOINT} should return the basic data about the corresponding events`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 668 })
    const dummyData: EventOverview[] = [{ id: 1, name: 'asdf', start: '2024-02-02', city: 'In Nürnberg', location: 'Löwensaal' }]
    queryAndRelease.mockResolvedValueOnce(dummyData)

    const response = await request(dummyApp).get(EVENT_OVERVIEW_GET_ENDPOINT.replace(':year', '2024'))
    expect(response.body).toStrictEqual(dummyData)

    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.user_id = ? AND YEAR(e.start) = ? ORDER BY e.start', [668, '2024'])
  })

  test(`${EVENT_DETAIL_GET_ENDPOINT} should return 404 if event not found`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667 })
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).get(EVENT_DETAIL_GET_ENDPOINT.replace(':id', '42'))
    expect(response.statusCode).toEqual(404)
    expect(queryAndRelease).toHaveBeenCalledTimes(1)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT e.id, e.name, e.start, e.end, e.category, l.id AS locationId, l.name AS locationName, l.city, l.country FROM event e JOIN location l ON l.id = e.location_id WHERE e.id = ? AND e.user_id = ?', ['42', 667])
  })

  test(`${EVENT_DETAIL_GET_ENDPOINT} should make the correct database calls`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667 })
    queryAndRelease.mockResolvedValueOnce([{ id: 12 }])
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).get(EVENT_DETAIL_GET_ENDPOINT.replace(':id', '42'))
    expect(response.statusCode).toEqual(200)
    expect(queryAndRelease).toHaveBeenCalledTimes(2)
    expect(queryAndRelease).toHaveBeenNthCalledWith(2, 'SELECT a.date, b.id, b.name FROM attendance a JOIN band b ON a.band_id = b.id WHERE event_id = ? ORDER BY a.date', [12])
  })

  test(`${EVENT_DETAIL_GET_ENDPOINT} should return the aggregated data`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 667 })
    queryAndRelease.mockResolvedValueOnce([{ id: 12, name: 'nice concert', start: '2022-02-22', end: '2022-03-30', category: 'concert', locationId: -4, locationName: 'Irish', city: 'Lichtenfels', country: 'Deutschland' }])
    queryAndRelease.mockResolvedValueOnce([{ date: '2022-02-02', id: 5, name: 'b1' }, { date: '2022-02-02', id: 6, name: 'b2' }, { date: '2022-02-03', id: 7, name: 'b3' }])
    const response = await request(dummyApp).get(EVENT_DETAIL_GET_ENDPOINT.replace(':id', '42'))
    expect(response.body).toStrictEqual({
      id: 12,
      name: 'nice concert',
      start: '2022-02-22',
      end: '2022-03-30',
      category: 'concert',
      location: {
        id: -4,
        name: 'Irish',
        city: 'Lichtenfels',
        country: 'Deutschland'
      },
      attendances: [
        { date: '2022-02-02', bands: [{ id: 5, name: 'b1' }, { id: 6, name: 'b2' }] },
        { date: '2022-02-03', bands: [{ id: 7, name: 'b3' }] }
      ]
    })
  })
})
