import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { locationsRouter } from './locations'
import { LOCATION_DETAILS_GET_ENDPOINT, LOCATION_SEARCH_POST_ENDPOINT } from './api-types/location'

describe('controllers/bands.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(locationsRouter)

  test(`${LOCATION_SEARCH_POST_ENDPOINT} should return 400 (Bad Request) if no search string is passed`, async () => {
    const response = await request(dummyApp).post(LOCATION_SEARCH_POST_ENDPOINT).send({ query: ' ' })
    expect(response.statusCode).toEqual(400)
  })

  test(`${LOCATION_SEARCH_POST_ENDPOINT} should call the database and return result`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' }])
    const response = await request(dummyApp).post(LOCATION_SEARCH_POST_ENDPOINT).send({ query: 'one' })
    expect(response.body).toStrictEqual([{ name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT id, country, city, name FROM location WHERE CONCAT_WS(" ",country, city, name) LIKE concat("%" , ?, "%")', ['one'])
  })

  test(`${LOCATION_DETAILS_GET_ENDPOINT} should return 404 (Bad Request) if no location with this id exists`, async () => {
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).get(LOCATION_DETAILS_GET_ENDPOINT)
    expect(response.statusCode).toEqual(404)
  })

  test(`${LOCATION_DETAILS_GET_ENDPOINT} should call database and return location details for id`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 666, name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' }])
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([])
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    const response = await request(dummyApp).get(LOCATION_DETAILS_GET_ENDPOINT.replace(':id', '666'))
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual({
      location: expect.anything(),
      events: expect.any(Array),
      bands: expect.any(Array)
    })
    expect(response.body.location).toStrictEqual({ id: 666, name: 'Paunchy Cats', city: 'Lichtenfels', country: 'Deutschland' })
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT id, country, city, name FROM location WHERE id = ?', ['666'])
    expect(queryAndRelease).toHaveBeenCalledTimes(3)
  })

  test(`${LOCATION_DETAILS_GET_ENDPOINT} should call database and return events for location`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 1234 }])
    queryAndRelease.mockResolvedValueOnce([{ id: 5678, name: 'event-nane', start: '2012-12-12' }])
    queryAndRelease.mockResolvedValueOnce([])
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    const response = await request(dummyApp).get(LOCATION_DETAILS_GET_ENDPOINT)
    expect(response.body.events).toStrictEqual([{ id: 5678, name: 'event-nane', start: '2012-12-12' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.location_id = ? AND e.user_id = ? ORDER BY e.start', [1234, 1337])
  })

  test(`${LOCATION_DETAILS_GET_ENDPOINT} should call database and return events for location`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 1234 }])
    queryAndRelease.mockResolvedValueOnce([])
    queryAndRelease.mockResolvedValueOnce([{ id: 1279, name: 'TGI' }])
    getCurrentUser.mockReturnValueOnce({ id: 1337 })
    const response = await request(dummyApp).get(LOCATION_DETAILS_GET_ENDPOINT)
    expect(response.body.bands).toStrictEqual([{ id: 1279, name: 'TGI' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT b.id, b.name FROM band b JOIN attendance a ON a.band_id = b.id JOIN event e ON e.id = a.event_id WHERE e.location_id = ? AND a.user_id = ? ORDER BY b.name', [1234, 1337])
  })
})
