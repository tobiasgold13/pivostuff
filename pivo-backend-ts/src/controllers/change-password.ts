import { Request, Response, Router } from 'express'
import { CHANGE_PASSWORD_ENDPOINT, ChangePasswordRequest, WHO_AM_I_ENDPOINT } from './api-types/user-handling'
import { object, ObjectSchema, string } from 'yup'
import { compare, hash } from 'bcrypt'
import { getCurrentUser } from '../user-handling/current-user'
import { PivolegUser } from '../user-handling/authenticated-request'
import { executeStatement } from '../db/mariadb'

export const changePasswordRouter = Router()

const changePasswordRequestSchema: ObjectSchema<ChangePasswordRequest> = object({
  oldPassword: string().required(),
  newPassword: string().required()
})

changePasswordRouter.post(CHANGE_PASSWORD_ENDPOINT, async (req: Request, res: Response) => {
  let changePasswordRequest: ChangePasswordRequest
  try {
    changePasswordRequest = await changePasswordRequestSchema.validate(req.body)
  } catch (err) {
    console.error(err)
    return res.status(400).send()
  }
  const user = getCurrentUser(req)

  const oldPasswordCorrect = await compare(changePasswordRequest.oldPassword, user.password)
  if (!oldPasswordCorrect) {
    return res.status(403).send()
  }

  const newPasswordHash = await hash(changePasswordRequest.newPassword, 4)
  await updatePassword(user, newPasswordHash)
  return res.status(204).send()
})

const updatePassword = async (user: PivolegUser, passwordHash: string): Promise<void> => (executeStatement('UPDATE user SET password = ? WHERE id = ?', [passwordHash, user.id]))

changePasswordRouter.get(WHO_AM_I_ENDPOINT, (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  res.json({ name: user.name })
})
