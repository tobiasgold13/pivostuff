import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { chartsRouter } from './charts'
import { CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT, CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT, CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT, CHARTS_TOP_10_BANDS_GET_ENDPOINT } from './api-types/charts'

describe('controllers/charts.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(chartsRouter)

  test(`${CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT} should function correctly`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ year: 2019, count: 13 }, { year: 2022, count: 3 }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT)
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual([{ year: 2019, count: 13 }, { year: 2020, count: 0 }, { year: 2021, count: 0 }, { year: 2022, count: 3 }])
    expect(queryAndRelease).toHaveBeenCalledTimes(1)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT YEAR(start) AS year, COUNT(1) AS count FROM event WHERE user_id = ? GROUP BY YEAR(start) ORDER BY year', [12])
  })

  test(`${CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT} should function correctly`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ year: 2019, count: 13 }, { year: 2022, count: 3 }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT)
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual([{ year: 2019, count: 13 }, { year: 2020, count: 0 }, { year: 2021, count: 0 }, { year: 2022, count: 3 }])
    expect(queryAndRelease).toHaveBeenCalledTimes(1)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT YEAR(date) AS year, COUNT(1) AS count FROM attendance WHERE user_id = ? GROUP BY YEAR(date) ORDER BY year', [12])
  })

  test(`${CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT} should function correctly`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ count: 13, category: 'aggregate' }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT)
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual([{ count: 13, category: 'aggregate' }])
    expect(queryAndRelease).toHaveBeenCalledTimes(1)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT category, COUNT(1) AS count FROM event WHERE user_id = ? GROUP BY category', [12])
  })

  test(`${CHARTS_TOP_10_BANDS_GET_ENDPOINT} should return the top 10 most seen bands & count`, async () => {
    getCurrentUser.mockReturnValueOnce({ id: 5 })
    const data = [{ id: 1, name: 'The Bones', count: 3 }, { id: 2, name: 'STYG', count: 12 }]
    queryAndRelease.mockResolvedValueOnce(data)
    const response = await request(dummyApp).get(CHARTS_TOP_10_BANDS_GET_ENDPOINT)
    expect(response.body).toStrictEqual(data)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT b.id, b.name, COUNT(*) AS count FROM band b JOIN attendance a ON b.id = a.band_id WHERE a.user_id = ? GROUP BY b.id ORDER BY count DESC, b.name ASC LIMIT 10', [5])
  })
})
