import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { bandsRouter } from './bands'
import { BAND_DETAILS_GET_ENDPOINT, SEARCH_BANDS_ENDPOINT } from './api-types/band'

describe('controllers/bands.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(bandsRouter)

  test(`${BAND_DETAILS_GET_ENDPOINT} should return 404 if band is not found`, async () => {
    queryAndRelease.mockResolvedValueOnce([])
    const response = await request(dummyApp).get(BAND_DETAILS_GET_ENDPOINT.replace(':id', '66'))
    expect(response.statusCode).toEqual(404)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT id, name FROM band WHERE id = ?', ['66'])
  })

  test(`${BAND_DETAILS_GET_ENDPOINT} should make the correct database calls`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 66, name: 'Slayer' }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(BAND_DETAILS_GET_ENDPOINT.replace(':id', '66'))
    expect(response.statusCode).toEqual(200)
    expect(queryAndRelease).toHaveBeenCalledTimes(2)
    expect(queryAndRelease).toHaveBeenNthCalledWith(2, 'SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN attendance a ON e.id = a.event_id JOIN location l ON l.id = e.location_id WHERE e.user_id = ? AND a.band_id = ? ORDER BY e.start', [12, 66])
  })

  test(`${BAND_DETAILS_GET_ENDPOINT} should return aggregated data`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 66, name: 'Slayer' }])
    queryAndRelease.mockResolvedValueOnce([{ id: 12, name: 'nice concert', start: '2022-02-22', end: '2022-03-30', location: 'Irish', city: 'Lichtenfels' }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(BAND_DETAILS_GET_ENDPOINT.replace(':id', '66'))
    expect(response.body).toStrictEqual({
      band: { id: 66, name: 'Slayer' },
      events: [{
        id: 12,
        name: 'nice concert',
        start: '2022-02-22',
        end: '2022-03-30',
        location: 'Irish',
        city: 'Lichtenfels'
      }]
    })
  })

  test(`${SEARCH_BANDS_ENDPOINT} should return 400 (Bad Request) if no search string is passed`, async () => {
    const response = await request(dummyApp).post(SEARCH_BANDS_ENDPOINT).send({ query: ' ' })
    expect(response.statusCode).toEqual(400)
  })

  test(`${SEARCH_BANDS_ENDPOINT} should call the database and return result`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 66, name: 'Slayer' }])
    const response = await request(dummyApp).post(SEARCH_BANDS_ENDPOINT).send({ query: 'one' })
    expect(response.body).toStrictEqual([{ id: 66, name: 'Slayer' }])
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT DISTINCT b.id, b.name FROM band b WHERE name LIKE concat("%" , ?, "%") ORDER BY b.name', ['one'])
  })
})
