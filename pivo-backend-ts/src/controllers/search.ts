import { Request, Response, Router } from 'express'
import { SEARCH_ENDPOINT, SearchResponse } from './api-types/search'
import { Band } from './api-types/band'
import { queryAndRelease } from '../db/mariadb'
import { PivolegUser } from '../user-handling/authenticated-request'
import { getCurrentUser } from '../user-handling/current-user'
import { Location } from './api-types/location'
import { EventOverview } from './api-types/event'

export const searchRouter = Router()

searchRouter.post(SEARCH_ENDPOINT, async (req:Request, res:Response) => {
  const query = req.body?.query?.trim()
  if (!query) {
    return res.status(400).send()
  }
  const user = getCurrentUser(req)

  const bands = await getBandsLike(query, user)
  const locations = await getLocationsLike(query, user)
  const events = await getEventsLike(query, user)
  const response: SearchResponse = {
    bands,
    locations,
    events
  }
  return res.json(response)
})

const getBandsLike = async (nameSearchStr: string, { id }: PivolegUser): Promise<Band[]> => {
  const result = await queryAndRelease<Band>('SELECT DISTINCT b.id, b.name FROM band b JOIN attendance a ON a.band_id = b.id WHERE b.name LIKE concat("%" , ?, "%") AND a.user_id = ? ORDER BY b.name', [nameSearchStr, id])
  return result
}

const getLocationsLike = async (query: string, { id }: PivolegUser): Promise<Location[]> => {
  const result = await queryAndRelease<Location>('SELECT DISTINCT l.id, l.country, l.city, l.name FROM location l JOIN event e ON e.location_id = l.id WHERE CONCAT_WS(" ", l.country, l.city, l.name) LIKE concat("%" , ?, "%") AND e.user_id = ?', [query, id])
  return result
}

const getEventsLike = async (query: string, { id }: PivolegUser): Promise<EventOverview[]> => {
  const result = await queryAndRelease<EventOverview>('SELECT DISTINCT e.id, e.name, e.start, l.name AS location, l.city FROM event e JOIN location l ON l.id = e.location_id WHERE e.name LIKE concat("%" , ?, "%") AND e.user_id = ? ORDER BY e.start', [query, id])
  return result
}
