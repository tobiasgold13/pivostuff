import { Router, Response } from 'express'
import os from 'os'
import { queryAndRelease } from '../db/mariadb'
import { DEBUG_GET_ENDPOINT, DebugResponse } from './api-types/user-handling'

export const debugRouter = Router()

debugRouter.get(DEBUG_GET_ENDPOINT, async (_, res: Response) => {
  const database: any = {}
  try {
    const now = await selectNow()
    database.selectNow = now
    const schemas = await getSchemas()
    database.schemas = schemas
  } catch (err) {
    database.error = err
  }

  const data: DebugResponse = {
    msg: 'this message is from the backend. database connection is optional',
    date: new Date().toLocaleString('de'),
    platform: os.platform(),
    hostname: os.hostname(),
    database
  }
  return res.json(data)
})

const selectNow = async (): Promise<string> => {
  const [result] = await queryAndRelease<string>('SELECT NOW()')
  return result
}

const getSchemas = async (): Promise<string[]> => {
  const result = await queryAndRelease<{Database: string}>('SHOW SCHEMAS')
  return result.map(r => r.Database)
}
