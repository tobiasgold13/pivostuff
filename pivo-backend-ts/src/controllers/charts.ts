import { Request, Response, Router } from 'express'
import { queryAndRelease } from '../db/mariadb'
import { PivolegUser } from '../user-handling/authenticated-request'
import { getCurrentUser } from '../user-handling/current-user'
import { CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT, CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT, CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT, CHARTS_TOP_10_BANDS_GET_ENDPOINT, ChartsEventCountPerCategoryResponseElement, ChartsCountPerYearResponseElement, ChartsTop10Band } from './api-types/charts'

export const chartsRouter = Router()

chartsRouter.get(CHARTS_EVENT_COUNT_PER_YEAR_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const aggregate = await getEventCountPerYear(user)
  res.json(addMissingYears(aggregate))
})

const getEventCountPerYear = async (user: PivolegUser): Promise<ChartsCountPerYearResponseElement[]> => {
  const aggregate = await queryAndRelease<ChartsCountPerYearResponseElement>('SELECT YEAR(start) AS year, COUNT(1) AS count FROM event WHERE user_id = ? GROUP BY YEAR(start) ORDER BY year', [user.id])
  return aggregate
}

chartsRouter.get(CHARTS_SHOW_COUNT_PER_YEAR_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const aggregate = await getShowCountPerYear(user)
  res.json(addMissingYears(aggregate))
})

const getShowCountPerYear = async (user: PivolegUser): Promise<ChartsCountPerYearResponseElement[]> => {
  const allUsers = await queryAndRelease<ChartsCountPerYearResponseElement>('SELECT YEAR(date) AS year, COUNT(1) AS count FROM attendance WHERE user_id = ? GROUP BY YEAR(date) ORDER BY year', [user.id])
  return allUsers
}

const addMissingYears = (input: ChartsCountPerYearResponseElement[]) => {
  const output: ChartsCountPerYearResponseElement[] = []
  let lastYear = undefined
  for (const el of input) {
    if (!lastYear) {
      lastYear = el.year
    }
    // add missing years with count 0
    while (lastYear + 1 < el.year) {
      lastYear++
      output.push({ year: lastYear, count: 0 })
    }
    lastYear = el.year
    output.push(el)
  }
  return output
}

chartsRouter.get(CHARTS_EVENT_COUNT_PER_CATEGORY_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const aggregate = await getEventCountPerCategory(user)
  res.json(aggregate)
})

const getEventCountPerCategory = async (user: PivolegUser): Promise<ChartsEventCountPerCategoryResponseElement[]> => {
  const aggregate = await queryAndRelease<ChartsEventCountPerCategoryResponseElement>('SELECT category, COUNT(1) AS count FROM event WHERE user_id = ? GROUP BY category', [user.id])
  return aggregate
}

chartsRouter.get(CHARTS_TOP_10_BANDS_GET_ENDPOINT, async (req: Request, res: Response) => {
  const user = getCurrentUser(req)
  const top10Bands = await getTop10Bands(user)
  res.json(top10Bands)
})

const getTop10Bands = async (user: PivolegUser): Promise<ChartsTop10Band[]> => {
  const top10Bands = await queryAndRelease<ChartsTop10Band>('SELECT b.id, b.name, COUNT(*) AS count FROM band b JOIN attendance a ON b.id = a.band_id WHERE a.user_id = ? GROUP BY b.id ORDER BY count DESC, b.name ASC LIMIT 10', [user.id])
  return top10Bands
}