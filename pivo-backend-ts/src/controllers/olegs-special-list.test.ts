import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('../db/mariadb', () => ({ queryAndRelease }))
const getCurrentUser = jest.fn()
jest.mock('../user-handling/current-user', () => ({ getCurrentUser }))

import { olegsRouter } from './olegs-special-list'
import { OLEGS_SPECIAL_LIST_GET_ENDPOINT } from './api-types/olegs-special-list'

describe('controllers/olegs-special-list.ts', () => {
  const dummyApp = express()
  dummyApp.use(json())
  dummyApp.use(olegsRouter)

  test(`${OLEGS_SPECIAL_LIST_GET_ENDPOINT} should function correctly`, async () => {
    queryAndRelease.mockResolvedValueOnce([{ id: 666, name: 'The Bones', count: 7 }])
    getCurrentUser.mockReturnValueOnce({ id: 12 })
    const response = await request(dummyApp).get(OLEGS_SPECIAL_LIST_GET_ENDPOINT)
    expect(response.statusCode).toEqual(200)
    expect(response.body).toStrictEqual([{ band: { id:666, name:'The Bones' }, count:7 }])
    expect(queryAndRelease).toHaveBeenCalledTimes(1)
    expect(queryAndRelease).toHaveBeenCalledWith('SELECT b.id, b.name, COUNT(1) AS count FROM attendance a JOIN band b ON a.band_id = b.id WHERE user_id = ? GROUP BY a.band_id ORDER BY b.name', [12])
  })
})
