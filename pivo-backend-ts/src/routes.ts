import { Express, Request, Response, NextFunction } from 'express'
require('express-async-errors')
import { rootRouter } from './controllers/root'
import { loginRouter } from './controllers/login'
import { authorizationRouter } from './user-handling/authorization-controller'
import { eventsRouter } from './controllers/events'
import { bandsRouter } from './controllers/bands'
import { locationsRouter } from './controllers/locations'
import { saveEventRouter } from './controllers/save-event'
import { changePasswordRouter } from './controllers/change-password'
import { searchRouter } from './controllers/search'
import { chartsRouter } from './controllers/charts'
import { olegsRouter } from './controllers/olegs-special-list'
import { adminUsersRouter } from './controllers/admin/admin-users'
import { adminSessionsRouter } from './controllers/admin/admin-sessions'
import { adminMetricsRouter } from './controllers/admin/admin-metrics'
import { adminOrphansRouter } from './controllers/admin/admin-orphans'

export const configureRoutes = (app: Express) => {
  app.use(rootRouter)
  // app.use(debugRouter)
  app.use(loginRouter)
  app.use(authorizationRouter)
  // everything underneath here, requires authentication & will be provided a user object
  app.use(eventsRouter)
  app.use(bandsRouter)
  app.use(locationsRouter)
  app.use(saveEventRouter)
  app.use(changePasswordRouter)
  app.use(searchRouter)
  app.use(chartsRouter)
  app.use(olegsRouter)

  // admin
  app.use(adminUsersRouter)
  app.use(adminSessionsRouter)
  app.use(adminMetricsRouter)
  app.use(adminOrphansRouter)

  // error handling middleware, to not crash the app if anything happens
  // eslint-disable-next-line @typescript-eslint/no-unused-vars, promise/prefer-await-to-callbacks
  app.use((err:Error, _req: Request, res: Response, _next: NextFunction) => {
    const now = new Date()
    const timestamp = now.toISOString().replaceAll('-', '').replace('T', '_').replaceAll(':', '').substring(0, 15)
    const logId = `${timestamp}-${now.getTime().toString(16)}`

    console.error(logId, err)
    res.status(500).send(logId)
  })
}
