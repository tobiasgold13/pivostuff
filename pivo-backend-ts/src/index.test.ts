describe('index.ts', () => {
  const appMock = {
    listen: jest.fn(),
    use: jest.fn()
  }
  jest.mock('express', () => ({
    ...jest.requireActual('express'),
    __esModule: true,
    default: () => appMock
  }))
  const serverlessMock = jest.fn()
  jest.mock('serverless-http', () => serverlessMock)

  beforeEach(() => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-function-type
    appMock.listen.mockImplementation((_, fn: Function) => { fn() })
  })

  const configureRoutes = jest.fn()
  jest.mock('./routes', () => ({ configureRoutes }))

  test('should print the port to console on startup', () => {
    const initialEnvPort = process.env.PORT
    process.env.PORT = '1337'
    const consoleSpy = jest.spyOn(console, 'log').mockImplementation()
    require('./index')
    expect(consoleSpy).toHaveBeenCalledWith('🚀 Server listening on port 1337')
    expect(appMock.listen).toHaveBeenCalledWith(1337, expect.anything())
    expect(appMock.listen).toHaveBeenCalledTimes(1)
    process.env.PORT = initialEnvPort
  })

  // this is the AWS Lambda case
  test('should not start the server if no port is passed', () => {
    const initialEnvPort = process.env.PORT
    delete process.env.PORT
    const consoleSpy = jest.spyOn(console, 'log').mockImplementation()
    require('./index')
    expect(consoleSpy).not.toHaveBeenCalledWith('🚀 Server listening on port 1337')
    expect(appMock.listen).toHaveBeenCalledTimes(0)
    process.env.PORT = initialEnvPort
  })

  test('should configure routes', () => {
    require('./index')
    expect(configureRoutes).toHaveBeenCalledTimes(1)
  })

  test('should configure serverless', () => {
    const handlerMock = jest.fn()
    serverlessMock.mockReturnValueOnce(handlerMock)
    const { handler } = require('./index')
    expect(handler).toBe(handlerMock)
    expect(serverlessMock).toHaveBeenCalledWith(appMock)
  })
})
