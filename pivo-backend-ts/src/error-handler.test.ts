import express, { json } from 'express'
import request from 'supertest'

const queryAndRelease = jest.fn()
jest.mock('./db/mariadb', () => ({ queryAndRelease }))

import { configureRoutes } from './routes'

// the error handler is express built in
// just mocking an error in one of the endpoints is the easiest way to test it
describe('routes.ts > error handler', () => {
  const dummyApp = express()
  dummyApp.use(json())
  configureRoutes(dummyApp)

  test('error thrown in the /api/login endpoint should lead to 500, not 401', async () => {
    queryAndRelease.mockRejectedValueOnce(new Error('just a test'))

    const response = await request(dummyApp).post('/api/login')
      .send({ username: 'oleg ', password: 'asdf' })
    expect(response.statusCode).toEqual(500)
    expect(response.text).toEqual(expect.any(String))
  })
})
