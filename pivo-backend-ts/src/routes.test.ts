import { Express } from 'express'
import { configureRoutes } from './routes'
import { rootRouter } from './controllers/root'
import { loginRouter } from './controllers/login'
import { authorizationRouter } from './user-handling/authorization-controller'
import { eventsRouter } from './controllers/events'
import { bandsRouter } from './controllers/bands'
import { locationsRouter } from './controllers/locations'
import { saveEventRouter } from './controllers/save-event'
import { changePasswordRouter } from './controllers/change-password'
import { searchRouter } from './controllers/search'
import { chartsRouter } from './controllers/charts'
import { olegsRouter } from './controllers/olegs-special-list'
import { adminUsersRouter } from './controllers/admin/admin-users'
import { adminSessionsRouter } from './controllers/admin/admin-sessions'
import { adminMetricsRouter } from './controllers/admin/admin-metrics'
import { adminOrphansRouter } from './controllers/admin/admin-orphans'

describe('routes.ts', () => {
  const appMock = {
    use: jest.fn()
  } as unknown as Express

  test('make sure all routes are correctly used', () => {
    configureRoutes(appMock)
    let routerCount = 1
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, rootRouter)
    // expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, debugRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, loginRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, authorizationRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, eventsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, bandsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, locationsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, saveEventRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, changePasswordRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, searchRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, chartsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, olegsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, adminUsersRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, adminSessionsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, adminMetricsRouter)
    expect(appMock.use).toHaveBeenNthCalledWith(routerCount++, adminOrphansRouter)
    // one more, because of the error handler
    expect(appMock.use).toHaveBeenCalledTimes(routerCount)
  })
})
