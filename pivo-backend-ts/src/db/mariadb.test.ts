const createPool = jest.fn()
jest.mock('mariadb', () => ({ createPool }))

import { execute, executeStatement, DbAction, queryAndRelease, insertAndRelease, batchAndRelease } from './mariadb'

describe('mariadb', () => {
  const getConnection = jest.fn()
  beforeEach(() => {
    createPool.mockReturnValue({ getConnection })
  })

  test('execute awaits a connection and calls action with it', async () => {
    const connectionMock = { release: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    const dummyAction: DbAction<object> = jest.fn()
    await execute(dummyAction)

    expect(getConnection).toHaveBeenCalled()
    expect(dummyAction).toHaveBeenCalledWith(connectionMock)
    expect(connectionMock.release).toHaveBeenCalled()
  })

  test('execute wont call action, if awaiting connection fails', async () => {
    getConnection.mockRejectedValueOnce(new Error())

    const dummyAction: DbAction<object> = jest.fn()
    await expect(
      execute(dummyAction)
    ).rejects.toEqual(expect.any(Error))

    expect(getConnection).toHaveBeenCalled()
    expect(dummyAction).not.toHaveBeenCalled()
  })

  test('executeStatement awaits a connection and calls statement', async () => {
    const connectionMock = { release: jest.fn(), query: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    await executeStatement('SELECT * FROM whatever WHERE id = ?', ['234'])

    expect(getConnection).toHaveBeenCalled()
    expect(connectionMock.query).toHaveBeenCalledWith('SELECT * FROM whatever WHERE id = ?', ['234'])
    expect(connectionMock.release).toHaveBeenCalled()
  })

  test('executeStatement wont call statement, if awaiting connection fails', async () => {
    getConnection.mockRejectedValueOnce(new Error())

    try {
      await executeStatement('')
    // eslint-disable-next-line no-empty
    } catch {}
    await expect(
      executeStatement('')
    ).rejects.toEqual(expect.any(Error))

    expect(getConnection).toHaveBeenCalled()
  })

  test('queryAndRelease awaits a connection, calls query and releases connection', async () => {
    const connectionMock = { query: jest.fn().mockResolvedValue([]), release: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    await queryAndRelease('SELECT 1', ['something'])

    expect(getConnection).toHaveBeenCalled()
    expect(connectionMock.query).toHaveBeenCalledWith('SELECT 1', ['something'])
    expect(connectionMock.release).toHaveBeenCalled()
  })

  test('queryAndRelease does nothing, if awaiting connection fails', async () => {
    getConnection.mockRejectedValueOnce(new Error())
    try {
      await queryAndRelease('SELECT 1', ['something'])
    // eslint-disable-next-line no-empty
    } catch {}
    await expect(
      queryAndRelease('SELECT 1', ['something'])
    ).rejects.toEqual(expect.any(Error))
    expect(getConnection).toHaveBeenCalled()
  })

  test('insertAndRelease awaits a connection, calls query and releases connection', async () => {
    const connectionMock = { query: jest.fn().mockResolvedValue({ insertId: 3 }), release: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    const { insertId } = await insertAndRelease('INSERT INTO sometable (columnname) VALUES (?)', ['something'])
    expect(insertId).toEqual(3)

    expect(getConnection).toHaveBeenCalled()
    expect(connectionMock.query).toHaveBeenCalledWith('INSERT INTO sometable (columnname) VALUES (?)', ['something'])
    expect(connectionMock.release).toHaveBeenCalled()
  })

  test('insertAndRelease does nothing, if awaiting connection fails', async () => {
    getConnection.mockRejectedValueOnce(new Error())
    await expect(
      insertAndRelease('INSERT INTO sometable (columnname) VALUES (?)', ['something'])
    ).rejects.toEqual(expect.any(Error))
    expect(getConnection).toHaveBeenCalled()
  })

  test('batchAndRelease awaits a connection, calls batch and releases connection', async () => {
    const connectionMock = { beginTransaction: jest.fn(), batch: jest.fn(), commit: jest.fn(), release: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    await batchAndRelease('INSERT INTO sometable (columnname) VALUES (?)', [['sth1'], ['sth2']])

    expect(getConnection).toHaveBeenCalled()
    expect(connectionMock.beginTransaction).toHaveBeenCalled()
    expect(connectionMock.batch).toHaveBeenCalledWith('INSERT INTO sometable (columnname) VALUES (?)', [['sth1'], ['sth2']])
    expect(connectionMock.commit).toHaveBeenCalled()
    expect(connectionMock.release).toHaveBeenCalled()
  })

  test('batchAndRelease does nothing, if awaiting connection fails', async () => {
    getConnection.mockRejectedValueOnce(new Error())
    await expect(
      batchAndRelease('INSERT INTO sometable (columnname) VALUES (?)', [['sth1'], ['sth2']])
    ).rejects.toEqual(expect.any(Error))
    expect(getConnection).toHaveBeenCalled()
  })

  test('batchAndRelease does rollback on error', async () => {
    const connectionMock = { beginTransaction: jest.fn(), batch: jest.fn().mockRejectedValueOnce(new Error()), commit: jest.fn(), rollback: jest.fn(), release: jest.fn() }
    getConnection.mockResolvedValueOnce(connectionMock)

    await expect(
      batchAndRelease('INSERT INTO sometable (columnname) VALUES (?)', [['sth1'], ['sth2']])
    ).rejects.toEqual(expect.any(Error))

    expect(getConnection).toHaveBeenCalled()
    expect(connectionMock.beginTransaction).toHaveBeenCalled()
    expect(connectionMock.batch).toHaveBeenCalledWith('INSERT INTO sometable (columnname) VALUES (?)', [['sth1'], ['sth2']])
    expect(connectionMock.commit).not.toHaveBeenCalled()
    expect(connectionMock.rollback).toHaveBeenCalled()
    expect(connectionMock.release).toHaveBeenCalled()
  })
})
