// TODO migrate to mysql
import { createPool, PoolConnection, Connection, Pool, QueryOptions } from 'mariadb'

export let pool: Pool

// yes, there's a hardcoded password: used to simplify test/dev scenarios
const getConnection = async (): Promise<PoolConnection> => {
  if (!pool) {
    pool = createPool({
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || 'admin',
      database: 'pivostuff',
      connectionLimit: 5,
      acquireTimeout: 5000,
      dateStrings: true,
      bigIntAsNumber: true,
      supportBigNumbers: true
    })
  }

  return pool.getConnection()
}

export type DbAction<T> = (conn: Connection) => Promise<T>

export const execute = async <T>(action: DbAction<T>): Promise<T> => {
  let conn: PoolConnection | undefined
  try {
    conn = await getConnection()
    return await action(conn)
  } finally {
    if (conn) conn.release()
  }
}

export const executeStatement = async (stmnt: string, params?: any[]): Promise<void> => {
  let conn: PoolConnection | undefined
  try {
    conn = await getConnection()
    await conn.query(stmnt, params)
  } finally {
    if (conn) conn.release()
  }
}

export const queryAndRelease = async <T>(sql: string | QueryOptions, values?: any[]): Promise<T[]> => {
  let conn: PoolConnection | undefined
  try {
    conn = await getConnection()
    const result: T[] = await conn.query(sql, values)
    return result.slice()
  } finally {
    if (conn) conn.release()
  }
}

export interface InsertResult {
  insertId: number
}

export const insertAndRelease = async (sql: string | QueryOptions, values?: any[]): Promise<InsertResult> => {
  let conn: PoolConnection | undefined
  try {
    conn = await getConnection()
    const result: InsertResult = await conn.query(sql, values)
    return result
  } finally {
    if (conn) conn.release()
  }
}

export const batchAndRelease = async (sql: string | QueryOptions, values?: any[]): Promise<void> => {
  let conn: PoolConnection | undefined
  try {
    conn = await getConnection()
    await conn.beginTransaction()
    try {
      await conn.batch(sql, values)
      await conn.commit()
    } catch (err) {
      await conn.rollback()
      throw err
    }
  } finally {
    if (conn) conn.release()
  }
}
