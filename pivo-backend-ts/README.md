# pivo-backend-ts

Backend (nodejs w/ typescript) for the *pivoleg* application.

## runtime port
The port the express app listens on, can be passed as the env var *PORT*. If the env var is missing, the port is chosen by the operating system.
* for the ```yarn dev``` task, the port is always *8002*

## testing
Not sure if this is the perfect approach and for sure not done yet: I just use supertest to test the endpoints, with mocked database calls. I know this is not proper unit testing, but should do the job just fine for me. This project is just my playground, so no idiots will add shit that have no clue how this project is supposed to work (except for the biggest idiot which is me).
Also I create dummy express apps in each test, to just test the routers of each controller. This might not test the actual behaviour, as you could specify a subpath for the usage of each controllers, so the resulting URI might be different. But we agreed in the team (which is me talking to myself) to not do that, just use the routers and they can specify the full path.

<!-- ## sessions
session stuff is build and configured in session-storage.ts
I got a feeling I haven't done the best job here, but I really can't tell. So please don't blame me.

Expired sessions get cleared from the database on every login of a user, because it's easier than a cronjob.
It's ok if the login process takes a minute longer and cleaning sessions is really just a SQL DELETE command, so you won't really feel it.

Although the backend should be stateless, sessions are cached to reduce db calls, but it should be possible to have multiple instances running now. -->

Just my 2 cents and thoughts here. 
