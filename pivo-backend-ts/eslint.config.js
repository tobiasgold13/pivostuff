const eslint = require('@eslint/js')
const tseslint = require('typescript-eslint')
const pluginSecurity = require('eslint-plugin-security')
const pluginPromise = require('eslint-plugin-promise')
const pluginImport = require('eslint-plugin-import')
const pluginN = require('eslint-plugin-n')

// TODO add rules like eqeqeq

module.exports = tseslint.config(
  eslint.configs.recommended,
  tseslint.configs.strict,
  tseslint.configs.stylistic,
  pluginSecurity.configs.recommended,
  pluginPromise.configs['flat/recommended'],
  pluginImport.flatConfigs.recommended,
  pluginN.configs['flat/recommended-script'],
  {
    ignores: ['coverage/', 'dist/'],
    settings: {
      'import/resolver': {
        typescript: {
        // Resolve imports based on tsconfig.json paths
          project: './tsconfig.json'
        }
      }
    },
    rules: {
      // stylistic stuff
      semi: ['error', 'never'],
      'comma-dangle': ['error', 'never'],
      quotes: ['error', 'single'],
      'object-curly-spacing': ['error', 'always'],
      'array-bracket-spacing': ['error', 'never'],
      'comma-spacing': ['error', { 'before': false, 'after': true }],
      'space-in-parens': ['error', 'never'],
      'no-multiple-empty-lines': ['error', { 'max': 1 }],
      'indent': ['error', 2],
      // promise stuff
      'promise/prefer-await-to-callbacks': 'error',
      'promise/prefer-await-to-then': 'error',
      'promise/prefer-catch': 'error',
      // imports are checked by eslint-import-plugin already (works better with TS imports also)
      'n/no-missing-import': 'off',
      'n/no-missing-require': 'off'
    }
  },
  {
    files: ['eslint.config.js', 'jest.config.js'],
    rules: {
      '@typescript-eslint/no-require-imports': 'off',
      'no-undef': 'off'
    }
  },
  {
    // TODO rework this better
    files: ['**/*.ts', 'eslint.config.js'],
    rules: {
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/no-require-imports': 'off',
      '@typescript-eslint/no-non-null-assertion': 'off',
      'n/no-extraneous-require': 'off'
    }
  }
)
