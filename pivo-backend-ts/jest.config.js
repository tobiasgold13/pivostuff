// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },

  // The paths to modules that run some code to configure or set up the testing environment before each test
  // setupFiles: [],
  // setupFilesAfterEnv: [],

  // An object that configures minimum threshold enforcement for coverage results
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },

  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}'],

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // An array of regexp pattern strings used to skip coverage collection
  coveragePathIgnorePatterns: [
    '/node_modules/',
    'src/generated'
  ],

  // Make calling deprecated APIs throw helpful error messages
  // errorOnDeprecated: false,

  // A set of global variables that need to be available in all test environments
  // globals: {},

  clearMocks: true,
  resetMocks: true,
  restoreMocks: true,
  resetModules: true,

  // The test environment that will be used for testing
  // testEnvironment: "jest-environment-jsdom",

  roots: ['<rootDir>/src/']
}
