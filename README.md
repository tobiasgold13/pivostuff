# pivoleg 🤘
pivoleg concert collection! The next rewrite of the concert collection tool. Cheers to Oleg Melonstitch!

## About
Mono-repo for everything around the *pivoleg* concert collection tool. Logo/Icon by ChatGPT.

> This project is just for fun and won't serve a real "business" use case. I just enjoy trying new stuff, but I'm sick of writing the same demo apps again and again.
