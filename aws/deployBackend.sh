#!/bin/bash
BACKEND_LAMBDA_NAME=pivo-backend

TARGET=$(pwd)
rm -rf lambda-backend.zip lambda/

cd ../pivo-backend-ts

if [ "$1" == "--build" ]; then
    if [ ! "$(uname -s)" == "Linux" ]; then
        # DOCKER BUILD
        docker container run --rm -v .:/app -it node:20 /bin/sh -c "\
            cd /app && \
            yarn install && \
            yarn build && \
            yarn install --production\
        "

    else
        # LOCAL BUILD
        yarn install
        yarn build
        yarn install --production
    fi
fi

zip -q -r $TARGET/lambda-backend.zip node_modules/
(cd build && zip -q -r $TARGET/lambda-backend.zip *)
if [ "$1" == "--build" ]; then
    rm -r build node_modules/
    yarn install
fi
cd $TARGET

aws lambda update-function-code \
    --function-name $BACKEND_LAMBDA_NAME \
    --zip-file fileb://lambda-backend.zip \
    --no-cli-pager
rm -rf lambda-backend.zip
