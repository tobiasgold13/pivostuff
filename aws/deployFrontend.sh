#!/bin/bash
FRONTEND_BUCKET_NAME=pivo-frontend-a1s2d3f4

TARGET=$(pwd)

cd ../pivo-angular
if [ "$1" == "--build" ]; then
    rm -rf dist/
    npm install
    npm run build
fi
aws s3 rm s3://$FRONTEND_BUCKET_NAME --recursive --exclude "pivoleg.jpg"
aws s3 cp --recursive dist/pivo-angular/browser/ s3://$FRONTEND_BUCKET_NAME/
# invalidate cloudfront cache
DISTRIBUTION_ID=$(aws cloudfront list-distributions \
    --query "DistributionList.Items[?Origins.Items[?Id == '$FRONTEND_BUCKET_NAME']].Id" \
    --output text)
aws cloudfront create-invalidation --distribution-id $DISTRIBUTION_ID --paths "/*" --no-cli-pager

if [ "$1" == "--build" ]; then
    rm -rf dist/
fi