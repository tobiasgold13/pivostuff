#!/bin/bash
# this script is just for copy & paste
exit 1

# Import
aws s3 cp s3://pivostuff-impex-a1b2c3d4/pivostuff_aws_20241107_175209.sql ./
mariadb -u $DB_USER -p"$DB_PASSWORD" -h $DB_HOST pivostuff < ./pivostuff_aws_20241107_175209.sql

# Export
FILENAME=pivostuff_aws_$(date --utc +%Y%m%d_%H%M%S).sql
mariadb-dump -u $DB_USER -p"$DB_PASSWORD" -h $DB_HOST pivostuff > $FILENAME
aws s3 cp $FILENAME s3://pivostuff-impex-a1b2c3d4/

# stuff
mariadb -u $DB_USER -p"$DB_PASSWORD" -h $DB_HOST
