#!/bin/bash

verify() {
    # set -x
    aws --no-cli-pager \
        cloudformation validate-template \
        --template-body file://infrastructure.yaml
}

info() {
    # set -x
    aws --no-cli-pager \
        cloudformation describe-stacks --stack-name pivostuff --query "Stacks[].Outputs[]"
}

create() {
    # set -x
    read -s -p "DbMasterPassword > " DB_MASTER_PASSWORD
    aws --no-cli-pager \
        cloudformation create-stack \
        --stack-name pivostuff \
        --template-body file://infrastructure.yaml \
        --capabilities CAPABILITY_IAM \
        --parameters ParameterKey=DbMasterPassword,ParameterValue=\"${DB_MASTER_PASSWORD}\"
    aws cloudformation wait stack-create-complete --stack-name pivostuff
}

update() {
    # set -x
    read -s -p "DbMasterPassword > " DB_MASTER_PASSWORD
    echo ""
    aws --no-cli-pager \
        cloudformation update-stack \
        --stack-name pivostuff \
        --template-body file://infrastructure.yaml \
        --capabilities CAPABILITY_IAM \
        --parameters ParameterKey=DbMasterPassword,ParameterValue=\"${DB_MASTER_PASSWORD}\"
    aws cloudformation wait stack-update-complete --stack-name pivostuff
}

delete() {
    # set -x
    aws s3 rm s3://pivo-frontend-a1s2d3f4 --recursive
    aws s3 rm s3://pivostuff-impex-a1b2c3d4 --recursive
    aws --no-cli-pager cloudformation delete-stack --stack-name pivostuff
    aws cloudformation wait stack-delete-complete --stack-name pivostuff
}

case $1 in
    "verify")
        verify
        ;;

    "create")
        create
        ;;

    "info")
        info
        ;;

    "update")
        update
        ;;

    "delete")
        delete
        ;;

    *)
        echo "use ./infrastructure.sh [verify|create|info|update|delete]"
        ;;
esac
