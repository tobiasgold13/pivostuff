USE `pivostuff`;

INSERT INTO `configuration` (`token_exp_time`, `jwt_secret`) VALUES ('5h', 'AMAZING_TEST_SECRET');
-- UPDATE `configuration` SET `token_exp_time` = '5m', `jwt_secret` = 'AMAZING_TEST_SECRET';

INSERT INTO `user` (`id`, `name`, `roles`, `password`) VALUES (1, 'root', 'ADMIN', '$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');

-- data for all the normal tests
INSERT INTO `user` (`id`, `name`, `password`) VALUES (2, 'oleg','$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');
INSERT INTO `band` (`id`, `name`) VALUES ('1', 'Slayer');
INSERT INTO `band` (`id`, `name`) VALUES ('2', 'Landmvrks');
INSERT INTO `location` (`id`, `country`, `city`, `name`) VALUES ('1', 'Deutschland','Lichtenfels','Paunchy Cats');

INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (1, 'Metalcamp','2022-02-02','2022-02-05', 1, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (1, 2, '2022-02-03', 2);

INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (2, 'Full Force','2021-04-04','2021-04-05', 1, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (2, 1, '2021-04-04', 2);

INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (3, 'Summerbreeze','2021-08-08','2021-08-10', 1, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (3, 2, '2021-08-09', 2);

-- additional data for event edit test
INSERT INTO `location` (`id`, `country`, `city`, `name`) VALUES ('2','Slowenien','Tolmin','Tolmin');
INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (4, 'Wrong Event','2022-02-12','2022-02-15', 2, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (4, 2, '2022-02-13', 2);

-- data for password change test
INSERT INTO `user` (`id`, `name`, `password`) VALUES (3, 'user-password-change','$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');

-- admin tests
INSERT INTO `user` (`id`, `name`, `roles`, `password`) VALUES (4, 'admin', 'ADMIN', '$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');
INSERT INTO `user` (`id`, `name`, `roles`, `password`) VALUES (5, 'rename-username-test', 'ADMIN', '$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');
INSERT INTO `user` (`id`, `name`, `roles`, `password`) VALUES (6, 'admin-password-change', 'ADMIN', '$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');

-- event category test
INSERT INTO `event` (`id`, `name`, `start`, `end`, `category`, `location_id`, `user_id`) VALUES (5, 'Kategorie E','2021-01-01','2021-01-10', 'concert', 1, 2);
-- should be possible to save existing events with null values
INSERT INTO `location` (`id`, `country`, `city`) VALUES ('3','irgendwo','ein Ort');
INSERT INTO `event` (`id`, `start`, `end`, `location_id`, `user_id`) VALUES (6, '2021-01-01','2021-01-10', 3, 2);

-- test data for orphaned entities
INSERT INTO `location` (`id`, `country`, `city`, `name`) VALUES ('4', 'Island','Grindavik','Vulkan');
INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (7, 'Event For The Orphans','1999-06-12','1999-06-12', 4, 4);
INSERT INTO `band` (`id`, `name`) VALUES ('3', 'The Orphans');
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (7, 3, '1999-06-12', 4);

-- test data for autocomplete location without name (asign it to a random event, to not create an orphan)
INSERT INTO `location` (`id`, `country`, `city`) VALUES ('5', 'Mexiko','Cdmx');
INSERT INTO `event` (`id`, `start`, `end`, `location_id`, `user_id`) VALUES (8, '2022-02-12','2022-02-15', 5, 2);

-- test data for setlist link on band page
INSERT INTO `location` (`id`, `country`, `city`, `name`) VALUES ('6', 'Deutschland','Nürnberg','Löwensaal');
INSERT INTO `band` (`id`, `name`) VALUES ('4', 'Stick To Your Guns');
INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (9, 'Release Tour','2025-01-15','2025-01-15', 6, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (9, 4, '2025-01-15', 2);
INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (10, 'Future Tour','2025-01-15','2025-01-15', 6, 2);
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (10, 4, '2025-01-15', 2);

-- #######################
-- charts & statistic data
INSERT INTO `user` (`id`, `name`, `password`) VALUES (7, 'charts-statistic', '$2b$04$Qi.xWJQEApE.3H6vBNyoaOa.2oWOWWqiFD.yGYliVbLGzVKz9GahO');

-- Oleg's Special List
INSERT INTO `event` (`id`, `name`, `start`, `end`, `location_id`, `user_id`) VALUES (12, 'Olegs Special List','2025-01-01','2025-01-31', 6, 7);
INSERT INTO `band` (`id`, `name`) VALUES ('5', '00-Plus 20');
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (12, 5, '2025-01-01', 7), (12, 5, '2025-01-02', 7), (12, 5, '2025-01-03', 7), (12, 5, '2025-01-04', 7), (12, 5, '2025-01-05', 7), (12, 5, '2025-01-06', 7), (12, 5, '2025-01-07', 7), (12, 5, '2025-01-08', 7), (12, 5, '2025-01-09', 7), (12, 5, '2025-01-10', 7), (12, 5, '2025-01-11', 7), (12, 5, '2025-01-12', 7), (12, 5, '2025-01-13', 7), (12, 5, '2025-01-14', 7), (12, 5, '2025-01-15', 7), (12, 5, '2025-01-16', 7), (12, 5, '2025-01-17', 7), (12, 5, '2025-01-18', 7), (12, 5, '2025-01-19', 7), (12, 5, '2025-01-20', 7);
INSERT INTO `band` (`id`, `name`) VALUES ('6', '00-Band 10-19');
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (12, 6, '2025-01-01', 7), (12, 6, '2025-01-02', 7), (12, 6, '2025-01-03', 7), (12, 6, '2025-01-04', 7), (12, 6, '2025-01-05', 7), (12, 6, '2025-01-06', 7), (12, 6, '2025-01-07', 7), (12, 6, '2025-01-08', 7), (12, 6, '2025-01-09', 7), (12, 6, '2025-01-10', 7);
INSERT INTO `band` (`id`, `name`) VALUES ('7', '00-From 5 Till 9');
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (12, 7, '2025-01-01', 7), (12, 7, '2025-01-02', 7), (12, 7, '2025-01-03', 7), (12, 7, '2025-01-04', 7), (12, 7, '2025-01-05', 7);
INSERT INTO `band` (`id`, `name`) VALUES ('8', '00-One Two Four');
INSERT INTO `attendance` (`event_id`, `band_id`, `date`, `user_id`) VALUES (12, 8, '2025-01-01', 7);

-- #######################
