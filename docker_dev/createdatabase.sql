DROP DATABASE IF EXISTS `pivostuff`;

CREATE DATABASE `pivostuff`;

USE `pivostuff`;

CREATE TABLE `band` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  CONSTRAINT `name_unique` UNIQUE (`name`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `location` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(70) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NULL,
  CONSTRAINT `location_unique` UNIQUE (`country`, `city`, `name`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `start` DATE NOT NULL,
  `end` DATE NOT NULL,
  `category` ENUM('concert', 'festival') NULL,
  `location_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `attendance` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `band_id` INT NOT NULL,
  `date` DATE NULL DEFAULT NULL,
  `user_id` INT NOT NULL,
  CONSTRAINT `unique` UNIQUE (`event_id`,`band_id`,`date`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `password` CHAR(60) NOT NULL,
  `roles` VARCHAR(255) NULL,
  CONSTRAINT `unique` UNIQUE (`name`),
  PRIMARY KEY (`id`)
);

CREATE TABLE `session` (
  `token` VARCHAR(255) NOT NULL,
  `created` DATETIME NOT NULL,
  `user_id` INT NOT NULL,
  `user_agent` VARCHAR(255) NULL,
  PRIMARY KEY (`token`)
);

CREATE TABLE `configuration` (
  `single_row_restriction` ENUM('') NOT NULL,
  `token_exp_time` VARCHAR(20) NOT NULL,
  `jwt_secret` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`single_row_restriction`)
);

CREATE TABLE `metrics_history` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `day` DATE NOT NULL,
  `count` INT NOT NULL,
  `metric` VARCHAR(255) NOT NULL,
  CONSTRAINT `entry_unique` UNIQUE (`user_id`, `day`, `metric`),
  PRIMARY KEY (`id`)
);

-- prod
-- ALTER TABLE `session` ADD `user_agent` VARCHAR(255) NULL;
