# docker_dev

spins up a production like  environment with docker-compose. the app is exposed on port *8000* so visiting http://localhost:8000 should work.

```sh
$ docker-compose up --build -d
```

## testing stuff
Some minimal database initialization is done on startup. Wait for the init container to exit and login with user *oleg* and password *test*.

## runtime
The database port (*3306*) will be mapped to the host, so I can run the data migration script and have data in my app. Of course the old database needs to be running as well on the expected port.
