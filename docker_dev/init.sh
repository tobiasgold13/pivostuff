#!/bin/bash
DB_HOST=db
MAX_WAIT_TIME=20

# wait till db is ready
counter=1
while (! mysqladmin ping -h $DB_HOST --silent) && ([ $counter -lt $MAX_WAIT_TIME ]); do
    echo "Waiting $counter for mysql...."
    counter=`expr $counter + 1`
    sleep 1
done

# init db
mysql -h $DB_HOST -u root -padmin < /testdata/createdatabase.sql
mysql -h $DB_HOST -u root -padmin < /testdata/testdata.sql

echo "Done initializing!"